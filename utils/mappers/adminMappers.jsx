import { HANDLE_CAMPUS, HANDLE_CAMPUSES, HANDLE_REQUEST, HANDLE_USERS } from '../permissions';
import { WORKFLOW_CONDITIONS_CHOICES } from '../constants/enums';

export const selectWorkflowCondition = (condition) => {
    if (!condition) {
        return WORKFLOW_CONDITIONS_CHOICES.NONE.select;
    }
    switch (condition.value) {
        case false:
            return WORKFLOW_CONDITIONS_CHOICES.POSITIVE.select;
        case true:
            return WORKFLOW_CONDITIONS_CHOICES.NEGATIVE.select;
        default:
            return WORKFLOW_CONDITIONS_CHOICES.NONE.select;
    }
};

export const workflowCards = (index, role, condition = null) => ({
    id: index + 1,
    text: role.shortLabel,
    role: role,
    behavior: role.validator.behavior,
    condition: selectWorkflowCondition(condition)
});

export const mapUserData = (data, campus) => {
    const unit = data.unit
        ? {
              id: data.unit.id,
              label: data.unit.label
          }
        : null;
    return {
        userData: {
            firstname: data.firstname,
            lastname: data.lastname,
            email: data.email
        },
        roleData: {
            role: data.role,
            campus: { id: campus.id, label: campus.label },
            unit
        }
    };
};

export const mapUnitData = (data, cards) => ({
    label: data.name,
    trigram: data.trigram.trim().toUpperCase(),
    workflow: {
        steps: cards.map((card, index) => {
            return {
                role: {
                    template: card.role.template,
                    label: card.role.label,
                    shortLabel: card.role.shortLabel,
                    validator: {
                        scope: card.role.validator.scope,
                        behavior: card.role.validator.behavior
                    }
                },
                condition:
                    card.condition !== WORKFLOW_CONDITIONS_CHOICES.NONE.select && index > 0
                        ? {
                              role: cards[index - 1].role.template,
                              value: WORKFLOW_CONDITIONS_CHOICES[card.condition].value
                          }
                        : null
            };
        })
    }
});

export const mapEditUnit = (unitData) => {
    const cards = unitData.workflow.steps.map((s, index) => {
        return workflowCards(index, s.role, s.condition);
    });
    return {
        id: unitData.id,
        name: unitData.label,
        trigram: unitData.trigram,
        cards
    };
};

export const mapUsersList = (usersList) =>
    usersList.map((user) => ({
        id: user.id,
        lastname: user.lastname,
        firstname: user.firstname,
        campus: user.roles[0] && user.roles[0].campuses[0] ? user.roles[0].campuses[0].label : '-',
        unit: user.roles[0] && user.roles[0].units[0] ? user.roles[0].units[0].label : '-',
        role: user.roles[0] ? user.roles[0].role.label : '-',
        userRole: user.roles[0] ? user.roles[0].role.template : null,
        deleteLabel: user.email.original
    }));

export const mapUserFormRolesList = (roles, userRole) => {
    const noUnitRoles = roles.filter(
        (r) => !r.validator.scope && !r.permissions.includes(HANDLE_REQUEST)
    );
    const filterRoles = roles.filter((r) => !r.validator.scope);
    switch (true) {
        case userRole.permissions.includes(HANDLE_CAMPUSES):
            return {
                rolesList: filterRoles,
                noUnitRoles
            };
        case userRole.permissions.includes(HANDLE_CAMPUS):
            return {
                rolesList: filterRoles.filter((r) => !r.permissions.includes(HANDLE_CAMPUSES)),
                noUnitRoles
            };
        case userRole.permissions.includes(HANDLE_USERS):
            return {
                rolesList: filterRoles.filter((r) => r.permissions.includes(HANDLE_REQUEST)),
                noUnitRoles
            };
        default:
            return {
                rolesList: [],
                noUnitRoles
            };
    }
};

export const mapAreaCampusList = (list) =>
    list.map((item) => ({ id: item.id, label: item.label, trigram: item.trigram }));
