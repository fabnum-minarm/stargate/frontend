import { ROLES } from '../constants/enums';

export default function findVisitorStatus(units) {
    const finalRoles = Object.keys(ROLES).filter((e) => e.includes('FINAL'));
    const status = units
        .find((u) => u.steps.find((s) => finalRoles.includes(s.role.template)))
        .steps.find((s) => finalRoles.includes(s.role.template)).state.tags;
    return status ? status.join(', ').toString() : '';
}
