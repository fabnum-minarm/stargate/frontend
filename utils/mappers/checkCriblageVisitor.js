import { ROLES } from '../constants/enums';

export const REFUSED_STATUS = 'NEGATIVE';
export const ACCEPTED_STATUS = 'POSITIVE';
export const PROGRESS_STEP_STATUS = 'IN_PROGRESS';

export default function checkCriblageVisitor(units) {
    const isScreeningDone = units.find((u) =>
        u.steps.find(
            (step) => step.role.template === ROLES.ROLE_AREA_ADVISEMENT.role && step.state.value
        )
    );
    return isScreeningDone
        ? isScreeningDone.steps.find((s) => s.role.template === ROLES.ROLE_AREA_ADVISEMENT.role)
              .state.value
        : PROGRESS_STEP_STATUS;
}
