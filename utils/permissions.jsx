import { MINDEF_CONNECT_REDIRECT_PAGE, RESET_PASS_PAGE, PRINT_VISITOR } from './constants/appUrls';

export const UNAUTH_PERMISSIONS = [MINDEF_CONNECT_REDIRECT_PAGE, RESET_PASS_PAGE];

// list of roles permissions, identical to backend
export const HANDLE_CAMPUSES = 'HANDLE_CAMPUSES';
export const HANDLE_CAMPUS = 'HANDLE_CAMPUS';
export const HANDLE_USERS = 'HANDLE_USERS';
export const HANDLE_REQUEST = 'HANDLE_REQUEST';
export const HANDLE_VISITOR = 'HANDLE_VISITOR';
export const EXPORT_REQUEST = 'EXPORT_REQUEST';
export const URL_PERMISSIONS = {
    HANDLE_CAMPUSES: [
        '/administration',
        '/administration/base',
        '/administration/base/creation',
        '/administration/utilisateurs',
        '/administration/utilisateurs/creation',
        '/administration/zoneinfluence',
        '/administration/zoneinfluence/creation'
    ],
    HANDLE_CAMPUS: [
        '/administration',
        '/administration/base',
        '/administration/utilisateurs',
        '/administration/utilisateurs/creation'
    ],
    HANDLE_USERS: ['/administration/utilisateurs', '/administration/utilisateurs/creation'],
    HANDLE_REQUEST: [
        '/demandes',
        '/nouvelle-demande',
        '/nouvelle-demande/simple',
        '/nouvelle-demande/groupe'
    ],
    HANDLE_VISITOR: ['/mes-traitements', '/visiteurs'],
    EXPORT_REQUEST: ['/mes-traitements']
};
export const checkUrlPermissions = (userPermissions, path) => {
    const permissionsList = userPermissions.reduce(
        (acc, curr) => {
            return [...acc, ...URL_PERMISSIONS[curr]];
        },
        ['/', '/a-propos', '/contactez-nous', '/compte', PRINT_VISITOR]
    );
    return permissionsList.includes(path);
};

export const isAdmin = (userRole) => userRole.permissions.includes(HANDLE_CAMPUS);

export const isSuperAdmin = (userRole) => userRole.permissions.includes(HANDLE_CAMPUSES);

export const canHandleUser = (userRole) => userRole.permissions.includes(HANDLE_USERS);

export const checkRequestDetailAuth = (data, activeRole) => {
    const isInWorkflow = data.getCampus.getRequest.listVisitors.list.find((visitor) =>
        visitor.units.find(
            (u) =>
                (u.id === activeRole.unit || !activeRole.unit) &&
                u.steps.find((step) => step.role.template === activeRole.role.template)
        )
    );
    return !!isInWorkflow;
};
