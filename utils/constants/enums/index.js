export const REQUEST_OBJECT = {
    PRIVATE: 'PRIVATE',
    PROFESSIONAL: 'PROFESSIONAL'
};

export const WORKFLOW_BEHAVIOR = {
    VALIDATION: {
        value: 'VALIDATION',
        RESPONSE: {
            positive: 'ACCEPTED',
            negative: 'REJECTED'
        }
    },
    ADVISEMENT: {
        value: 'ADVISEMENT',
        RESPONSE: {
            positive: 'POSITIVE',
            negative: 'NEGATIVE',
            externally: 'EXTERNALLY'
        }
    },
    INFORMATION: {
        value: 'INFORMATION',
        RESPONSE: {
            positive: 'validated'
        }
    }
};

export const WORKFLOW_CONDITIONS_CHOICES = {
    NONE: { value: null, label: 'Aucune condition', select: 'NONE' },
    POSITIVE: {
        value: false,
        label: 'Seulement si intervenant précédent Positif',
        select: 'POSITIVE'
    },
    NEGATIVE: {
        value: true,
        label: 'Seulement si intervenant précédent Négatif',
        select: 'NEGATIVE'
    }
};

export const VISITOR_STATUS = {
    ACCEPTED: 'Accepté',
    REJECTED: 'Refusé',
    MIXED: 'Partiellement accepté',
    CREATED: 'En cours',
    CANCELED: 'Annulé'
};

export const VALIDATION_SCOPE_UNIT = 'VALIDATION_SCOPE_UNIT';
export const VALIDATION_SCOPE_CAMPUS = 'VALIDATION_SCOPE_CAMPUS';
export const VALIDATION_SCOPE_AREA = 'VALIDATION_SCOPE_AREA';

export const ROLES = {
    ROLE_SUPER_ADMIN: {
        role: 'ROLE_SUPER_ADMIN',
        workflow: false
    },
    ROLE_ADMIN: {
        role: 'ROLE_ADMIN',
        workflow: false
    },
    ROLE_BASIC_UNIT_VALIDATION: {
        role: 'ROLE_BASIC_UNIT_VALIDATION',
        workflow: true
    },
    ROLE_AREA_ADVISEMENT: {
        role: 'ROLE_AREA_ADVISEMENT',
        workflow: true
    },
    ROLE_UNIT_VALIDATION_TAGS: {
        role: 'ROLE_UNIT_VALIDATION_TAGS',
        workflow: true
    },
    ROLE_FINAL_VALIDATION: {
        role: 'ROLE_FINAL_VALIDATION',
        workflow: true
    },
    ROLE_FINAL_VALIDATION_CFAS: {
        role: 'ROLE_FINAL_VALIDATION_CFAS',
        workflow: true
    },
    ROLE_FINAL_VALIDATION_OSB: {
        role: 'ROLE_FINAL_VALIDATION_OSB',
        workflow: true
    },
    ROLE_HOST: {
        role: 'ROLE_HOST',
        workflow: false
    },
    ROLE_BASIC_USER: {
        role: 'ROLE_BASIC_USER',
        workflow: false
    },
    ROLE_OBSERVER: {
        role: 'ROLE_BASIC_USER',
        workflow: false
    }
};

export const STATE_REQUEST = {
    STATE_DRAFTED: { state: 'DRAFTED' },
    STATE_CREATED: { state: 'CREATED' },
    STATE_CANCELED: { state: 'CANCELED' },
    STATE_REMOVED: { state: 'REMOVED' },
    STATE_ACCEPTED: { state: 'ACCEPTED' },
    STATE_REJECTED: { state: 'REJECTED' },
    STATE_MIXED: { state: 'MIXED' }
};

export const ID_DOCUMENT = {
    IDCard: { label: "Carte d'identité", regex: /^[a-z0-9]+$/gi },
    Passport: { label: 'Passeport', regex: /^\d{2}[A-Za-z]{2}\d{5}$/ },
    CIMSCard: { label: 'Carte CIMS', regex: /^\d{10}$/ }
};

export const VISITOR_INFOS = {
    firstname: 'Prénom',
    employeeType: 'Type employé',
    birthLastname: 'Nom de naissance',
    usageLastname: "Nom d'usage",
    rank: 'Grade',
    company: 'Unité/Entreprise',
    email: 'Mail',
    vip: 'VIP',
    vipReason: 'Motif VIP',
    nationality: 'Nationalitée',
    birthday: 'Date de Naissance',
    birthplace: 'Ville de Naissance',
    birthcountry: 'Pays de Naissance',
    kind: 'Type Document',
    reference: 'Numero Document'
};

export const ERROR_TYPE = {
    required: 'Ce champ est obligatoire',
    enum: 'Pas la valeur attendue',
    regexp: 'Mail non conforme',
    date: 'Format de date invalide'
};
