import React, { useCallback, useContext } from 'react';
import { useLazyQuery } from '@apollo/client';
import PropTypes from 'prop-types';
import { useSnackBar } from './snackbar';
import { GEN_CSV_EXPORTS_BY_UNIT } from '../apollo/queries';
import { activeRoleCacheVar } from '../apollo/cache';

export const ExportUnitContext = React.createContext();

export function ExportUnitProvider({ children }) {
    const { addAlert } = useSnackBar();

    const [exportCsvByUnit] = useLazyQuery(GEN_CSV_EXPORTS_BY_UNIT, {
        fetchPolicy: 'no-cache',
        onCompleted: useCallback((d) => {
            const link = document.createElement('a');
            link.href = d.getCampus.generateCSVExportByUnitsLink.link;
            link.setAttribute('download', `export-${new Date()}.csv`);
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);
        }),

        onError: useCallback(() => {
            addAlert({
                message: "Erreur lors de l'export",
                severity: 'error'
            });
        }, [])
    });

    const exportUnitVisitors = useCallback(() => {
        exportCsvByUnit({
            fetchPolicy: 'no-cache',
            variables: { unitsId: activeRoleCacheVar().unit }
        });
    }, []);

    const value = {
        exportUnitVisitors
    };

    return <ExportUnitContext.Provider value={value}>{children}</ExportUnitContext.Provider>;
}

ExportUnitProvider.propTypes = {
    children: PropTypes.node.isRequired
};

export const withExportUnitProvider = (Components) => {
    const returnProvider = (props) => {
        return (
            <ExportUnitProvider>
                <Components {...props} />
            </ExportUnitProvider>
        );
    };
    return returnProvider;
};

export function useExportUnit() {
    const exports = useContext(ExportUnitContext);
    if (!exports) {
        throw new Error('Cannot use `useExport` outside of a ExportProvider');
    }
    return exports;
}
