import { gql } from '@apollo/client';

export const CANCEL_REQUEST = gql`
    mutation cancelRequest(
        $requestId: String!
        $campusId: String!
        $transition: RequestTransition!
    ) {
        campusId @client @export(as: "campusId")
        mutateCampus(id: $campusId) {
            shiftRequest(id: $requestId, transition: $transition) {
                id
                status
            }
        }
    }
`;

export const CANCEL_VISITOR = gql`
    mutation cancelVisitor($visitorId: ObjectID!, $requestId: String!, $campusId: String!) {
        campusId @client @export(as: "campusId")
        mutateCampus(id: $campusId) {
            mutateRequest(id: $requestId) {
                cancelVisitor(id: $visitorId) {
                    id
                    status
                }
            }
        }
    }
`;

export const MUTATE_VISITOR = gql`
    mutation validateVisitorStep(
        $requestId: String!
        $campusId: String!
        $visitorId: ObjectID!
        $as: ValidationPersonas!
        $tags: [String]
        $decision: String!
    ) {
        campusId @client @export(as: "campusId")
        activeRoleCache @client {
            validationPersonas @export(as: "as")
        }
        mutateCampus(id: $campusId) {
            mutateRequest(id: $requestId) {
                validateVisitorStep(id: $visitorId, as: $as, decision: $decision, tags: $tags) {
                    id
                    units {
                        label
                        steps {
                            role {
                                template
                                label
                                shortLabel
                                validator {
                                    scope
                                    behavior
                                }
                                tags {
                                    label
                                    value
                                    primary
                                }
                            }
                            state {
                                value
                                isOK
                                date
                                tags
                            }
                        }
                    }
                }
            }
        }
    }
`;

export const GEN_CSV_EXPORTS = gql`
    mutation generateCSVExport($campusId: String!, $visitorsId: [String]!) {
        campusId @client @export(as: "campusId")
        mutateCampus(id: $campusId) {
            generateCSVExportLink(visitorsId: $visitorsId) {
                token
                link
            }
        }
    }
`;

export const CREATE_USER = gql`
    mutation createUser($user: UserInput!) {
        createUser(user: $user) {
            id
            firstname
            lastname
            email {
                original
            }
            roles {
                role {
                    template
                    label
                    shortLabel
                    permissions
                    validator {
                        scope
                        behavior
                    }
                    tags {
                        label
                        value
                        primary
                    }
                }
                campuses {
                    id
                    label
                }
                units {
                    id
                    label
                }
            }
        }
    }
`;

export const DELETE_ROLE = gql`
    mutation deleteUserRole($id: ObjectID!, $roleData: UserRoleInput) {
        deleteUserRole(id: $id, roleData: $roleData) {
            id
        }
    }
`;

export const UPDATE_CAMPUS_ROLE = gql`
    mutation editCampusRole(
        $id: String!
        $shortLabel: String!
        $role: RoleInput!
        $tag: RoleTagInput
    ) {
        editCampusRole(id: $id, shortLabel: $shortLabel, role: $role, tag: $tag) {
            id
            roles {
                label
                template
                permissions
            }
        }
    }
`;

export const ADD_CAMPUS_ROLE = gql`
    mutation addCampusRole($id: String!, $role: RoleInput!) {
        addCampusRole(id: $id, role: $role) {
            id
            roles {
                label
                template
                permissions
            }
        }
    }
`;

export const ADD_USER_ROLE = gql`
    mutation addUserRole($roleData: UserRoleInput!, $id: ObjectID!, $campusId: String!) {
        addUserRole(roleData: $roleData, id: $id, campusId: $campusId) {
            id
            firstname
            lastname
            email {
                original
            }
            roles {
                role {
                    template
                    label
                    shortLabel
                    permissions
                    validator {
                        scope
                        behavior
                    }
                    tags {
                        label
                        value
                        primary
                    }
                }
                campuses {
                    id
                    label
                }
                units {
                    id
                    label
                }
            }
        }
    }
`;
