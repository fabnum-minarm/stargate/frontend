import { gql } from '@apollo/client';

import { REQUEST_DATA, VISITOR_DATA } from './fragments';

export const GET_ME = gql`
    query getMe {
        me {
            id
            firstname
            lastname
            roles {
                role {
                    template
                    label
                    shortLabel
                    permissions
                    validator {
                        scope
                        behavior
                    }
                    tags {
                        label
                        value
                        primary
                    }
                }
                campuses {
                    id
                    label
                }
                units {
                    id
                    label
                }
            }
            email {
                original
            }
        }
    }
`;

export const LIST_ROLES = gql`
    query ListRoles {
        listRoles {
            list {
                id
                label
                shortLabel
                permissions
                validator {
                    scope
                    behavior
                }
                tags {
                    label
                    value
                    primary
                }
                campuses {
                    id
                }
            }
        }
    }
`;

export const LIST_CAMPUS_ROLES = gql`
    query ListCampusRoles($id: String!) {
        getCampus(id: $id) {
            listCampusRoles {
                list {
                    label
                    shortLabel
                    permissions
                    validator {
                        scope
                        behavior
                    }
                    tags {
                        label
                        value
                        primary
                    }
                    campuses {
                        id
                    }
                }
            }
        }
    }
`;

export const GET_ACTIVE_ROLE = gql`
    query getActiveRole {
        activeRoleCache @client {
            role {
                template
                label
                shortLabel
                permissions
                validator {
                    scope
                    behavior
                }
                tags {
                    label
                    value
                    primary
                }
            }
            unit
            unitLabel
        }
    }
`;

export const IS_LOGGED_IN = gql`
    query IsUserLoggedIn {
        isLoggedIn @client
    }
`;

export const INIT_CACHE = gql`
    query initCache(
        $campusId: String!
        $as: ValidationPersonas!
        $cursor: OffsetCursor!
        $filterCreated: RequestFilters!
        $filterTreated: RequestFilters!
    ) {
        getCampus(id: $campusId) {
            id
            label
            listVisitorsToValidate(as: $as, cursor: $cursor) {
                list {
                    ...VisitorData
                    request {
                        ...RequestData
                    }
                }
                meta {
                    total
                }
            }
            progress: listMyRequests(filters: $filterCreated, cursor: $cursor) {
                list {
                    id
                    from
                    to
                    reason
                    status
                    places {
                        id
                        label
                    }
                    owner {
                        firstname
                        lastname
                        unit {
                            label
                        }
                    }
                }
                meta {
                    total
                }
            }
            treated: listMyRequests(filters: $filterTreated, cursor: $cursor) {
                list {
                    id
                    from
                    to
                    reason
                    status
                    places {
                        id
                        label
                    }
                    owner {
                        firstname
                        lastname
                        unit {
                            label
                        }
                    }
                }
                meta {
                    total
                }
            }
        }
    }
    ${VISITOR_DATA}
    ${REQUEST_DATA}
`;

export const LIST_MY_REQUESTS = gql`
    query listMyRequests(
        $campusId: String!
        $cursor: OffsetCursor
        $filtersP: RequestFilters!
        $filtersT: RequestFilters!
    ) {
        campusId @client @export(as: "campusId")
        getCampus(id: $campusId) {
            id
            progress: listMyRequests(filters: $filtersP, cursor: $cursor) {
                list {
                    id
                    from
                    to
                    reason
                    status
                    places {
                        id
                        label
                    }
                    owner {
                        firstname
                        lastname
                        unit {
                            label
                        }
                    }
                }
                meta {
                    total
                }
            }
            treated: listMyRequests(filters: $filtersT, cursor: $cursor) {
                list {
                    id
                    from
                    to
                    reason
                    status
                    places {
                        id
                        label
                    }
                    owner {
                        firstname
                        lastname
                        unit {
                            label
                        }
                    }
                }
                meta {
                    total
                }
            }
        }
    }
`;

export const FIND_USER_BY_MAIL = gql`
    query findUser($email: EmailAddress!) {
        findUser(email: $email) {
            id
            firstname
            lastname
            email {
                original
            }
            roles {
                role {
                    template
                    label
                }
                userInCharge
                campuses {
                    id
                    label
                }
                units {
                    id
                    label
                }
            }
        }
    }
`;

export const GET_REQUEST = gql`
    query getRequest($campusId: String!, $requestId: String!, $cursor: OffsetCursor) {
        campusId @client @export(as: "campusId")
        getCampus(id: $campusId) {
            id
            getRequest(id: $requestId) {
                id
                from
                to
                reason
                status
                places {
                    id
                    label
                }
                owner {
                    firstname
                    lastname
                    unit {
                        label
                    }
                }
                referent {
                    email
                    firstname
                    lastname
                    phone
                }
                listVisitors(cursor: $cursor) {
                    list {
                        id
                        vip
                        rank
                        firstname
                        birthLastname
                        employeeType
                        company
                        status
                        isSecurityNotificationSend
                        units {
                            label
                            steps {
                                role {
                                    template
                                    label
                                    shortLabel
                                    validator {
                                        scope
                                        behavior
                                    }
                                }
                                state {
                                    isOK
                                    value
                                    tags
                                }
                            }
                        }
                    }
                    meta {
                        total
                    }
                }
            }
        }
    }
`;

export const GET_CAMPUS = gql`
    query getCampus($id: String!) {
        getCampus(id: $id) {
            id
            label
            trigram
            createdAt
            roles {
                template
                label
                shortLabel
                validator {
                    scope
                    behavior
                }
                tags {
                    label
                    value
                    primary
                }
                permissions
            }
            securityNotificationEmail
            securityNotificationEnabled
            campusDocuments {
                file {
                    id
                }
            }
            generateSecurityFileExportLink {
                link
            }
            generateAccesPlanExportLink {
                link
            }
        }
    }
`;

export const GET_UNIT = gql`
    query GetUnitQuery($campusId: String!, $id: ObjectID!) {
        getCampus(id: $campusId) {
            id
            getUnit(id: $id) {
                id
                label
                trigram
                workflow {
                    steps {
                        role {
                            template
                            label
                            shortLabel
                            validator {
                                scope
                                behavior
                            }
                        }
                        condition {
                            role
                            value
                        }
                    }
                }
            }
        }
    }
`;

export const GET_PLACES_LIST = gql`
    query getPlacesList($campusId: String!, $filters: PlaceFilters) {
        getCampus(id: $campusId) {
            id
            label
            listPlaces(filters: $filters) {
                list {
                    id
                    label
                    unitInCharge {
                        id
                        label
                    }
                }
                meta {
                    total
                }
            }
        }
    }
`;

export const GET_EMPLOYEE_LIST = gql`
    query getEmployeeList($campusId: String!, $cursor: OffsetCursor) {
        getCampus(id: $campusId) {
            id
            label
            listEmployeeType(cursor: $cursor) {
                list {
                    id
                    label
                }
                meta {
                    total
                }
            }
        }
    }
`;

export const GEN_CSV_EXPORTS_BY_UNIT = gql`
    query generateCSVExportByUnitsLink($campusId: String!, $unitsId: [String]) {
        campusId @client @export(as: "campusId")
        getCampus(id: $campusId) {
            generateCSVExportByUnitsLink(unitsId: $unitsId) {
                token
                link
            }
        }
    }
`;

export const GET_UNITS_LIST = gql`
    query listUnits($cursor: OffsetCursor, $campusId: String!, $search: String) {
        getCampus(id: $campusId) {
            id
            listUnits(cursor: $cursor, search: $search) {
                meta {
                    offset
                    first
                    total
                }
                list {
                    id
                    label
                    trigram
                }
            }
        }
    }
`;

export const LIST_USERS = gql`
    query listUsers($cursor: OffsetCursor, $hasRole: HasRoleInput, $campus: String) {
        listUsers(cursor: $cursor, hasRole: $hasRole, campus: $campus) {
            list {
                id
                firstname
                lastname
                email {
                    original
                }
                roles {
                    role {
                        template
                        label
                        shortLabel
                        permissions
                        validator {
                            scope
                            behavior
                        }
                        tags {
                            label
                            value
                            primary
                        }
                    }
                    userInCharge
                    campuses {
                        id
                        label
                    }
                    units {
                        id
                        label
                    }
                }
            }
            meta {
                total
            }
        }
    }
`;

export const LIST_TREATMENTS = gql`
    query listTreatments(
        $campusId: String!
        $as: ValidationPersonas!
        $filters: RequestVisitorFilters
        $role: RoleTemplate
        $unit: ObjectID
        $cursor: OffsetCursor
    ) {
        campusId @client @export(as: "campusId")
        activeRoleCache @client {
            validationPersonas @export(as: "as")
            role {
                template @export(as: "role")
            }
            unit @export(as: "unit")
        }
        getCampus(id: $campusId) {
            id
            securityNotificationEnabled
            progress: listVisitorsToValidate(as: $as, filters: $filters, cursor: $cursor) {
                list {
                    ...VisitorData
                    exportDate
                    request {
                        ...RequestData
                    }
                }
                meta {
                    total
                }
            }
            treated: listVisitors(
                isDone: { role: $role, unit: $unit, value: true }
                cursor: $cursor
            ) {
                list {
                    id
                    nid
                    firstname
                    birthLastname
                    isInternal
                    employeeType
                    rank
                    company
                    email
                    vip
                    vipReason
                    nationality
                    birthday
                    birthplace
                    exportDate
                    isScreened @client
                    units {
                        label
                        steps {
                            role {
                                template
                                label
                                shortLabel
                                validator {
                                    scope
                                    behavior
                                }
                                tags {
                                    label
                                    value
                                    primary
                                }
                            }
                            state {
                                value
                                isOK
                                date
                                tags
                            }
                        }
                    }
                    request {
                        ...RequestData
                    }
                }
                meta {
                    total
                }
            }
        }
    }
    ${VISITOR_DATA}
    ${REQUEST_DATA}
`;

export const LIST_EXPORTS = gql`
    query listExports(
        $campusId: String!
        $filters: RequestVisitorFilters
        $role: RoleTemplate
        $unit: ObjectID
        $cursor: OffsetCursor
    ) {
        campusId @client @export(as: "campusId")
        activeRoleCache @client {
            validationPersonas @export(as: "as")
            role {
                template @export(as: "role")
            }
        }
        getCampus(id: $campusId) {
            id
            securityNotificationEnabled
            export: listVisitors(
                isDone: { role: $role, unit: $unit, value: true }
                filters: $filters
                cursor: $cursor
            ) {
                list {
                    id
                    nid
                    firstname
                    birthLastname
                    isInternal
                    employeeType
                    rank
                    company
                    email
                    vip
                    vipReason
                    nationality
                    birthday
                    birthplace
                    exportDate
                    isScreened @client
                    units {
                        label
                        steps {
                            role {
                                template
                                label
                                shortLabel
                                validator {
                                    scope
                                    behavior
                                }
                                tags {
                                    label
                                    value
                                    primary
                                }
                            }
                            state {
                                value
                                isOK
                                date
                                tags
                            }
                        }
                    }
                    request {
                        ...RequestData
                    }
                }
                meta {
                    total
                }
            }
        }
    }
    ${REQUEST_DATA}
`;

export const LIST_TREATMENTS_SCREENING = gql`
    query listTreatments(
        $campusId: String!
        $as: ValidationPersonas!
        $filters: RequestVisitorFilters
        $role: RoleTemplate!
        $unit: ObjectID
        $cursor: OffsetCursor
        $search: String
    ) {
        campusId @client @export(as: "campusId")
        activeRoleCache @client {
            validationPersonas @export(as: "as")
            role {
                template @export(as: "role")
            }
        }
        getCampus(id: $campusId) {
            id
            securityNotificationEnabled
            progress: listVisitorsToValidate(
                as: $as
                filters: $filters
                cursor: $cursor
                search: $search
            ) {
                list {
                    ...VisitorData
                    request {
                        ...RequestData
                    }
                    identityDocuments {
                        file {
                            id
                        }
                    }
                    generateIdentityFileExportLink {
                        link
                    }
                }
                meta {
                    total
                }
            }
            treated: listVisitors(
                isDone: { role: $role, unit: $unit, value: true }
                cursor: $cursor
                search: $search
            ) {
                list {
                    id
                    nid
                    firstname
                    birthLastname
                    isInternal
                    employeeType
                    rank
                    company
                    email
                    vip
                    vipReason
                    nationality
                    birthday
                    birthplace
                    exportDate
                    isScreened @client
                    units {
                        label
                        steps {
                            role {
                                template
                                label
                                shortLabel
                                validator {
                                    scope
                                    behavior
                                }
                                tags {
                                    label
                                    value
                                    primary
                                }
                            }
                            state {
                                value
                                isOK
                                date
                                tags
                            }
                        }
                    }
                    request {
                        ...RequestData
                    }
                    identityDocuments {
                        file {
                            id
                        }
                    }
                    generateIdentityFileExportLink {
                        link
                    }
                }
                meta {
                    total
                }
            }
        }
    }
    ${VISITOR_DATA}
    ${REQUEST_DATA}
`;

export const GET_CAMPUSES_LIST = gql`
    query listCampuses($cursor: OffsetCursor, $filters: CampusFilters, $search: String) {
        listCampuses(filters: $filters, cursor: $cursor, search: $search) {
            meta {
                offset
                first
                total
            }
            list {
                id
                label
                trigram
                roles {
                    template
                    label
                    shortLabel
                    validator {
                        scope
                        behavior
                    }
                    tags {
                        label
                        value
                        primary
                    }
                    permissions
                }
            }
        }
    }
`;

export const GET_AREA = gql`
    query getArea($id: String!) {
        getArea(id: $id) {
            id
            label
            campuses {
                id
                label
                trigram
            }
        }
    }
`;

export const LIST_AREAS = gql`
    query listAreas {
        listAreas {
            list {
                id
                label
                campuses {
                    id
                    label
                    trigram
                }
            }
            meta {
                offset
                first
                total
            }
        }
    }
`;
