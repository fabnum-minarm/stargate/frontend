/* eslint-disable react/jsx-wrap-multilines */
import React, { useEffect } from 'react';

import PropTypes from 'prop-types';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import { makeStyles } from '@material-ui/core/styles';

import { format } from 'date-fns';
import findValidationStep from '../../../utils/mappers/findValidationStep';
import findVisitorStatus from '../../../utils/mappers/findVisitorStatus';
import { ID_DOCUMENT, ROLES } from '../../../utils/constants/enums';
import { IconButton } from '@material-ui/core';
import { AttachFile, Print } from '@material-ui/icons';
import { useLogin } from '../../../lib/loginContext';

const useStyles = makeStyles((theme) => ({
    listItem: {
        backgroundColor: theme.palette.common.white,
        marginBottom: 20,
        borderRadius: 4
    },
    title: {
        marginBottom: 20,
        '& h5': {
            fontWeight: 'bold',
            fontSize: '1.2rem'
        }
    },
    subtitles: {
        fontWeight: 'bold'
    },
    information: {
        marginLeft: '4px'
    },
    iconButton: {
        position: 'relative',
        bottom: 3,
        padding: 0
    }
}));

const createItem = ({
    request,
    units,
    birthLastname,
    usageLastname,
    firstname,
    birthday,
    birthplace,
    birthcountry,
    company,
    nationality,
    identityDocuments,
    generateIdentityFileExportLink
}) => ({
    title: { label: '#Demande', value: request.id },
    visitor: [
        {
            label: 'Nom de naissance (usage), Prénom',
            value: `${birthLastname.toUpperCase()} ${
                usageLastname ? '(' + usageLastname.toUpperCase() + ')' : ''
            } ${firstname}`
        },
        {
            label: 'Date de venue',
            value: `Du
                ${format(new Date(request.from), 'dd/MM/yyyy')} au
                ${format(new Date(request.to), 'dd/MM/yyyy')} inclus`
        },
        {
            label: 'Né le',
            value: format(new Date(birthday), 'dd/MM/yyyy')
        },
        {
            label: 'Statut de la demande',
            value: findVisitorStatus(units) ? findVisitorStatus(units) : findValidationStep(units)
        },
        {
            label: 'Ville de naissance',
            value: birthplace
        },
        {
            label: 'Pays de naissance',
            value: birthcountry
        },
        {
            label: 'Unité/Société',
            value: company
        },
        {
            label: 'Nationalité',
            value: nationality
        },
        {
            label: 'Demandeur',
            value: `${request.owner.rank || ''}
                  ${request.owner.lastname.toUpperCase()}
                  ${request.owner.firstname}`
        },
        {
            label: "Pièce d'identité",
            value: `${ID_DOCUMENT[identityDocuments[0].kind].label} n° ${
                identityDocuments[0].reference
            }`,
            fileLink:
                identityDocuments[0].file && identityDocuments[0].file.id
                    ? generateIdentityFileExportLink.link
                    : null
        },
        {
            label: 'Lieux visités',
            value: request.places.reduce((prev, place, index) => {
                if (index === 0) return place.label;
                return `${prev}, ${place.label}`;
            }, '')
        },
        {
            label: 'Motif',
            value: request.reason
        }
    ]
});
export default function RequestVisitorItem({ requestVisitor }) {
    const item = createItem(requestVisitor);

    const { activeRole } = useLogin();

    const classes = useStyles();

    const handlePrint = (id) => {
        const iframe = document.frames
            ? document.frames[id]
            : document.getElementById(`print-${requestVisitor.id}`);

        iframe.src = `/visitors-print/${requestVisitor.id}`;
    };

    useEffect(() => {
        const handleMessage = (event) => {
            if (event.data.action === `print-${requestVisitor.id}`) {
                const iframe = document.frames
                    ? document.frames[`print-${requestVisitor.id}`]
                    : document.getElementById(`print-${requestVisitor.id}`);
                const iframeWindow = iframe?.contentWindow || iframe;

                iframe.focus();
                iframeWindow.print();
            }
        };

        window.addEventListener('message', handleMessage);

        return () => {
            window.removeEventListener('message', handleMessage);
        };
    }, []);

    return (
        <>
            <ListItem className={classes.listItem}>
                <ListItemText>
                    <Grid container>
                        <Grid container item sm={12} className={classes.title}>
                            <Typography variant="h5">{item.title.label} : </Typography>
                            <Typography
                                variant="h5"
                                color="primary"
                                className={classes.information}>
                                {item.title.value}
                            </Typography>
                            <div style={{ flexGrow: 1 }} />
                            <IconButton onClick={() => handlePrint(`print-${requestVisitor.id}`)}>
                                <Print />
                            </IconButton>
                        </Grid>
                    </Grid>
                    <Grid container>
                        {item.visitor.map((v) => {
                            if (
                                activeRole.role.template === ROLES.ROLE_BASIC_USER.role &&
                                v.label === 'Motif'
                            )
                                return '';
                            else {
                                return (
                                    <Grid item sm={6} key={v.label}>
                                        <Grid container>
                                            <Typography
                                                variant="body2"
                                                className={classes.subtitles}>
                                                {v.label} :
                                            </Typography>
                                            <Typography
                                                variant="body2"
                                                className={classes.information}>
                                                {v.value}{' '}
                                                {v.fileLink && (
                                                    <a href={v.fileLink} download>
                                                        <IconButton
                                                            className={classes.iconButton}
                                                            aria-label="AttachFileIcon">
                                                            <AttachFile />
                                                        </IconButton>
                                                    </a>
                                                )}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                );
                            }
                        })}
                    </Grid>
                </ListItemText>
            </ListItem>

            <iframe
                id={`print-${requestVisitor.id}`}
                src=""
                title="Print"
                style={{ display: 'none' }}
            />
        </>
    );
}

RequestVisitorItem.propTypes = {
    requestVisitor: PropTypes.shape({
        id: PropTypes.string.isRequired,
        units: PropTypes.array.isRequired,
        request: PropTypes.shape({
            id: PropTypes.string.isRequired,
            from: PropTypes.string.isRequired,
            to: PropTypes.string.isRequired,
            owner: PropTypes.shape({
                rank: PropTypes.string,
                firstname: PropTypes.string.isRequired,
                lastname: PropTypes.string.isRequired
            }).isRequired
        }).isRequired,
        birthLastname: PropTypes.string.isRequired,
        usageLastname: PropTypes.string.isRequired,
        firstname: PropTypes.string.isRequired,
        birthday: PropTypes.string.isRequired,
        birthplace: PropTypes.string.isRequired,
        birthcountry: PropTypes.string.isRequired,
        nationality: PropTypes.string.isRequired,
        identityDocuments: PropTypes.arrayOf(
            PropTypes.shape({
                kind: PropTypes.string,
                reference: PropTypes.string
            })
        )
    }).isRequired
};
