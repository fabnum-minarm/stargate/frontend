import { useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import { memo, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { activeRoleCacheVar } from '../../lib/apollo/cache';
import { ROLES, WORKFLOW_BEHAVIOR } from '../../utils/constants/enums';
import CustomTableCellHeader from './cells/TableCellHeader';
import TableContainer from './styled/TableContainer';
import RowTreatment from './rows/RowTreatments';
import ProcessDialog from '../styled/common/ProcessDialogs';

const useStyles = makeStyles(() => ({
    table: {
        zIndex: 10
    }
}));

export const columnsArray = (role, treated, exported) => {
    const columns = [];
    switch (role.template) {
        case ROLES.ROLE_BASIC_UNIT_VALIDATION.role:
            columns.push({ id: 'reason', label: 'Motif', position: 3 });
            break;
        case ROLES.ROLE_UNIT_VALIDATION_TAGS.role:
            columns.push({ id: 'screening', label: 'Criblage', position: 3 });
            break;
        case ROLES.ROLE_FINAL_VALIDATION.role:
            if (!treated) columns.push({ id: 'decisions', label: 'Décisions', position: 3 });
            if (exported) columns.push({ id: 'export', label: 'Exporté le', position: 5 });
            break;
        case ROLES.ROLE_FINAL_VALIDATION_CFAS.role:
            columns.push({ id: 'reason', label: 'Motif', position: 3 });
            break;
        case ROLES.ROLE_FINAL_VALIDATION_OSB.role:
            columns.push({ id: 'decisions', label: 'Décisions', position: 3 });
            break;

        default:
            return null;
    }
    columns.push(
        { id: 'request', label: 'Infos demande', position: 1 },
        { id: 'visitor', label: 'Infos visiteur', position: 2 },
        { id: 'action', label: 'Status', position: 4 }
    );

    function compare(a, b) {
        const posA = a.position;
        const posB = b.position;

        if (posA > posB) return 1;
        if (posA < posB) return -1;
    }

    return columns.sort(compare);
};

function mapCreateRow({
    id,
    firstname,
    birthLastname,
    company,
    employeeType,
    isInternal,
    isCribled,
    exportDate,
    units,
    isSecurityNotificationSend,
    request,
    nationality
}) {
    const fromDate = format(new Date(request.from), 'dd/MM/yyyy');
    const toDate = format(new Date(request.to), 'dd/MM/yyyy');
    return {
        request: {
            id: request.id,
            period: `${fromDate === toDate ? fromDate : `${fromDate} au ${toDate}`}`,
            owner: request.owner
                ? `
          ${request.owner.rank || ''} 
          ${request.owner.firstname} 
          ${request.owner.lastname.toUpperCase()} 
          ${request.owner.unit?.label ? `(${request.owner.unit.label})` : ''}`
                : '',
            places: request.places
                .map((place, index) => {
                    if (index === request.places.length - 1) return `${place.label}.`;
                    return `${place.label}, `;
                })
                .join(''),
            referent: `${request.referent.email} (${request.referent.phone})`,
            reason: request.reason
        },
        visitor: {
            id,
            firstname,
            lastname: birthLastname,
            nationality,
            isInternal: isInternal ? 'MINARM' : 'HORS MINARM',
            company: company,
            employeeType: employeeType,
            criblage: isCribled,
            units,
            exportDate,
            isSecurityNotificationSend
        }
    };
}

const makeSimpleChoicesArray = (validator, tags) =>
    tags.map((tag) => ({
        label: tag.value,
        validation: WORKFLOW_BEHAVIOR[validator.behavior].RESPONSE.positive,
        tags: [tag.value]
    }));
const makeComplexChoicesArray = (validator, tags) => {
    return [
        ...tags
            .filter((tag) => tag.primary)
            .map((tag) => ({
                label: tag.value,
                validation: WORKFLOW_BEHAVIOR[validator.behavior].RESPONSE.positive,
                tags: [tag.value]
            })),
        {
            label: 'Autre choix',
            validation: null,
            tags: [],
            subChoices: tags
                .filter((tag) => !tag.primary)
                .map((tag) => ({
                    label: tag.label,
                    description: tag.label,
                    tags: [tag.value],
                    validation: WORKFLOW_BEHAVIOR[validator.behavior].RESPONSE.positive
                }))
        }
    ];
};

export const choicesArray = (role) => {
    const { validator, tags } = role;
    let choices = role.tags.every((tag) => !tag.primary)
        ? makeSimpleChoicesArray(validator, tags)
        : makeComplexChoicesArray(validator, tags);

    if (!choices.length) {
        choices.push({
            label: 'ACCEPTER',
            validation: WORKFLOW_BEHAVIOR[validator.behavior].RESPONSE.positive,
            tags: []
        });
    }
    choices.push({
        label: 'REFUSER',
        validation: WORKFLOW_BEHAVIOR[validator.behavior].RESPONSE.negative,
        tags: []
    });
    return choices;
};

const TableTreatmentsToTreat = ({ visitors, treated, exported }) => {
    const [toViewVisitor, setToViewVisitor] = useState(null);

    const classes = useStyles();
    const rows = useMemo(
        () =>
            visitors.reduce((acc, dem) => {
                acc.push(mapCreateRow(dem));
                return acc;
            }, []),
        [visitors]
    );

    const choices = useMemo(() => choicesArray(activeRoleCacheVar().role, treated), [
        activeRoleCacheVar()
    ]);
    const columns = useMemo(() => columnsArray(activeRoleCacheVar().role, treated, exported), [
        activeRoleCacheVar()
    ]);
    return (
        <TableContainer height={57}>
            <Table stickyHeader aria-label="sticky table" className={classes.table}>
                <TableHead>
                    <TableRow>
                        {columns.map((column) => (
                            <CustomTableCellHeader
                                id={column.id}
                                key={column.id}
                                align={column.align}>
                                {column.label || ''}
                            </CustomTableCellHeader>
                        ))}
                    </TableRow>
                </TableHead>

                {rows.map((row, index) => (
                    <TableBody key={`${treated ? 'treated' : ''}_${row.request.id}_${index}`}>
                        <RowTreatment
                            choices={choices}
                            row={row}
                            modalOpen={() => setToViewVisitor(row.visitor)}
                            columns={columns}
                            treated={treated}
                            exported={exported}
                        />
                    </TableBody>
                ))}
            </Table>
            <ProcessDialog
                isOpen={toViewVisitor !== null}
                units={toViewVisitor?.units}
                onClose={() => setToViewVisitor(null)}
            />
        </TableContainer>
    );
};
export default memo(TableTreatmentsToTreat);

TableTreatmentsToTreat.propTypes = {
    treated: PropTypes.bool,
    exported: PropTypes.bool,
    visitors: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            firstname: PropTypes.string,
            birthLastname: PropTypes.string,
            isInternal: PropTypes.bool,
            isScreened: PropTypes.string,
            company: PropTypes.string,
            employeeType: PropTypes.string,
            isSecurityNotificationSend: PropTypes.bool,
            request: PropTypes.shape({
                id: PropTypes.string,
                from: PropTypes.string,
                to: PropTypes.string,
                reason: PropTypes.string,
                owner: PropTypes.shape({
                    rank: PropTypes.string,
                    firstname: PropTypes.string,
                    lastname: PropTypes.string,
                    unit: PropTypes.shape({
                        label: PropTypes.string
                    })
                }),
                places: PropTypes.arrayOf(
                    PropTypes.shape({
                        label: PropTypes.string
                    })
                ),
                referent: PropTypes.shape({
                    email: PropTypes.string,
                    phone: PropTypes.string
                })
            })
        })
    ).isRequired
};

TableTreatmentsToTreat.defaultProps = { treated: false, exported: false };
