import PropTypes from 'prop-types';
import { useReactiveVar, useApolloClient, gql, useMutation } from '@apollo/client';

import React, { useState } from 'react';

import FormControlLabel from '@material-ui/core/FormControlLabel';

import Radio from '@material-ui/core/Radio';
import TableCell from '@material-ui/core/TableCell';

import { makeStyles, withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import MailOutilineIcon from '@material-ui/icons/MailOutline';
import ScheduleIcon from '@material-ui/icons/Schedule';
import InputBase from '@material-ui/core/InputBase';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import { useDecisions } from '../../../lib/hooks/useDecisions';
import { campusIdVar } from '../../../lib/apollo/cache';

const useStyles = makeStyles((theme) => ({
    radio: {
        '&$checked': {
            color: theme.palette.success.main
        }
    },
    radioRejected: {
        '&$checked': {
            color: theme.palette.error.main
        }
    },
    radioWarning: {
        '&$checked': {
            color: theme.palette.warning.main
        }
    },
    checked: {},
    formControl: {
        display: 'block'
    },
    noLabel: {
        marginTop: '5px',
        marginLeft: '-15px',
        position: 'absolute',
        maxWidth: '155px'
    },
    liFontSize: {
        '& li': {
            fontSize: '0.75rem'
        }
    },
    divRelative: {
        position: 'relative',
        maxHeight: 32
    },
    button: {
        borderRadius: '25px',
        marginTop: '18px'
    },
    send: {
        color: '#ffa726 !important',
        borderColor: '#ffa726 !important'
    }
}));

const CustomInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3)
        }
    },
    input: {
        border: 'none',
        fontSize: '0.75rem'
    }
}))(InputBase);

// use to reset the choice
const initChoice = {
    label: '',
    validation: '',
    tags: []
};

export const MUTATE_VISITOR_SECURITY_INFO = gql`
    mutation updateVisitorsSecurityInfo(
        $campusId: String!
        $idRequest: String!
        $idVisitor: ObjectID!
    ) {
        campusId @client @export(as: "campusId")
        mutateCampus(id: $campusId) {
            mutateRequest(id: $idRequest) {
                sendSecurityEmail(id: $idVisitor) {
                    id
                    isSecurityNotificationSend
                }
            }
        }
    }
`;
export function ActionCell({ choices, decision }) {
    const classes = useStyles();

    const client = useApolloClient();

    const campusId = useReactiveVar(campusIdVar);

    const campusNotifEnabled = client.readFragment({
        id: `Campus:${campusId}`, // The value of the to-do item's cache ID
        fragment: gql`
            fragment notifEnabled on Campus {
                id
                securityNotificationEnabled
            }
        `
    });

    const {
        decisions: { [`${decision.request.id}_${decision.id}`]: decisionQuery },
        addDecision
    } = useDecisions();

    const [mutateSecurityNotif] = useMutation(MUTATE_VISITOR_SECURITY_INFO);

    const decisionChoice = React.useMemo(() => {
        if (!decisionQuery) return initChoice;

        const choice = choices.find((choice) => {
            if (choice.label === decisionQuery?.choice?.label) return true;

            if (choice.subChoices) {
                let exist = false;
                choice.subChoices.forEach((subChoice) => {
                    if (subChoice.label === decisionQuery?.choice?.label) exist = true;
                });
                return exist;
            }
        });

        if (!choice) return initChoice;
        return choice;
    }, [decisionQuery]);

    const radioColor = (label) => {
        if (label === 'REFUSER') return classes.radioRejected;
        if (label === 'RES') return classes.radioWarning;
        return classes.radio;
    };

    const [otherChoice, setOtherChoice] = useState(initChoice);

    /**
     * If the actual value of decisionChoice is equal to the value of
     * the clicked element => reset the choice (deselect).
     * If the clicked element is equal to
     * 'Autre choix' (select menu) => set choice to the value of otherChoice.
     * else set the decision to the values of the clicked element
     * @param {*} choice
     * @returns addDecision.
     */
    const handleRadioClick = (choice) => {
        let selectedChoice;
        if (decisionChoice === choice) {
            selectedChoice = initChoice;
        } else if (choice.label === 'Autre choix') {
            selectedChoice = otherChoice;
        } else {
            selectedChoice = choice;
        }
        return addDecision({
            choice: selectedChoice,
            request: { id: decision.request.id },
            id: decision.id
        });
    };

    /**
     * Management fo the select change.
     * If the actual value of decisionChoice is equal to the value of
     * the clicked element => reset the choice (deselect).
     * Else set the value of choice to the clicked element.
     */
    const handleChangeOtherChoice = (event, choice) => {
        if (choice === otherChoice) {
            addDecision({
                choice: initChoice,
                request: { id: decision.request.id },
                id: decision.id
            });
            return;
        }
        setOtherChoice(choice);
        addDecision({
            choice,
            request: { id: decision.request.id },
            id: decision.id
        });
    };

    return (
        <TableCell>
            <Grid container style={{ width: '140px' }}>
                {choices.map((choice) => {
                    if (choice.label === 'Autre choix') {
                        if (choice.subChoices && choice.subChoices.length <= 0) {
                            return;
                        }
                        return (
                            <Grid item sm={12} className={classes.divRelative} key={choice.label}>
                                <FormControlLabel
                                    label=""
                                    value={choice.label}
                                    control={
                                        <Radio
                                            classes={{
                                                root: classes.radio,
                                                checked: classes.checked
                                            }}
                                            disabled={otherChoice.label === ''}
                                            checked={choice.label === decisionChoice.label}
                                            onClick={() => handleRadioClick(choice)}
                                        />
                                    }
                                />
                                <FormControl className={classes.noLabel}>
                                    <Select
                                        displayEmpty
                                        labelId="select-label"
                                        id="select"
                                        MenuProps={{ classes: { paper: classes.liFontSize } }}
                                        value={otherChoice.label}
                                        input={<CustomInput />}>
                                        <MenuItem key="other choice" disabled value="">
                                            Autre choix
                                        </MenuItem>
                                        {choice.subChoices.map((ch) => (
                                            <MenuItem
                                                onClick={(event) =>
                                                    handleChangeOtherChoice(event, ch)
                                                }
                                                key={ch.label}
                                                value={ch.label}>
                                                {ch.label}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                        );
                    }
                    return (
                        <Grid item sm={12} key={choice.label} className={classes.divRelative}>
                            <FormControlLabel
                                key={choice.label}
                                label={<Typography variant="caption">{choice.label}</Typography>}
                                value={choice.label}
                                control={
                                    <Radio
                                        classes={{
                                            root: radioColor(choice.label),
                                            checked: classes.checked
                                        }}
                                        checked={choice.label === decisionChoice.label}
                                        onClick={() => {
                                            handleRadioClick(choice);
                                            setOtherChoice(initChoice);
                                        }}
                                    />
                                }
                            />
                        </Grid>
                    );
                })}
                {
                    /**
                     * Send notification component
                     **/
                    (() => {
                        // screening doesnt need to send notification
                        if (
                            decision.isSecurityNotificationSend === undefined ||
                            !campusNotifEnabled?.securityNotificationEnabled
                        )
                            return '';

                        return !decision.isSecurityNotificationSend ? (
                            <Button
                                variant="outlined"
                                size="small"
                                onClick={() =>
                                    mutateSecurityNotif({
                                        variables: {
                                            idRequest: decision.request.id,
                                            idVisitor: decision.id
                                        }
                                    })
                                }
                                className={classes.button}
                                startIcon={<MailOutilineIcon />}>
                                NOTE SÛRETÉ
                            </Button>
                        ) : (
                            <Button
                                variant="outlined"
                                size="small"
                                color="secondary"
                                disabled
                                className={`${classes.button} ${classes.send}`}
                                startIcon={<ScheduleIcon />}>
                                NOTE SÛRETÉ
                            </Button>
                        );
                    })()
                }
            </Grid>
        </TableCell>
    );
}

export default ActionCell;

ActionCell.propTypes = {
    choices: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            validation: PropTypes.string,
            tags: PropTypes.arrayOf(PropTypes.string)
        })
    ).isRequired,
    decision: PropTypes.object.isRequired
};
