import TableCell from '@material-ui/core/TableCell';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

import SeeMoreOrLess from '../../styled/common/SeeMoreOrLess';

const useStyles = makeStyles(() => ({
    subtitles: {
        fontWeight: 'bold'
    },
    widthCell: {
        maxWidth: '220px',
        verticalAlign: 'baseline'
    }
}));

export default function RequestCell({ request, columns }) {
    const classes = useStyles();
    return (
        <TableCell className={classes.widthCell}>
            <div>
                <Typography display="inline" variant="body2" className={classes.subtitles}>
                    Demande :
                </Typography>
                <Typography display="inline" variant="caption">{` ${request.id}`}</Typography>
            </div>
            <div>
                <Typography display="inline" variant="body2" className={classes.subtitles}>
                    Période :
                </Typography>
                <Typography display="inline" variant="caption">{` ${request.period}`}</Typography>
            </div>
            <div>
                <Typography display="inline" variant="body2" className={classes.subtitles}>
                    Demandeur :
                </Typography>
                <Typography display="inline" variant="caption">{` ${request.owner}`}</Typography>
            </div>
            <div>
                <Typography display="inline" variant="body2" className={classes.subtitles}>
                    Lieux :
                </Typography>

                <Typography display="inline" variant="caption">{` ${request.places}`}</Typography>
            </div>
            <div>
                <Typography display="inline" variant="body2" className={classes.subtitles}>
                    Responsable visite :
                </Typography>

                <Typography display="inline" variant="caption">{` ${request.referent}`}</Typography>
            </div>
            {!columns.find((col) => col.id === 'reason') && (
                <div>
                    <Typography display="inline" variant="body2" className={classes.subtitles}>
                        Motif :
                    </Typography>
                    <SeeMoreOrLess>{` ${request.reason}`}</SeeMoreOrLess>
                </div>
            )}
        </TableCell>
    );
}

RequestCell.defaultProps = {
    columns: []
};

RequestCell.propTypes = {
    request: PropTypes.shape({
        id: PropTypes.string,
        period: PropTypes.string,
        owner: PropTypes.string,
        places: PropTypes.string,
        reason: PropTypes.string,
        referent: PropTypes.string
    }).isRequired,
    columns: PropTypes.arrayOf(PropTypes.object)
};
