import React, { useRef } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import PropTypes from 'prop-types';
import ItemCard from './itemCard';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import SelectedBadge from './common/TabBadge';
import { MenuItem, Select } from '@material-ui/core';
import { WORKFLOW_CONDITIONS_CHOICES } from '../../utils/constants/enums';

const useStyles = makeStyles(() => ({
    cardHeader: {
        width: 137,
        paddingTop: 2
    },
    buttonStyle: {
        height: 8
    },
    removeIcon: {
        height: 12,
        cursor: 'pointer',
        zIndex: 1000
    },
    cardContent: {
        position: 'relative',
        top: '-8px'
    },
    selectCondition: {
        width: 140,
        marginTop: 2,
        height: 20,
        fontSize: '.8rem'
    }
}));

const ItemTypes = {
    CARD: 'cards'
};

const DndCard = ({ id, text, condition, index, moveCard, deleteCard, handleChangeCondition }) => {
    const classes = useStyles();
    const ref = useRef(null);
    const [, drop] = useDrop({
        accept: ItemTypes.CARD,
        hover(item, monitor) {
            if (!ref.current) {
                return;
            }
            const dragIndex = item.index;
            const hoverIndex = index;
            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return;
            }
            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect();
            // Get vertical middle
            const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
            // Determine mouse position
            const clientOffset = monitor.getClientOffset();
            // Get pixels to the top
            const hoverClientY = clientOffset.y - hoverBoundingRect.top;
            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%
            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return;
            }
            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return;
            }
            // Time to actually perform the action
            moveCard(dragIndex, hoverIndex);
            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            // eslint-disable-next-line no-param-reassign
            item.index = hoverIndex;
        }
    });
    const [{ isDragging }, drag] = useDrag({
        item: { type: ItemTypes.CARD, id, index },
        collect: (monitor) => ({
            isDragging: monitor.isDragging()
        })
    });
    const opacity = isDragging ? 0 : 1;
    drag(drop(ref));
    return (
        <div ref={ref}>
            <ItemCard
                opacity={opacity}
                style={{ cursor: 'move', opacity }}
                conditionned={condition !== WORKFLOW_CONDITIONS_CHOICES.NONE.select}>
                <Grid
                    container
                    justify="space-between"
                    alignItems="center"
                    className={classes.cardHeader}>
                    <SelectedBadge small>{index + 1}</SelectedBadge>
                    <CloseIcon onClick={() => deleteCard(text)} className={classes.removeIcon} />
                </Grid>
                <Grid className={classes.cardContent}>{text}</Grid>
            </ItemCard>
            {index > 0 && (
                <Grid container>
                    <Select
                        id="select-condition"
                        className={classes.selectCondition}
                        variant="outlined"
                        value={condition}
                        onChange={(e) => handleChangeCondition(e.target.value)}
                        renderValue={(selected) => {
                            return WORKFLOW_CONDITIONS_CHOICES[selected].label.length < 10
                                ? WORKFLOW_CONDITIONS_CHOICES[selected].label
                                : `${WORKFLOW_CONDITIONS_CHOICES[selected].label.slice(0, 10)}...`;
                        }}>
                        <MenuItem value={WORKFLOW_CONDITIONS_CHOICES.NONE.select}>
                            {WORKFLOW_CONDITIONS_CHOICES.NONE.label}
                        </MenuItem>
                        <MenuItem value={WORKFLOW_CONDITIONS_CHOICES.POSITIVE.select}>
                            {WORKFLOW_CONDITIONS_CHOICES.POSITIVE.label}
                        </MenuItem>
                        <MenuItem value={WORKFLOW_CONDITIONS_CHOICES.NEGATIVE.select}>
                            {WORKFLOW_CONDITIONS_CHOICES.NEGATIVE.label}
                        </MenuItem>
                    </Select>
                </Grid>
            )}
        </div>
    );
};

DndCard.propTypes = {
    id: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    condition: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    moveCard: PropTypes.func.isRequired,
    deleteCard: PropTypes.func.isRequired,
    handleChangeCondition: PropTypes.func.isRequired
};

export default DndCard;
