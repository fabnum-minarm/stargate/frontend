import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const useStyles = makeStyles(() => ({
    input: {
        display: 'none'
    }
}));

export default function InputUpload({ onChange, name }) {
    const classes = useStyles();

    const handleChange = (event) => {
        const { value, files } = event.target;
        onChange({ value, files: files[0] });
    };

    return (
        <>
            <label htmlFor={`icon-button-${name}`}>
                <input
                    className={classes.input}
                    id={`icon-button-${name}`}
                    onChange={handleChange}
                    type="file"
                />
                <Tooltip title={'Upload'} arrow>
                    <IconButton aria-label="upload" component="span" color="primary">
                        <AttachFileIcon />
                    </IconButton>
                </Tooltip>
            </label>
        </>
    );
}

InputUpload.propTypes = {
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired
};
