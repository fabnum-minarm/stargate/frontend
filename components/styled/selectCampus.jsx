import React from 'react';
import { MenuItem, Select } from '@material-ui/core';
import { activeRoleCacheVar, campusIdVar } from '../../lib/apollo/cache';
import { GET_ME } from '../../lib/apollo/queries';
import { useQuery } from '@apollo/client';

export default function SelectCampus() {
    const { data } = useQuery(GET_ME);
    const handleChange = (id) => {
        campusIdVar(id);
    };

    if (!data) return '';

    return (
        <div>
            <Select
                style={{ height: 40 }}
                variant="outlined"
                value={campusIdVar()}
                onChange={(e) => handleChange(e.target.value)}>
                {data.me.roles
                    .find((r) => r.role.template === activeRoleCacheVar().role.template)
                    .campuses.map((campus) => (
                        <MenuItem key={campus.id} value={campus.id}>
                            {campus.label}
                        </MenuItem>
                    ))}
            </Select>
        </div>
    );
}
