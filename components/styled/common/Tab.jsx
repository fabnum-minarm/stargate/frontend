import { withStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import { alpha } from '@material-ui/core/styles';

const AntTab = withStyles((theme) => ({
    root: {
        textTransform: 'none',
        color: theme.palette.common.black,
        fontSize: '0.875rem',
        minWidth: 150,
        marginRight: theme.spacing(5),
        '&:hover': {
            opacity: 1,
            color: theme.palette.primary.main,
            backgroundColor: alpha(theme.palette.primary.main, 0.05)
        },
        '&$selected': {
            color: theme.palette.primary.main,
            fontWeight: 'bold',
            backgroundColor: alpha(theme.palette.primary.main, 0)
        },
        borderColor: theme.palette.primary.main,
        borderRadius: '5%'
    },
    selected: {},
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
    // Many props needed by Material-UI
    // eslint-disable-next-line react/jsx-props-no-spreading
}))((props) => <Tab disableRipple {...props} />);

export default AntTab;
