import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { HANDLE_REQUEST, HANDLE_VISITOR } from '../../utils/permissions';
import { activeRoleCacheVar } from '../../lib/apollo/cache';

import { isAdmin, isSuperAdmin } from '../../utils/permissions';

const useStyles = makeStyles((theme) => ({
    root: {
        color: theme.palette.common.black,
        '&:hover': {
            color: theme.palette.primary.main,
            backgroundColor: theme.palette.common.white
        }
    },
    font: {
        fontWeight: '500'
    },
    selected: {
        color: theme.palette.primary.main,
        backgroundColor: theme.palette.common.white,
        '&:hover': {
            color: theme.palette.primary.dark,
            backgroundColor: theme.palette.common.white
        }
    },
    selectItem: {
        marginLeft: '-19px',
        marginRight: '19px'
    },
    selectIcon: {
        borderLeft: `3px solid ${theme.palette.primary.main}`,
        marginRight: '17px'
    },
    icon: {
        color: 'inherit'
    },
    nested: {
        paddingLeft: theme.spacing(6),
        paddingRight: theme.spacing(4)
    },
    gutters: {
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4)
    }
}));

export default function NormalListItem({ item, action, pathname, label, child }) {
    const classes = useStyles();
    const isSelected = React.useMemo(() => {
        if (item.path === '/') return pathname === item.path;

        if (
            (isSuperAdmin(activeRoleCacheVar().role) || isAdmin(activeRoleCacheVar().role)) &&
            pathname === '/'
        )
            return item.path === '/administration/utilisateurs';

        if (
            activeRoleCacheVar().role.permissions.includes(HANDLE_VISITOR) &&
            !activeRoleCacheVar().role.permissions.includes(HANDLE_REQUEST) &&
            pathname === '/'
        )
            return item.path === '/demandes';

        return pathname.includes(item.path);
    }, [pathname]);

    const handleClick = () => action(item.path);

    return (
        <>
            <ListItem
                button
                disableRipple
                classes={{
                    root: classes.root,
                    gutters: child ? classes.nested : classes.gutters
                }}
                className={`${isSelected ? classes.selected : ''}`}
                onClick={handleClick}>
                <ListItemIcon
                    classes={{ root: classes.icon }}
                    className={isSelected && !child ? classes.selectItem : ''}>
                    {isSelected && !child && <div className={classes.selectIcon} />}
                    {React.createElement(item.icon)}
                </ListItemIcon>
                <ListItemText
                    disableTypography
                    classes={{ root: classes.font }}
                    primary={label || item.label}
                />
            </ListItem>
        </>
    );
}

NormalListItem.propTypes = {
    child: PropTypes.bool,
    item: PropTypes.object.isRequired,
    action: PropTypes.func.isRequired,
    pathname: PropTypes.string,
    label: PropTypes.string
};

NormalListItem.defaultProps = {
    pathname: '',
    label: null,
    child: false
};
