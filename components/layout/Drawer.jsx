import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { gql, useQuery } from '@apollo/client';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { alpha } from '@material-ui/core/styles';
import { checkUrlPermissions, HANDLE_REQUEST, HANDLE_VISITOR } from '../../utils/permissions';
import NormalListItem from './NormalListItem';

import DescriptionIcon from '@material-ui/icons/Description';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import PeopleIcon from '@material-ui/icons/People';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';

import TreatmentsIcon from '../icons/MyTreatmentsIcon';
import MyRequestsIcon from '../icons/MyRequestsIcon';
import ContactUsIcon from '../icons/ContactUsIcon';
import NewDemandIcon from '../icons/NewDemandIcon';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import { GET_ACTIVE_ROLE } from '../../lib/apollo/queries';
import { campusIdVar } from '../../lib/apollo/cache';
import { isAdmin, isSuperAdmin } from '../../utils/permissions';

const useStyles = (drawerWidth) =>
    makeStyles((theme) => ({
        drawer: {
            width: drawerWidth
        },
        drawerPaper: {
            width: drawerWidth,
            background: theme.palette.background.layout,
            borderRight: 'none'
        },
        appName: {
            width: 200
        },
        gutter: {
            paddingLeft: theme.spacing(4),
            paddingRight: theme.spacing(4)
        },
        baseLabel: {
            display: 'flex',
            flexDirection: 'row',
            borderRadius: '4px',
            margin: '0 20px 20px 20px',
            padding: 6,
            backgroundColor: theme.palette.background.layoutDark
        },
        baseLabelIcon: {
            minWidth: '56px'
        },
        baseLabelText: {
            textTransform: 'uppercase'
        },
        toolBar: {
            minHeight: 64,
            display: 'table-cell',
            textAlign: 'center',
            padding: '30px',
            verticalAlign: 'middle'
        },
        selected: {
            color: theme.palette.primary.main,
            '&:hover': {
                color: theme.palette.primary.dark,
                backgroundColor: alpha(theme.palette.primary.light, 0.15)
            }
        },
        gridLogos: {
            bottom: 40,
            width: '100%',
            position: 'absolute',
            display: 'flex',
            justifyContent: 'center'
        },
        icon: {
            color: 'inherit'
        }
    }))();

const menu = [
    { label: 'index', path: '/', icon: TreatmentsIcon },
    {
        label: 'Mes demandes',
        path: '/demandes',
        icon: MyRequestsIcon
    },
    {
        label: 'Nouvelle demande',
        path: '/nouvelle-demande',
        icon: NewDemandIcon
    },
    {
        label: 'Utilisateurs',
        path: '/administration/utilisateurs',
        icon: PeopleIcon
    },
    {
        label: 'Base',
        path: '/administration/base',
        icon: DescriptionIcon
    },
    {
        label: "Zone d'influence",
        path: '/administration/zoneinfluence',
        icon: DescriptionIcon
    },
    {
        label: 'Rechercher',
        path: '/visiteurs',
        icon: SearchIcon
    }
];

function rootNameByRolePermission(role) {
    switch (true) {
        case !role.permissions.length:
            return 'Recherche';
        case isAdmin(role):
            return 'Utilisateurs';
        case role.permissions.includes(HANDLE_VISITOR):
            return 'Mes traitements';
        case role.permissions.includes(HANDLE_REQUEST):
            return 'Mes demandes';
        default:
            return 'Mes traitements';
    }
}

function getBaseLabel(campus, role) {
    return isSuperAdmin(role) ? 'ADMINISTRATEUR' : campus?.label ?? '';
}

export const GET_MENU_DRAWER = gql`
    query getMenuDrawer($campusId: String!) {
        campusId @client @export(as: "campusId")
        activeRoleCache @client {
            role {
                template
                permissions
            }
            unit
            unitLabel
        }
        getCampus(id: $campusId) {
            id
            label
        }
    }
`;

export default function DrawerTemplate({ drawerWidth }) {
    const classes = useStyles(drawerWidth);
    const router = useRouter();
    const campusId = campusIdVar();

    const { data } = useQuery(campusId.length ? GET_MENU_DRAWER : GET_ACTIVE_ROLE);

    if (!data) {
        // TODO rework data fetching or add a loading in menu when switching role
        return '';
    }

    let menuModified = menu.map(function (item) {
        if (item.label === 'index' && data.activeRoleCache.role.template === 'ROLE_HOST')
            return false;
        if (
            item.label === 'index' &&
            (isAdmin(data.activeRoleCache.role) || isSuperAdmin(data.activeRoleCache.role))
        )
            return false;
        else return item;
    });

    return (
        <Drawer
            className={classes.drawer}
            classes={{
                paper: classes.drawerPaper
            }}
            variant="permanent"
            anchor="left">
            <div className={classes.toolBar}>
                <img src="/img/logo.svg" alt="STARGATE" className={classes.appName} />
            </div>

            <div className={classes.baseLabel}>
                <RoomOutlinedIcon className={classes.baseLabelIcon} />
                <Typography variant="subtitle2" className={classes.baseLabelText}>
                    {getBaseLabel(data?.getCampus, data.activeRoleCache.role)}
                </Typography>
            </div>
            <List>
                {data?.activeRoleCache &&
                    menuModified.map(
                        (item) =>
                            checkUrlPermissions(
                                data.activeRoleCache.role.permissions,
                                item.path
                            ) && (
                                <NormalListItem
                                    key={item.label}
                                    item={item}
                                    action={(path) => router.push(path)}
                                    label={
                                        item.label ===
                                            rootNameByRolePermission(data.activeRoleCache.role) ||
                                        item.label === 'index'
                                            ? rootNameByRolePermission(data.activeRoleCache.role)
                                            : null
                                    }
                                    pathname={router.pathname}
                                />
                            )
                    )}
                <Divider variant="middle" />
                <NormalListItem
                    item={{ label: 'À propos', path: '/a-propos', icon: ThumbUpAltIcon }}
                    action={(path) => router.push(path)}
                    pathname={router.pathname}
                />
                <NormalListItem
                    item={{
                        label: 'Contactez-nous',
                        path: '/contactez-nous',
                        icon: ContactUsIcon
                    }}
                    action={(path) => router.push(path)}
                    pathname={router.pathname}
                />
            </List>
            <div className={classes.gridLogos}>
                <img src="/img/logo-fabrique-numerique.svg" alt="Logo de la fabrique numérique" />
            </div>
        </Drawer>
    );
}

DrawerTemplate.propTypes = {
    drawerWidth: PropTypes.number.isRequired
};
