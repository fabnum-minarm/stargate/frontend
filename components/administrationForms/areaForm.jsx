import React, { useState } from 'react';

import PropTypes from 'prop-types';

import { Button, Divider, Paper, TextField } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';

const useStyles = makeStyles(() => ({
    addButton: {
        width: 109,
        height: 50,
        marginTop: 5,
        marginLeft: 20,
        padding: '10px 27px 10px 26px',
        borderRadius: 25
    },
    areaData: {
        padding: 15,
        minHeight: 170
    },
    listCampuses: {},
    areaCampusesList: {
        marginTop: 20,
        width: '100%'
    }
}));

export default function AreaForm({ onSubmit, defaultValues, listCampuses }) {
    const [areaLabel, setAreaLabel] = useState(defaultValues.label);
    const [areaCampusesList, setAreasCampusesList] = useState(defaultValues.areaCampusesList);
    const classes = useStyles();

    const handleSubmit = () => {
        onSubmit({ areaLabel, areaCampusesList: areaCampusesList.map((c) => c.id) });
    };

    const handleDelete = (index) => {
        const newChecked = [...areaCampusesList];
        newChecked.splice(index, 1);
        setAreasCampusesList(newChecked);
    };

    const handleToggle = (value) => () => {
        const currentIndex = areaCampusesList.findIndex((c) => c.id === value.id);
        const newChecked = [...areaCampusesList];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }
        setAreasCampusesList(newChecked);
    };

    return (
        <>
            <Paper elevation={2} className={classes.areaData}>
                <TextField
                    label="Nom"
                    variant="outlined"
                    value={areaLabel}
                    onChange={(event) => setAreaLabel(event.target.value)}
                />
                <Button
                    variant="outlined"
                    color="primary"
                    className={classes.addButton}
                    onClick={handleSubmit}>
                    Valider
                </Button>
                <div className={classes.areaCampusesList}>
                    <Typography className={classes.heading}>Bases :</Typography>
                    {areaCampusesList.map((campus, i) => (
                        <Chip
                            color="primary"
                            variant="outlined"
                            style={{ margin: '2px' }}
                            key={campus.id}
                            label={campus.label}
                            onDelete={() => handleDelete(i)}
                        />
                    ))}
                </div>
            </Paper>
            <List className={classes.listCampuses}>
                {listCampuses.map((campus) => {
                    const labelId = `checkbox-list-label-${campus.id}`;
                    return (
                        <div key={campus.id}>
                            <ListItem dense data-testid={`listitem-bases-${campus.id}`}>
                                <ListItemIcon>
                                    <Checkbox
                                        color="primary"
                                        edge="start"
                                        checked={!!areaCampusesList.find((c) => c.id === campus.id)}
                                        tabIndex={-1}
                                        onChange={handleToggle(campus)}
                                        disableRipple
                                        inputProps={{ 'aria-labelledby': labelId }}
                                    />
                                </ListItemIcon>
                                <ListItemText
                                    id={campus.id}
                                    primary={`${campus.trigram} - ${campus.label}`}
                                />
                            </ListItem>
                            <Divider light variant="middle" />
                        </div>
                    );
                })}
            </List>
        </>
    );
}

AreaForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    defaultValues: PropTypes.shape({
        label: PropTypes.string.isRequired,
        areaCampusesList: PropTypes.array.isRequired
    }).isRequired,
    listCampuses: PropTypes.array.isRequired
};
