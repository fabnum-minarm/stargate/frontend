import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { Controller, useForm } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import { checkMailFormat, createUserData } from '../../utils/mappers/createUserFromMail';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import WarningIcon from '@material-ui/icons/Warning';
import {
    FormControlLabel,
    IconButton,
    InputLabel,
    MenuItem,
    Radio,
    RadioGroup,
    Typography
} from '@material-ui/core';
import GridList from '@material-ui/core/ImageList';
import GridListTile from '@material-ui/core/ImageListItem';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import { ADD_USER_ROLE, CREATE_USER, DELETE_ROLE } from '../../lib/apollo/mutations';
import { FIND_USER_BY_MAIL, LIST_AREAS, LIST_USERS } from '../../lib/apollo/queries';
import { useSnackBar } from '../../lib/hooks/snackbar';
import { VALIDATION_SCOPE_AREA } from '../../utils/constants/enums';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: 40
    },
    fieldTitle: {
        marginBottom: 17
    },
    fieldSection: {
        maxWidth: 400,
        marginRight: 40
    },
    fieldInput: {
        backgroundColor: theme.palette.common.white
    },
    listUsers: {
        marginTop: 10,
        marginLeft: 20
    },
    userListItem: {
        borderRadius: 4,
        paddingLeft: 5,
        '&:hover': {
            backgroundColor: theme.palette.background.layoutDark,
            transition: '.4s'
        }
    },
    warningIcon: {
        width: 50
    },
    icon: {
        color: 'rgba(0, 0, 0, 0.25)',
        padding: '0 !important',
        '&:hover': {
            color: theme.palette.primary.main
        }
    },
    iconSvg: {
        height: 20
    },
    addButton: {
        width: 109,
        height: 50,
        marginTop: 5,
        padding: '10px 27px 10px 26px',
        borderRadius: 25
    },
    selectArea: {
        width: 220,
        marginRight: 25
    },
    radioGroup: {
        height: 56,
        flexDirection: 'row',
        '&> label': {
            marginRight: '60px'
        }
    }
}));

const mapRoleDataRequest = (roleData, campuses) => ({
    role: roleData.role.template,
    campuses: campuses.map((c) => ({ id: c.id, label: c.label })),
    unit: roleData.unit ? { id: roleData.unit.id, label: roleData.unit.label } : null,
    area: roleData.area ? { id: roleData.area.id, label: roleData.area.label } : null
});

const CreateRoleField = ({
    usersList,
    roleData,
    canDelete,
    isOptional,
    manualRefresh,
    children,
    mailDomain
}) => {
    const classes = useStyles();
    const { addAlert } = useSnackBar();
    const client = useApolloClient();
    const { control, handleSubmit, errors, setValue } = useForm();
    const isRoleAreaScope = [VALIDATION_SCOPE_AREA, VALIDATION_SCOPE_AREA].includes(
        roleData.role?.validator?.scope
    );
    const [isRoleInArea, setIsRoleInArea] = useState(false);
    const [selectedArea, setSelectedArea] = useState('');

    const { data: dataListAreas } = useQuery(LIST_AREAS, {
        skip: !isRoleAreaScope,
        onCompleted: (d) => setSelectedArea(d.listAreas.list[0])
    });

    const [deleteUserRoleReq] = useMutation(DELETE_ROLE, {
        update: (cache, { data: { deleteUserRole: deletedUser } }) => {
            let variables = {
                campus: roleData.campus.id,
                hasRole: { role: roleData.role.template }
            };
            if (roleData.unit) {
                variables = {
                    ...variables,
                    hasRole: {
                        ...variables.hasRole,
                        unit: roleData.unit.id
                    }
                };
            }
            const currentUsers = cache.readQuery({
                query: LIST_USERS,
                variables
            });
            const updatedTotal = currentUsers.listUsers.meta.total - 1;
            const updatedUsers = {
                ...currentUsers,
                listUsers: {
                    ...currentUsers.listUsers,
                    list: currentUsers.listUsers.list.filter((user) => user.id !== deletedUser.id),
                    meta: {
                        ...currentUsers.listUsers.meta,
                        total: updatedTotal
                    }
                }
            };
            cache.writeQuery({
                query: LIST_USERS,
                variables,
                data: updatedUsers
            });

            if (manualRefresh) {
                manualRefresh(roleData);
            }
        }
    });

    const addRoleUpdate = (cache, user) => {
        let variables = {
            campus: roleData.campus.id,
            hasRole: { role: roleData.role.template }
        };
        if (roleData.unit) {
            variables = {
                ...variables,
                hasRole: {
                    ...variables.hasRole,
                    unit: roleData.unit.id
                }
            };
        }
        const currentUsers = cache.readQuery({
            query: LIST_USERS,
            variables
        });
        const updatedTotal = currentUsers.listUsers.meta.total + 1;
        const updatedUsers = {
            ...currentUsers,
            listUsers: {
                ...currentUsers.listUsers,
                list: [...currentUsers.listUsers.list, user],
                meta: {
                    ...currentUsers.listUsers.meta,
                    total: updatedTotal
                }
            }
        };
        cache.writeQuery({
            query: LIST_USERS,
            variables,
            data: updatedUsers
        });

        if (manualRefresh) {
            manualRefresh(roleData);
        }
    };

    const [addUserRole] = useMutation(ADD_USER_ROLE, {
        update: (cache, { data: { addUserRole: updatedUser } }) => addRoleUpdate(cache, updatedUser)
    });
    const [createUser] = useMutation(CREATE_USER);

    const submitCreateUser = async (userData) => {
        try {
            const {
                data: { createUser: user }
            } = await createUser({ variables: { user: userData } });
            addAlert({
                message: `L'utilisateur pour le rôle ${roleData.role.label} a bien été créé`,
                severity: 'success'
            });
            setValue('userEmail', '');
            return user;
        } catch (e) {
            switch (true) {
                case e.message.includes('User already exists'):
                    addAlert({
                        message: 'Un utilisateur est déjà enregistré avec cet e-mail',
                        severity: 'error'
                    });
                    break;
                case e.message.includes('User validation failed: email.original: '):
                    addAlert({
                        message: "Erreur, veuillez vérifier le domaine de l'adresse e-mail",
                        severity: 'warning'
                    });
                    break;
                default:
                    addAlert({
                        message: 'Erreur serveur, merci de réessayer',
                        severity: 'warning'
                    });
                    break;
            }
            return false;
        }
    };

    const submitAddUserRole = async (user) => {
        const userRole = isRoleInArea
            ? mapRoleDataRequest({ area: selectedArea, ...roleData }, selectedArea.campuses)
            : mapRoleDataRequest(roleData, [roleData.campus]);
        try {
            const {
                data: {
                    addUserRole: { email }
                }
            } = await addUserRole({
                variables: { roleData: userRole, id: user.id, campusId: roleData.campus.id }
            });
            addAlert({
                message: `Le rôle ${roleData.role.label} à bien été ajouté à ${email.original}`,
                severity: 'success'
            });
            return setValue('userEmail', '');
        } catch {
            addAlert({
                message: "Erreur lors de l'ajout du rôle à l'utilisateur",
                severity: 'error'
            });
        }
    };

    const handleDeleteUser = async (id) => {
        try {
            await deleteUserRoleReq({
                variables: {
                    id,
                    roleData: {
                        role: roleData.role.template,
                        unit: roleData.unit
                    }
                }
            });
            addAlert({
                message: `Le rôle ${roleData.role.label} à bien été supprimé de l'utilisateur`,
                severity: 'success'
            });
        } catch {
            return null;
        }
    };

    const handleCreateUserWithRole = async (formData) => {
        const { data } = await client.query({
            query: FIND_USER_BY_MAIL,
            variables: {
                email: formData.userEmail
            },
            fetchPolicy: 'no-cache'
        });
        if (!data.findUser) {
            const userAdmin = createUserData(formData.userEmail);
            const user = await submitCreateUser(userAdmin);
            return submitAddUserRole(user);
        } else {
            return submitAddUserRole(data.findUser);
        }
    };

    const displayEmail = (mail) => {
        return mail.length > 30 ? mail.slice(0, 30) + '...' : mail;
    };

    return (
        (dataListAreas || !isRoleAreaScope) && (
            <Grid className={classes.root}>
                <form onSubmit={handleSubmit(handleCreateUserWithRole)}>
                    <Grid>
                        <Grid container className={classes.fieldTitle}>
                            {!usersList.length && !isOptional && (
                                <WarningIcon className={classes.warningIcon} />
                            )}
                            {children}
                        </Grid>
                        <Grid container item sm={12}>
                            <Grid item className={classes.fieldSection}>
                                <Controller
                                    as={
                                        <TextField
                                            label="Adresse mail"
                                            variant="outlined"
                                            fullWidth
                                            className={classes.fieldInput}
                                            error={Object.prototype.hasOwnProperty.call(
                                                errors,
                                                'userEmail'
                                            )}
                                            helperText={
                                                errors.userEmail && errors.userEmail.message
                                            }
                                        />
                                    }
                                    control={control}
                                    name="userEmail"
                                    defaultValue=""
                                    rules={{
                                        validate: (value) =>
                                            checkMailFormat(value, mailDomain) ||
                                            `L'email doit être au format nom.prenom@domain (${mailDomain.join(
                                                ', '
                                            )})`
                                    }}
                                />
                                <Grid item className={classes.listUsers}>
                                    <GridList cols={1} rowHeight={25}>
                                        {usersList.map((user) => (
                                            <GridListTile key={user.id}>
                                                <Grid
                                                    container
                                                    justify="space-between"
                                                    className={classes.userListItem}>
                                                    <Grid item>
                                                        <Typography variant="body1">
                                                            {displayEmail(user.email.original)}
                                                        </Typography>
                                                    </Grid>
                                                    {canDelete && (
                                                        <Grid item>
                                                            <IconButton
                                                                aria-label="deleteUser"
                                                                onClick={() =>
                                                                    handleDeleteUser(user.id)
                                                                }
                                                                classes={{ root: classes.icon }}>
                                                                <PersonAddDisabledIcon
                                                                    classes={{
                                                                        root: classes.iconSvg
                                                                    }}
                                                                />
                                                            </IconButton>
                                                        </Grid>
                                                    )}
                                                </Grid>
                                            </GridListTile>
                                        ))}
                                    </GridList>
                                </Grid>
                            </Grid>
                            {isRoleAreaScope && (
                                <RadioGroup
                                    className={classes.radioGroup}
                                    aria-label="selectscope"
                                    name="isInScope">
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                checked={!isRoleInArea}
                                                onChange={() => {
                                                    setIsRoleInArea(false);
                                                }}
                                                color="primary"
                                            />
                                        }
                                        label={<Typography variant="body2">Base</Typography>}
                                    />
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                checked={isRoleInArea}
                                                onChange={() => setIsRoleInArea(true)}
                                                color="primary"
                                            />
                                        }
                                        label={
                                            <Typography variant="body2">
                                                Zone d&apos;influence
                                            </Typography>
                                        }
                                    />
                                </RadioGroup>
                            )}
                            {isRoleInArea && (
                                <FormControl variant="outlined" className={classes.selectArea}>
                                    <InputLabel id="select-area-label">
                                        Zone d&apos;influence
                                    </InputLabel>
                                    <Select
                                        id="select-area"
                                        labelId="select-area-label"
                                        label="Zone d'influence"
                                        value={selectedArea}
                                        onChange={(e) => setSelectedArea(e.target.value)}>
                                        {dataListAreas.listAreas.list.map((area) => (
                                            <MenuItem key={area.id} value={area}>
                                                {area.label}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            )}
                            {!isOptional && (
                                <Grid item>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.addButton}>
                                        Ajouter
                                    </Button>
                                </Grid>
                            )}
                        </Grid>
                    </Grid>
                </form>
            </Grid>
        )
    );
};

CreateRoleField.defaultProps = {
    usersList: [],
    canDelete: true,
    isOptional: false
};

CreateRoleField.propTypes = {
    mailDomain: PropTypes.arrayOf(PropTypes.string).isRequired,
    usersList: PropTypes.array,
    roleData: PropTypes.shape({
        role: PropTypes.shape.isRequired,
        unit: PropTypes.objectOf(PropTypes.string),
        campus: PropTypes.object
    }).isRequired,
    isOptional: PropTypes.bool,
    canDelete: PropTypes.bool,
    children: PropTypes.node.isRequired,
    manualRefresh: PropTypes.func
};

export default CreateRoleField;
