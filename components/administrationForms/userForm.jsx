import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Controller, useForm } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { Select } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import Link from 'next/link';
import { makeStyles } from '@material-ui/core/styles';
import { gql, useQuery, useLazyQuery } from '@apollo/client';
import classNames from 'classnames';
import { mapUserData, mapUserFormRolesList } from '../../utils/mappers/adminMappers';
import { isSuperAdmin } from '../../utils/permissions';
import { useSnackBar } from '../../lib/hooks/snackbar';
import { GET_CAMPUS, GET_CAMPUSES_LIST } from '../../lib/apollo/queries';
import { campusIdVar } from '../../lib/apollo/cache';
import { ROLES } from '../../utils/constants/enums';

import { useLogin } from '../../lib/loginContext';

import RoundButton from '../styled/common/roundButton';
import LoadingCircle from '../styled/animations/loadingCircle';

const useStyles = makeStyles((theme) => ({
    createUserForm: {
        padding: '60px'
    },
    formTextField: {
        marginTop: theme.spacing(1)
    },
    formSelect: {
        margin: theme.spacing(2),
        width: '40%'
    },
    formRadio: {
        width: '100%'
    },
    radioGroup: {
        flexDirection: 'row'
    },
    buttonsContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: '30px',
        '& button': {
            margin: '3px'
        }
    },
    errorText: {
        color: theme.palette.error.main
    }
}));

const GET_UNITS = gql`
    query listUnits($campusId: String!) {
        getCampus(id: $campusId) {
            id
            listUnits {
                list {
                    id
                    label
                }
            }
        }
    }
`;

const UserForm = ({ submitForm, defaultValues, userRole, type }) => {
    function guardCampusId() {
        if (!isSuperAdmin(userRole.role)) return campusIdVar();
        return defaultValues.campus ? defaultValues.campus.id : '';
    }
    const campusId = guardCampusId();
    const classes = useStyles();
    const { addAlert } = useSnackBar();
    const { handleSubmit, errors, control } = useForm();

    const [mappedRolesList, setMappedRolesList] = useState(null);
    const [listCampuses, setListCampuses] = useState(null);
    const [isUnitDisabled, setIsUnitDisabled] = useState(defaultValues.role === 'ROLE_BASIC_USER');

    const { activeRole } = useLogin();

    const handleMapRolesList = (list) => {
        setMappedRolesList(mapUserFormRolesList(list, userRole.role));
    };

    const isUnitValidation = activeRole.role.template === ROLES.ROLE_BASIC_UNIT_VALIDATION.role;

    const { data: dataCampuses, loading } = useQuery(GET_CAMPUSES_LIST, {
        skip: !isSuperAdmin(userRole.role),
        onCompleted: (d) => {
            setListCampuses(d.listCampuses.list);
        }
    });
    const { data: dataCampus } = useQuery(GET_CAMPUS, {
        variables: { id: campusId },
        skip: campusId === '',
        onCompleted: (d) => {
            if (!isSuperAdmin(userRole.role)) setListCampuses([d.getCampus]);

            handleMapRolesList(d.getCampus.roles);
        }
    });

    const [reqUnitsList, { data: dataUnits }] = useLazyQuery(GET_UNITS);

    const onSubmit = (data) => {
        const formData = { ...data };

        // if CU auto replace data
        if (isUnitValidation) {
            formData.role = defaultValues?.role ?? '';
            formData.campus = defaultValues?.campus.id ?? '';
            formData.unit = defaultValues?.unit.id ?? '';
        }

        if (mappedRolesList.noUnitRoles.find((r) => formData.role === r.id)) {
            delete formData.unit;
        }

        const campus = isSuperAdmin(userRole.role)
            ? dataCampuses.listCampuses.list.find((campus) => campus.id === formData.campus)
            : dataCampus.getCampus;

        if (formData.unit) {
            formData.unit = dataUnits.getCampus.listUnits.list.find((u) => formData.unit === u.id);
        }

        const mappedUserData = mapUserData(formData, campus);
        submitForm(mappedUserData);
    };

    const getListUnits = async (campusId) => {
        try {
            reqUnitsList({ variables: { campusId } });
        } catch (e) {
            addAlert({
                message: 'Une erreur est survenue au chargement de la liste des unités',
                severity: 'error'
            });
        }
    };

    const updateAffectationSublist = (e) => {
        if (e.target.value === 'ROLE_BASIC_USER') {
            setIsUnitDisabled(true);
        } else {
            setIsUnitDisabled(false);
        }
    };

    useEffect(() => {
        if (defaultValues.campus) {
            getListUnits(defaultValues.campus.id);
        }
    }, [defaultValues]);

    const filterRolesForCampus = (campusValue) => {
        return dataCampuses.listCampuses.list.find((campus) => campus.id === campusValue);
    };

    if (loading) return <LoadingCircle />;
    return (
        <form onSubmit={handleSubmit(onSubmit)} className={classes.createUserForm}>
            <Grid
                container
                item
                style={{ justifyContent: 'space-between' }}
                sm={12}
                xs={12}
                lg={12}>
                <Grid item sm={4} xs={4}>
                    <Grid item xs={12} sm={12}>
                        <Typography variant="subtitle2" gutterBottom>
                            Informations personnelles
                        </Typography>
                    </Grid>
                    <Grid>
                        <Controller
                            as={
                                <TextField
                                    label="Nom"
                                    inputProps={{ 'data-testid': 'create-user-lastname' }}
                                    error={Object.prototype.hasOwnProperty.call(errors, 'lastname')}
                                    helperText={errors.lastname && errors.lastname.message}
                                    className={classes.formTextField}
                                    fullWidth
                                />
                            }
                            rules={{
                                validate: (value) => value.trim() !== '' || 'Le nom est obligatoire'
                            }}
                            control={control}
                            name="lastname"
                            defaultValue={defaultValues.lastname ? defaultValues.lastname : ''}
                        />
                    </Grid>
                    <Grid>
                        <Controller
                            as={
                                <TextField
                                    label="Prénom"
                                    inputProps={{ 'data-testid': 'create-user-firstname' }}
                                    error={Object.prototype.hasOwnProperty.call(
                                        errors,
                                        'firstname'
                                    )}
                                    helperText={errors.firstname && errors.firstname.message}
                                    className={classes.formTextField}
                                    fullWidth
                                />
                            }
                            rules={{
                                validate: (value) =>
                                    value.trim() !== '' || 'Le prénom est obligatoire'
                            }}
                            control={control}
                            name="firstname"
                            defaultValue={defaultValues.firstname ? defaultValues.firstname : ''}
                        />
                    </Grid>
                    <Grid>
                        <Controller
                            as={
                                <TextField
                                    label="Adresse e-mail"
                                    inputProps={{
                                        'data-testid': 'create-user-email',
                                        type: 'email'
                                    }}
                                    error={Object.prototype.hasOwnProperty.call(errors, 'email')}
                                    helperText={errors.email && errors.email.message}
                                    className={classes.formTextField}
                                    fullWidth
                                />
                            }
                            rules={{
                                validate: (value) =>
                                    value.trim() !== '' || "L'adresse e-mail est obligatoire"
                            }}
                            control={control}
                            name="email"
                            defaultValue={defaultValues.email ? defaultValues.email : ''}
                        />
                    </Grid>
                </Grid>
                {!isUnitValidation && (
                    <Grid item sm={7} xs={7}>
                        <Grid
                            container
                            item
                            style={{ justifyContent: 'space-between' }}
                            xs={12}
                            sm={12}>
                            <Typography variant="subtitle2" gutterBottom>
                                Rôle
                            </Typography>
                        </Grid>
                        {/* todo: check and fix */}
                        <FormControl
                            variant="outlined"
                            error={Object.prototype.hasOwnProperty.call(errors, 'roles')}
                            className={classes.formRadio}>
                            <Controller
                                as={
                                    <RadioGroup
                                        className={classNames(classes.radioGroup, {
                                            [classes.errorText]: errors.role
                                        })}
                                        aria-label="vip">
                                        {mappedRolesList?.rolesList?.map((roleItem) => (
                                            <FormControlLabel
                                                key={roleItem.template}
                                                value={roleItem.template}
                                                control={<Radio color="primary" />}
                                                label={roleItem.label}
                                                labelPlacement="start"
                                                disabled={mappedRolesList?.rolesList?.length === 1}
                                                onChange={updateAffectationSublist}
                                            />
                                        ))}
                                    </RadioGroup>
                                }
                                control={control}
                                rules={{ required: 'Le rôle est obligatoire' }}
                                name="role"
                                defaultValue={defaultValues?.role ?? ''}
                            />
                            {errors.role && (
                                <FormHelperText className={classes.errorText}>
                                    Le rôle obligatoire
                                </FormHelperText>
                            )}
                        </FormControl>
                        <Grid
                            container
                            item
                            style={{ justifyContent: 'space-between' }}
                            xs={12}
                            sm={12}>
                            <Typography variant="subtitle2" gutterBottom>
                                Affectation
                            </Typography>
                        </Grid>
                        <Grid>
                            <Grid container style={{ justifyContent: 'space-between' }}>
                                <FormControl
                                    variant="outlined"
                                    error={Object.prototype.hasOwnProperty.call(errors, 'campus')}
                                    className={classes.formSelect}>
                                    <InputLabel id="select-outlined-label">Base</InputLabel>

                                    {listCampuses && (
                                        <Controller
                                            as={
                                                <Select
                                                    fullWidth
                                                    labelId="create-user-campus"
                                                    label="Base"
                                                    id="campus">
                                                    {listCampuses.map((campus) => (
                                                        <MenuItem key={campus.id} value={campus.id}>
                                                            {campus.label}
                                                        </MenuItem>
                                                    ))}
                                                </Select>
                                            }
                                            control={control}
                                            onChange={([selected]) => {
                                                getListUnits(selected.target.value);
                                                handleMapRolesList(
                                                    filterRolesForCampus(selected.target.value)
                                                        .roles
                                                );
                                                return selected;
                                            }}
                                            name="campus"
                                            defaultValue={defaultValues?.campus?.id ?? ''}
                                            rules={{ required: true }}
                                        />
                                    )}

                                    {errors.campus && (
                                        <FormHelperText className={classes.errorText}>
                                            Base obligatoire
                                        </FormHelperText>
                                    )}
                                </FormControl>
                                {dataUnits &&
                                    !isUnitDisabled &&
                                    dataUnits.getCampus?.listUnits?.list && (
                                        <FormControl
                                            variant="outlined"
                                            error={Object.prototype.hasOwnProperty.call(
                                                errors,
                                                'unit'
                                            )}
                                            className={classes.formSelect}>
                                            <InputLabel id="select-outlined-label">
                                                Unité
                                            </InputLabel>
                                            <Controller
                                                as={
                                                    <Select
                                                        labelId="create-user-unit"
                                                        id="unit"
                                                        label="Unité">
                                                        {dataUnits.getCampus.listUnits.list.map(
                                                            (unit) => (
                                                                <MenuItem
                                                                    key={unit.id}
                                                                    value={unit.id}>
                                                                    {unit.label}
                                                                </MenuItem>
                                                            )
                                                        )}
                                                    </Select>
                                                }
                                                control={control}
                                                name="unit"
                                                defaultValue={defaultValues?.unit?.id ?? ''}
                                                rules={{ required: true }}
                                            />
                                            {errors.unit && (
                                                <FormHelperText className={classes.errorText}>
                                                    Unité obligatoire
                                                </FormHelperText>
                                            )}
                                        </FormControl>
                                    )}
                            </Grid>
                        </Grid>
                    </Grid>
                )}
            </Grid>
            <Grid
                item
                sm={!isUnitValidation ? 12 : 4}
                xs={!isUnitValidation ? 12 : 4}
                className={classes.buttonsContainer}>
                <Link href="/administration/utilisateurs">
                    <RoundButton
                        variant="outlined"
                        color="primary"
                        className={classes.buttonCancel}>
                        Annuler
                    </RoundButton>
                </Link>
                <RoundButton type="submit" variant="contained" color="primary">
                    {type === 'create' ? 'Créer' : 'Modifier'}
                </RoundButton>
            </Grid>
        </form>
    );
};

UserForm.propTypes = {
    submitForm: PropTypes.func.isRequired,
    defaultValues: PropTypes.shape({
        lastname: PropTypes.string,
        firstname: PropTypes.string,
        email: PropTypes.string,
        role: PropTypes.string,
        campus: PropTypes.objectOf(PropTypes.string),
        unit: PropTypes.objectOf(PropTypes.string)
    }).isRequired,
    userRole: PropTypes.shape({
        role: PropTypes.object.isRequired,
        campus: PropTypes.string
    }).isRequired,
    type: PropTypes.string.isRequired
};

export default UserForm;
