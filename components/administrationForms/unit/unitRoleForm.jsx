import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { CreateRoleField } from '../../index';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { GOUV_DOMAIN_MAIL } from '../../../utils/mappers/createUserFromMail';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: `20px 10px 23px 50px`,
        backgroundColor: theme.palette.background.layout,
        borderRadius: 4
    },
    firstField: {
        marginBottom: 20
    }
}));

const UnitRoleForm = ({ unit, campus, listUsersByRole, refreshList }) => {
    const classes = useStyles();
    const isRoleInWorkflow = (roleId) =>
        unit.workflow.steps.find((step) => step.role.template === roleId);

    return (
        <Grid className={classes.root}>
            {Object.entries(listUsersByRole).map(([, { role, list }]) => (
                <CreateRoleField
                    key={role.template}
                    className={classes.firstField}
                    roleData={{
                        role,
                        unit: { id: unit.id, label: unit.label },
                        campus: { id: campus.id, label: campus.label }
                    }}
                    usersList={list}
                    mailDomain={[GOUV_DOMAIN_MAIL]}
                    canDelete={
                        !isRoleInWorkflow(role.template) ||
                        (isRoleInWorkflow(role.template) && list.length > 1)
                    }
                    manualRefresh={refreshList}>
                    <Typography variant="body1" style={{ fontWeight: 'bold' }}>
                        {role.label}
                    </Typography>
                </CreateRoleField>
            ))}
        </Grid>
    );
};

UnitRoleForm.propTypes = {
    unit: PropTypes.object.isRequired,
    campus: PropTypes.object.isRequired,
    listUsersByRole: PropTypes.array.isRequired,
    refreshList: PropTypes.func.isRequired
};

export default UnitRoleForm;
