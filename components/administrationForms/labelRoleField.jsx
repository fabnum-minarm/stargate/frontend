import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { Controller, useForm } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation } from '@apollo/client';
import { UPDATE_CAMPUS_ROLE, ADD_CAMPUS_ROLE } from '../../lib/apollo/mutations';
import { useSnackBar } from '../../lib/hooks/snackbar';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: 40
    },
    fieldInput: {
        backgroundColor: theme.palette.common.white
    },
    fieldTitle: {
        marginBottom: 17,
        display: 'flex'
    },
    fieldSection: {
        maxWidth: 400,
        marginRight: 40
    },
    labelSection: {
        marginTop: 30
    },
    labelTitle: {
        marginBottom: 20,
        fontWeight: 'bold'
    },
    boldTitle: {
        fontWeight: 'bold'
    },
    italicTitle: {
        fontStyle: 'italic',
        paddingLeft: 20
    },
    editButton: {
        width: 109,
        height: 50
    }
}));

function LabelRoleField({ listRoles, roleData, isEdit }) {
    const { addAlert } = useSnackBar();
    const classes = useStyles();
    const getTags = () => {
        let tags = [];
        roleData.role.tags.forEach((tag) =>
            tags.push({
                label: tag.label,
                primary: tag.primary,
                value: tag.value
            })
        );
        return tags;
    };
    const { control, handleSubmit, errors, setValue } = useForm();
    const [updateCampusRole] = useMutation(UPDATE_CAMPUS_ROLE);
    const existingLabels = listRoles.map((r) => r.label);
    const existingShortLabels = listRoles.map((r) => r.shortLabel);
    const submitEditRoleCampus = async (formData) => {
        try {
            if (formData.roleLabel === '' && formData.roleShortLabel === '')
                return addAlert({
                    message: 'Veuillez saisir une modification',
                    severity: 'error'
                });
            await updateCampusRole({
                variables: {
                    id: roleData.campus.id,
                    shortLabel: roleData.role.shortLabel,
                    role: {
                        label: formData.roleLabel !== '' ? formData.roleLabel : roleData.role.label,
                        template: roleData.role.template,
                        tags: getTags(),
                        shortLabel:
                            formData.roleShortLabel !== ''
                                ? formData.roleShortLabel
                                : roleData.role.shortLabel
                    }
                }
            });
            setValue('roleShortLabel', '');
            setValue('roleLabel', '');
            return addAlert({
                message: `Le rôle à bien été mis à jour`,
                severity: 'success'
            });
        } catch {
            addAlert({
                message: 'Erreur lors de la modification du label de rôle',
                severity: 'error'
            });
        }
    };

    const [addCampusRole] = useMutation(ADD_CAMPUS_ROLE);
    const addOrEditButton = () => {
        return isEdit ? 'Modifier' : 'Ajouter';
    };

    const addOrEdit = (formData) => {
        return isEdit ? submitEditRoleCampus(formData) : submitAddRoleCampus(formData);
    };

    const submitAddRoleCampus = async (formData) => {
        try {
            await addCampusRole({
                variables: {
                    id: roleData.data.getCampus.id,
                    role: {
                        label: formData.roleLabel,
                        template: roleData.role.template,
                        shortLabel: formData.roleShortLabel
                    }
                }
            });

            addAlert({
                message: `Le rôle ${formData.roleLabel} a bien été ajouté`,
                severity: 'success'
            });
            setValue('roleShortLabel', '');
            return setValue('roleLabel', '');
        } catch {
            addAlert({
                message: "Erreur lors de l'ajout de rôle",
                severity: 'error'
            });
        }
    };
    return (
        <Grid className={classes.root}>
            <form onSubmit={handleSubmit(addOrEdit)}>
                <Grid container direction="row" item sm={12} alignItems="flex-end">
                    <Grid item sm={4}>
                        <Grid container item sm={12} className={classes.fieldTitle}>
                            <Typography variant="body1" className={classes.boldTitle}>
                                Label validateur
                            </Typography>
                            {!isEdit && (
                                <Typography variant="body1" className={classes.italicTitle}>
                                    Requis
                                </Typography>
                            )}
                        </Grid>
                        <Grid container item sm={12}>
                            <Grid item md={12} className={classes.fieldSection}>
                                <Controller
                                    as={
                                        <TextField
                                            label={!isEdit ? '' : roleData.role.label}
                                            variant="outlined"
                                            fullWidth
                                            inputProps={{ maxLength: 40 }}
                                            className={classes.fieldInput}
                                            error={Object.prototype.hasOwnProperty.call(
                                                errors,
                                                'roleLabel'
                                            )}
                                            helperText={
                                                errors.roleLabel && errors.roleLabel.message
                                            }
                                        />
                                    }
                                    control={control}
                                    name="roleLabel"
                                    defaultValue=""
                                    rules={{
                                        validate: {
                                            valide: (value) =>
                                                value.trim() !== '' ||
                                                isEdit === true ||
                                                'Veuillez saisir un label',

                                            unique: (value) =>
                                                !existingLabels.includes(value.trim()) ||
                                                'Ce label existe déjà'
                                        }
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item sm={4}>
                        <Grid item sm={12}>
                            <Grid item className={classes.fieldTitle}>
                                <Typography variant="body1" className={classes.boldTitle}>
                                    Label Court
                                </Typography>
                                {!isEdit && (
                                    <Typography variant="body1" className={classes.italicTitle}>
                                        Requis
                                    </Typography>
                                )}
                            </Grid>
                            <Grid item className={classes.fieldSection}>
                                <Controller
                                    as={
                                        <TextField
                                            label={!isEdit ? '' : roleData.role.shortLabel}
                                            variant="outlined"
                                            fullWidth
                                            inputProps={{ maxLength: 5 }}
                                            className={classes.fieldInput}
                                            error={Object.prototype.hasOwnProperty.call(
                                                errors,
                                                'roleShortLabel'
                                            )}
                                            helperText={
                                                errors.roleShortLabel &&
                                                errors.roleShortLabel.message
                                            }
                                        />
                                    }
                                    control={control}
                                    name="roleShortLabel"
                                    defaultValue=""
                                    rules={{
                                        validate: {
                                            valide: (value) =>
                                                value.trim() !== '' ||
                                                isEdit === true ||
                                                'Veuillez saisir un label court',
                                            unique: (value) =>
                                                !existingShortLabels.includes(value.trim()) ||
                                                'Ce label court existe déjà'
                                        }
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item sm={2} md={2}>
                        <Button
                            type="submit"
                            variant="outlined"
                            color="primary"
                            className={classes.editButton}>
                            {addOrEditButton()}
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Grid>
    );
}

LabelRoleField.defaultProps = {
    isEdit: false
};

LabelRoleField.propTypes = {
    roleData: PropTypes.shape({
        role: PropTypes.shape.isRequired,
        unit: PropTypes.objectOf(PropTypes.string),
        campus: PropTypes.object,
        shortLabel: PropTypes.string,
        data: PropTypes.shape.isRequired
    }).isRequired,
    isEdit: PropTypes.bool,
    listRoles: PropTypes.array.isRequired,
    children: PropTypes.node
};

export default LabelRoleField;
