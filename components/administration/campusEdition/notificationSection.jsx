import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Button, TextField, Typography, Grid, FormControlLabel, Switch } from '@material-ui/core';
import { gql, useMutation } from '@apollo/client';

import { makeStyles } from '@material-ui/core/styles';
import { useSnackBar } from '../../../lib/hooks/snackbar';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: `20px 10px 23px 50px`,
        backgroundColor: theme.palette.background.layout,
        borderRadius: 4
    },
    row: {
        marginBottom: '17px'
    },
    editButton: {
        width: 109,
        height: 50,
        marginTop: 5,
        padding: '10px 27px 10px 26px',
        borderRadius: 25
    },

    fieldInput: {
        marginRight: '40px'
    },
    fieldInputWhite: {
        backgroundColor: theme.palette.common.white,
        marginRight: '40px'
    }
}));

function AddAdress({ onChange }) {
    const [placeValue, setPlaceValue] = useState('');
    const classes = useStyles();

    // Create method
    const handleEditAdress = () => {
        onChange(placeValue);
        setPlaceValue('');
    };

    return (
        <div>
            <TextField
                label="Nouvelle adresse"
                variant="outlined"
                value={placeValue}
                className={classes.fieldInputWhite}
                onKeyPress={(event) => {
                    if (event.key === 'Enter') handleEditAdress();
                }}
                onChange={(event) => setPlaceValue(event.target.value)}
            />
            <Button
                variant="outlined"
                color="primary"
                className={classes.editButton}
                onClick={handleEditAdress}>
                Ajouter
            </Button>
        </div>
    );
}

AddAdress.propTypes = {
    onChange: PropTypes.func.isRequired
};

function EditAdress({ value, onChange }) {
    const [placeValue, setPlaceValue] = useState(value);
    const [editMode, setEditMode] = useState(false);

    const classes = useStyles();

    // Create method
    const handleEditAdress = () => {
        onChange(placeValue);
        setEditMode(false);
    };

    return (
        <div>
            {!editMode ? (
                <Grid container alignItems="center">
                    <Typography display="inline" className={classes.fieldInput}>
                        {value}
                    </Typography>
                    <Button
                        variant="outlined"
                        color="primary"
                        className={classes.editButton}
                        onClick={() => setEditMode(true)}>
                        Editer
                    </Button>
                </Grid>
            ) : (
                <Grid container alignItems="center">
                    <TextField
                        value={placeValue}
                        className={classes.fieldInput}
                        onKeyPress={(event) => {
                            if (event.key === 'Enter') handleEditAdress();
                        }}
                        onChange={(event) => setPlaceValue(event.target.value)}
                    />
                    <Button
                        variant="outlined"
                        className={classes.editButton}
                        style={{ marginRight: '4px' }}
                        onClick={() => setEditMode(false)}>
                        Annuler
                    </Button>
                    <Button
                        variant="outlined"
                        color="primary"
                        className={classes.editButton}
                        onClick={handleEditAdress}>
                        Valider
                    </Button>
                </Grid>
            )}
        </div>
    );
}

EditAdress.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

const EDIT_CAMPUS_ADRESS = gql`
    mutation editCampusAdress($id: String!, $campus: EditCampusInput!) {
        editCampus(id: $id, campus: $campus) {
            id
            securityNotificationEmail
            securityNotificationEnabled
        }
    }
`;

function NotificationSection({ campusData }) {
    const classes = useStyles();

    const { addAlert } = useSnackBar();

    const [editCampus] = useMutation(EDIT_CAMPUS_ADRESS);

    const handleChange = async ({ adress, notif }) => {
        const { id } = campusData;

        try {
            await editCampus({
                variables: {
                    id,
                    campus: {
                        securityNotificationEmail: adress ?? campusData.securityNotificationEmail,
                        securityNotificationEnabled: notif ?? campusData.securityNotificationEnabled
                    }
                },
                optimisticResponse: {
                    editCampus: {
                        id,
                        __typename: 'Campus',
                        securityNotificationEmail: adress ?? campusData.securityNotificationEmail,
                        securityNotificationEnabled: notif ?? campusData.securityNotificationEnabled
                    }
                }
            });
            addAlert({
                message: 'La modification a bien été effectuée',
                severity: 'success'
            });
        } catch (e) {
            return addAlert({
                message: 'Erreur serveur, merci de réessayer',
                severity: 'warning'
            });
        }
    };

    return (
        <Grid container alignItems="center" className={classes.root}>
            <Grid item sm={12} className={classes.row}>
                <Typography variant="body1" style={{ fontWeight: 'bold' }}>
                    Adresse fonctionnelle sureté
                </Typography>
            </Grid>
            <Grid item sm={12} className={classes.row}>
                {campusData.securityNotificationEmail ? (
                    <EditAdress
                        value={campusData.securityNotificationEmail}
                        onChange={(adress) => handleChange({ adress })}
                    />
                ) : (
                    <AddAdress onChange={(adress) => handleChange({ adress })} />
                )}
            </Grid>
            <Grid item sm={12}>
                <FormControlLabel
                    control={
                        <Switch
                            color="primary"
                            checked={campusData.securityNotificationEnabled}
                            onChange={(event) => handleChange({ notif: event.target.checked })}
                        />
                    }
                    label="Activer notification"
                    labelPlacement="start"
                />
            </Grid>
        </Grid>
    );
}

NotificationSection.propTypes = {
    campusData: PropTypes.shape({
        id: PropTypes.string.isRequired,
        securityNotificationEmail: PropTypes.string,
        securityNotificationEnabled: PropTypes.bool
    })
};

export default NotificationSection;
