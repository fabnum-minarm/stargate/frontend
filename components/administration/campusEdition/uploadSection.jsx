import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import InputUpload from '../../../components/styled/inputUpload';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: `20px 10px 23px 50px`,
        backgroundColor: theme.palette.background.layout,
        borderRadius: 4
    }
}));

function UploadSection({ campusData, onChange }) {
    const classes = useStyles();
    return (
        <Grid container alignItems="center" className={classes.root}>
            <Grid item sm={6}>
                <Typography variant="body1" style={{ fontWeight: 'bold' }}>
                    Réglementation & consigne de sécurité
                </Typography>
                <Typography variant="subtitle1" style={{ fontStyle: 'italic' }}>
                    fichier de taille inferieur à 1Mo
                    <InputUpload name="0" onChange={(files) => onChange({ index: '0', files })} />
                </Typography>
                {campusData.getCampus.generateSecurityFileExportLink && (
                    <a href={campusData.getCampus.generateSecurityFileExportLink.link} download>
                        <Typography variant="body1">Téléchargement du fichier</Typography>
                    </a>
                )}
            </Grid>
            <Grid item sm={6}>
                <Typography variant="body1" style={{ fontWeight: 'bold' }}>
                    Plan d&apos;accès
                </Typography>
                <Typography variant="subtitle1" style={{ fontStyle: 'italic' }}>
                    fichier de taille inferieur à 1Mo
                    <InputUpload name="1" onChange={(files) => onChange({ index: '1', files })} />
                </Typography>
                {campusData.getCampus.generateAccesPlanExportLink && (
                    <a href={campusData.getCampus.generateAccesPlanExportLink.link} download>
                        <Typography variant="body1">Téléchargement du fichier</Typography>
                    </a>
                )}
            </Grid>
        </Grid>
    );
}

UploadSection.propTypes = {
    campusData: PropTypes.object,
    onChange: PropTypes.func
};

export default UploadSection;
