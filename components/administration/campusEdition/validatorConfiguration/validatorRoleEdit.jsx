import React, { useMemo, useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useMutation } from '@apollo/client';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';

import StarIcon from '@material-ui/icons/Star';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import GridList from '@material-ui/core/ImageList';
import GridListTile from '@material-ui/core/ImageListItem';
import { VALIDATION_SCOPE_AREA, VALIDATION_SCOPE_CAMPUS } from '../../../../utils/constants/enums';

import { isSuperAdmin } from '../../../../utils/permissions';

import { CreateRoleField, LabelRoleField } from '../../../index';
import { UPDATE_CAMPUS_ROLE } from '../../../../lib/apollo/mutations';
import { useSnackBar } from '../../../../lib/hooks/snackbar';

import { activeRoleCacheVar } from '../../../../lib/apollo/cache';

import {
    GOUV_DOMAIN_MAIL,
    SCREENING_DOMAIN_MAIL
} from '../../../../utils/mappers/createUserFromMail';
import { Controller, useForm } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '20px 50px'
    },
    tagSection: {
        marginTop: 30,
        marginBottom: 30
    },
    tagTitle: {
        marginBottom: 20,
        fontWeight: 'bold'
    },
    mainIcon: {
        color: theme.palette.common.yellow
    },
    icon: {
        marginRight: 5
    },
    colorEdit: {
        color: theme.palette.primary.main
    },
    editButton: {
        width: 109,
        height: 50,
        marginTop: 5,
        padding: '10px 27px 10px 26px',
        borderRadius: 25
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    truncate: {
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap'
    },
    DeleteIcon: {
        textAlign: 'center'
    }
}));

function ValidatorRoleEdit({ listRoles, roleData, roleUsers }) {
    const classes = useStyles();
    const { addAlert } = useSnackBar();
    const { control, handleSubmit, errors, setValue } = useForm();

    const existLabel = useMemo(() => roleData.role.tags.map((tag) => tag.label.toUpperCase()), [
        roleData
    ]);
    const existValue = useMemo(
        () => roleData.role.tags.map((tag) => tag.value.toUpperCase())[roleData]
    );

    const [updateCampusRole] = useMutation(UPDATE_CAMPUS_ROLE);

    const isSuper = isSuperAdmin(activeRoleCacheVar().role);
    /**
     * return an array with the actual tags of the role
     */
    const getTags = () => {
        return roleData.role.tags.map((tag) => ({
            label: tag.label,
            primary: tag.primary,
            value: tag.value
        }));
    };

    const submitEditRoleCampus = async (formData) => {
        try {
            await updateCampusRole({
                variables: {
                    id: roleData.campus.id,
                    shortLabel: roleData.role.shortLabel,
                    role: {
                        label: roleData.role.label,
                        template: roleData.role.template,
                        tags: getTags(),
                        shortLabel: roleData.role.shortLabel
                    },
                    tag: {
                        label: formData.tagLabel,
                        value: formData.tagValue,
                        primary: false
                    }
                }
            });
            setValue('tagValue', '');
            setValue('tagLabel', '');
            return addAlert({
                message: `Le rôle à bien été mis à jour`,
                severity: 'success'
            });
        } catch {
            addAlert({
                message: 'Erreur lors de la modification des badges',
                severity: 'error'
            });
        }
    };

    const handleDeleteTag = async (label) => {
        let tags = getTags();
        const indexToDelete = tags.findIndex((tag) => tag.label === label);
        tags.splice(indexToDelete, 1);
        try {
            await updateCampusRole({
                variables: {
                    id: roleData.campus.id,
                    shortLabel: roleData.role.shortLabel,
                    role: {
                        label: roleData.role.label,
                        template: roleData.role.template,
                        tags: tags,
                        shortLabel: roleData.role.shortLabel
                    }
                }
            });
            return addAlert({
                message: `Le badge a bien été supprimé`,
                severity: 'success'
            });
        } catch {
            addAlert({
                message: 'Erreur lors de la suppression du badge',
                severity: 'error'
            });
        }
    };

    const handleEditTag = async (label, tag) => {
        let tags = getTags();
        const indexToUpdate = tags.findIndex((tag) => tag.label === label);
        tags[indexToUpdate] = tag;

        try {
            await updateCampusRole({
                variables: {
                    id: roleData.campus.id,
                    shortLabel: roleData.role.shortLabel,
                    role: {
                        label: roleData.role.label,
                        template: roleData.role.template,
                        tags: tags,
                        shortLabel: roleData.role.shortLabel
                    }
                }
            });
            return addAlert({
                message: `Le badge a bien été modifié`,
                severity: 'success'
            });
        } catch {
            addAlert({
                message: 'Erreur lors de la modification du badge',
                severity: 'error'
            });
        }
    };

    return (
        <Grid className={classes.root}>
            {[VALIDATION_SCOPE_CAMPUS, VALIDATION_SCOPE_AREA].includes(
                roleData.role.validator.scope
            ) && (
                <CreateRoleField
                    roleData={roleData}
                    usersList={roleUsers}
                    mailDomain={[GOUV_DOMAIN_MAIL, SCREENING_DOMAIN_MAIL]}
                    canDelete={roleUsers.length > 1}>
                    <Typography variant="body1" style={{ fontWeight: 'bold' }}>
                        Responsable(s)
                    </Typography>
                </CreateRoleField>
            )}
            <LabelRoleField roleData={roleData} listRoles={listRoles} isEdit />
            {roleData.role.tags?.length > 0 && (
                <form onSubmit={handleSubmit(submitEditRoleCampus)}>
                    <Grid item sm={9} md={9} className={classes.tagSection}>
                        <Grid item>
                            <Typography variant="body1" className={classes.tagTitle}>
                                Badge accès temporaire
                            </Typography>
                        </Grid>
                        <Grid container>
                            <Grid item sm={12} md={3}>
                                <Controller
                                    as={
                                        <TextField
                                            margin="dense"
                                            label="Label court"
                                            variant="outlined"
                                            fullWidth
                                            inputProps={{ maxLength: 7 }}
                                            error={Object.prototype.hasOwnProperty.call(
                                                errors,
                                                'tagValue'
                                            )}
                                            helperText={errors.tagValue && errors.tagValue.message}
                                        />
                                    }
                                    control={control}
                                    name="tagValue"
                                    defaultValue=""
                                    rules={{
                                        validate: {
                                            valide: (value) =>
                                                value.trim() !== '' ||
                                                'Veuillez saisir un label court',
                                            unique: (value) =>
                                                !existValue.includes(value.toUpperCase()) ||
                                                'Ce label court existe déjà'
                                        }
                                    }}
                                />
                            </Grid>
                            <Grid item sm={12} md={4} style={{ marginLeft: 80, marginRight: 10 }}>
                                <Controller
                                    as={
                                        <TextField
                                            margin="dense"
                                            label="Label"
                                            variant="outlined"
                                            fullWidth
                                            error={Object.prototype.hasOwnProperty.call(
                                                errors,
                                                'tagLabel'
                                            )}
                                            helperText={errors.tagLabel && errors.tagLabel.message}
                                        />
                                    }
                                    control={control}
                                    name="tagLabel"
                                    defaultValue=""
                                    rules={{
                                        validate: {
                                            valide: (value) =>
                                                value.trim() !== '' || 'Veuillez saisir un label',
                                            unique: (value) =>
                                                !existLabel.includes(value.toUpperCase()) ||
                                                'Ce label existe déjà'
                                        }
                                    }}
                                />
                            </Grid>
                            <Grid item sm={12} md={2} className={classes.buttonContainer}>
                                <Button
                                    type="submit"
                                    size="small"
                                    variant="outlined"
                                    color="primary"
                                    className={classes.editButton}>
                                    Ajouter
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid item sm={12} style={{ marginTop: 20 }}>
                            <GridList cols={1} cellHeight={30}>
                                {roleData.role.tags.map((choice, index) => (
                                    <GridListTile key={index}>
                                        <RowBadges
                                            choice={choice}
                                            isSuperAdmin={isSuper}
                                            onDelete={(label) => handleDeleteTag(label)}
                                            onUpdate={(label, tag) => handleEditTag(label, tag)}
                                        />
                                    </GridListTile>
                                ))}
                            </GridList>
                        </Grid>
                    </Grid>
                </form>
            )}
        </Grid>
    );
}

ValidatorRoleEdit.propTypes = {
    roleData: PropTypes.shape({
        role: PropTypes.object,
        campus: PropTypes.object.isRequired
    }).isRequired,
    listRoles: PropTypes.array.isRequired,
    roleUsers: PropTypes.array.isRequired
};

function RowBadges({ choice, isSuperAdmin, onDelete, onUpdate }) {
    const classes = useStyles();

    const [edit, setEdit] = useState(false);

    const shortRef = useRef();
    const longRef = useRef();

    useEffect(() => {
        if (edit && !shortRef.current.value) {
            shortRef.current.value = choice.value;
            longRef.current.value = choice.label;
        }
    }, [edit, choice]);

    return (
        <Grid container alignItems="center">
            <Grid item className={classes.icon}>
                {choice.primary ? (
                    <StarIcon className={classes.mainIcon} />
                ) : (
                    <StarBorderOutlinedIcon />
                )}
            </Grid>
            <Grid item sm={4}>
                {edit ? (
                    <TextField
                        margin="dense"
                        inputRef={shortRef}
                        style={{ marginTop: 0, marginBottom: 0 }}
                        inputProps={{ maxLength: 7 }}
                        InputProps={{ className: classes.colorEdit }}
                    />
                ) : (
                    choice.value
                )}
            </Grid>
            <Tooltip title={choice.label} key={choice.label} arrow>
                <Grid item sm={4} className={classes.truncate}>
                    {edit ? (
                        <TextField
                            margin="dense"
                            inputRef={longRef}
                            style={{ marginTop: 0, marginBottom: 0 }}
                            InputProps={{ className: classes.colorEdit }}
                        />
                    ) : (
                        choice.label
                    )}
                </Grid>
            </Tooltip>
            {choice.primary === false && (
                <Grid item sm={2} className={classes.DeleteIcon}>
                    <IconButton
                        size="small"
                        edge="end"
                        color="primary"
                        onClick={() => onDelete(choice.label)}
                        aria-label="supprimer">
                        <DeleteOutlinedIcon />
                    </IconButton>
                </Grid>
            )}
            {choice.primary && isSuperAdmin && (
                <Grid item sm={2} className={classes.DeleteIcon}>
                    {edit ? (
                        <>
                            <IconButton
                                style={{ marginRight: 10 }}
                                size="small"
                                edge="end"
                                aria-label="cancel"
                                onClick={() => setEdit(false)}>
                                <CloseIcon />
                            </IconButton>
                            <IconButton
                                size="small"
                                edge="end"
                                color="primary"
                                aria-label="valid"
                                onClick={() => {
                                    setEdit(false);
                                    onUpdate(choice.label, {
                                        primary: choice.primary,
                                        value: shortRef.current.value,
                                        label: longRef.current.value
                                    });
                                }}>
                                <CheckIcon />
                            </IconButton>
                        </>
                    ) : (
                        <IconButton
                            size="small"
                            edge="end"
                            color="primary"
                            aria-label="edit"
                            onClick={() => setEdit(true)}>
                            <EditOutlinedIcon />
                        </IconButton>
                    )}
                </Grid>
            )}
        </Grid>
    );
}

RowBadges.propTypes = {
    choice: PropTypes.shape({
        value: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        primary: PropTypes.bool.isRequired
    }).isRequired,
    isSuperAdmin: PropTypes.bool,
    onDelete: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired
};

export default ValidatorRoleEdit;
