import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import ItemCard from '../../../styled/itemCard';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import theme from '../../../../styles/theme';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '20px 50px',
        borderRadius: 4,
        backgroundColor: theme.palette.background.layout
    }
}));

const itemCardStyle = (currentRole, selectedRole) => {
    let style = {
        cursor: 'pointer',
        justifyContent: 'center'
    };
    if (currentRole.label === selectedRole.label) {
        style = {
            ...style,
            border: `1px solid ${theme.palette.primary.main}`,
            color: theme.palette.primary.main
        };
    }
    return style;
};

const HeaderConfigurationValidator = ({ validatorsRoles, selectedRole, handleSelectRole }) => {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            {validatorsRoles.map((role) => (
                <Grid item key={role.label} onClick={() => handleSelectRole(role.label)}>
                    <ItemCard style={itemCardStyle(role, selectedRole)}>
                        <Typography variant="body1">{role.shortLabel}</Typography>
                    </ItemCard>
                </Grid>
            ))}
        </Grid>
    );
};

HeaderConfigurationValidator.propTypes = {
    validatorsRoles: PropTypes.array.isRequired,
    selectedRole: PropTypes.object,
    handleSelectRole: PropTypes.func.isRequired
};

export default HeaderConfigurationValidator;
