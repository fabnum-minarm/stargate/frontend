import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { LabelRoleField } from '../../../index';

const useStyles = makeStyles(() => ({
    root: {
        padding: '20px 50px'
    },
    flexDiv: {
        display: 'flex'
    },
    boldTitle: {
        fontWeight: 'bold'
    },
    italicTitle: {
        fontStyle: 'italic',
        paddingLeft: 20
    }
}));

function ValidatorRoleAdd({ listRoles, roleData }) {
    const classes = useStyles();
    return (
        <Grid className={classes.root}>
            <LabelRoleField roleData={roleData} listRoles={listRoles}>
                <div className={classes.flexDiv}>
                    <Typography variant="body1" className={classes.boldTitle}>
                        Label validateur
                    </Typography>
                    <Typography variant="body1" className={classes.italicTitle}>
                        Requis
                    </Typography>
                </div>
            </LabelRoleField>
        </Grid>
    );
}

ValidatorRoleAdd.propTypes = {
    roleData: PropTypes.shape({
        role: PropTypes.string,
        campus: PropTypes.objectOf(PropTypes.string).isRequired
    }).isRequired,
    listRoles: PropTypes.array.isRequired,
    roleUsers: PropTypes.array.isRequired
};

export default ValidatorRoleAdd;
