import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';
import { format } from 'date-fns';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import ItemCard from '../../styled/itemCard';
import {
    ADMIN_CAMPUS_EDITION,
    ADMIN_CAMPUS_ROLE_EDITION,
    ADMIN_CAMPUS_ROLE_ADD
} from '../../../utils/constants/appUrls';
import WarningIcon from '@material-ui/icons/Warning';
import Button from '@material-ui/core/Button';
import RoundButton from '../../styled/common/roundButton';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
    globalContainer: {
        height: 100,
        marginBottom: 40
    },
    textInfos: {
        padding: theme.spacing(2)
    },
    textInfosSubtitle: {
        fontStyle: 'italic'
    },
    validators: {
        padding: `20px 10px 23px 50px`,
        backgroundColor: theme.palette.background.layout,
        borderRadius: 4
    },
    emptyValidatorsIcon: {
        marginLeft: 5,
        height: 22,
        color: theme.palette.warning.main
    },
    buttonsAria: {
        alignItems: 'center',
        justifyContent: 'space-evenly'
    }
}));

function CampusSection({ campusData, usersTotalByRole }) {
    const classes = useStyles();
    const router = useRouter();

    const validatorsRoles = campusData.roles.filter((r) => r.validator.scope);
    return (
        <Grid item sm={12}>
            <Grid container className={classes.globalContainer}>
                <Grid item sm={8} md={4}>
                    <Paper elevation={3} className={classes.textInfos}>
                        <Grid container justifyContent="space-between">
                            <Grid item>
                                <Typography variant="h5">
                                    {campusData.label} - {campusData.trigram}
                                </Typography>
                                <Typography
                                    variant="subtitle2"
                                    className={classes.textInfosSubtitle}>
                                    Créée le {format(new Date(campusData.createdAt), 'dd/MM/yyyy')}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <RoundButton
                                    onClick={() => router.push(ADMIN_CAMPUS_EDITION(campusData.id))}
                                    variant="outlined"
                                    color="primary">
                                    Modifier
                                </RoundButton>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
            <Grid container justifyContent="space-between" className={classes.validators}>
                <Grid container item sm={10} md={10} lg={10}>
                    <Typography variant="body1" style={{ fontWeight: 'bold' }}>
                        Configuration validateurs
                    </Typography>
                    <Grid container>
                        {validatorsRoles.map((role) => (
                            <Tooltip title={role.label} key={role.label} arrow>
                                <Grid item key={role.template}>
                                    <ItemCard
                                        style={{
                                            justifyContent: 'center'
                                        }}>
                                        <Grid container justifyContent="center">
                                            <Typography variant="body1">
                                                {role.shortLabel}
                                            </Typography>
                                            {usersTotalByRole[role.template] < 1 && (
                                                <WarningIcon
                                                    className={classes.emptyValidatorsIcon}
                                                />
                                            )}
                                        </Grid>
                                    </ItemCard>
                                </Grid>
                            </Tooltip>
                        ))}
                    </Grid>
                </Grid>
                <Grid container item sm={2} md={2} lg={2} className={classes.buttonsAria}>
                    <Button
                        onClick={() => router.push(ADMIN_CAMPUS_ROLE_ADD(campusData.id))}
                        variant="outlined"
                        color="primary">
                        Ajouter
                    </Button>
                    <RoundButton
                        onClick={() => router.push(ADMIN_CAMPUS_ROLE_EDITION(campusData.id))}
                        variant="outlined"
                        color="primary">
                        Modifier
                    </RoundButton>
                </Grid>
            </Grid>
        </Grid>
    );
}

CampusSection.propTypes = {
    campusData: PropTypes.object.isRequired,
    usersTotalByRole: PropTypes.objectOf(PropTypes.number).isRequired,
    listRoles: PropTypes.array.isRequired
};

export default CampusSection;
