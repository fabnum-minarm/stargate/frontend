import React, { useState } from 'react';

import PropTypes from 'prop-types';

import { Button, TextField } from '@material-ui/core';
import { gql, useMutation } from '@apollo/client';

import { GET_EMPLOYEE_LIST } from '../../../lib/apollo/fragments';
import { makeStyles } from '@material-ui/core/styles';

export const CREATE_EMPLOY = gql`
    mutation createEmploy($campusId: String!, $employeetype: EmployeeTypeInput!) {
        mutateCampus(id: $campusId) {
            createEmployeeType(employeetype: $employeetype) {
                id
                label
            }
        }
    }
`;

const useStyles = makeStyles(() => ({
    addButton: {
        width: 109,
        height: 50,
        marginTop: 5,
        padding: '10px 27px 10px 26px',
        borderRadius: 25
    }
}));

export default function AddEmployee({ campusId }) {
    const [employeeValue, setEmployeeValue] = useState('');
    const classes = useStyles();
    const [createEmployeeType] = useMutation(CREATE_EMPLOY);

    // Create method
    const handleCreateEmployeeType = () => {
        createEmployeeType({
            variables: {
                campusId,
                employeetype: { label: employeeValue }
            },
            update: (cache, { data: { mutateCampus: createEmployeeType } }) => {
                const campus = cache.readFragment({
                    id: `Campus:${campusId}`,
                    fragment: GET_EMPLOYEE_LIST,
                    fragmentName: 'getEmployeeList'
                });

                const updateList = {
                    ...campus,
                    listEmployeeType: {
                        ...campus.listEmployeeType,
                        list: [...campus.listEmployeeType.list, createEmployeeType],
                        meta: {
                            ...campus.listEmployeeType.meta,
                            total: campus.listEmployeeType.meta.total + 1
                        }
                    }
                };

                cache.writeFragment({
                    id: `Campus:${campusId}`,
                    fragment: GET_EMPLOYEE_LIST,
                    fragmentName: 'getEmployeeList',
                    data: updateList
                });
                setEmployeeValue('');
            }
        });
    };

    return (
        <div>
            <TextField
                label="Nouveau type"
                variant="outlined"
                value={employeeValue}
                onKeyPress={(event) => {
                    if (event.key === 'Enter') handleCreateEmployeeType();
                }}
                onChange={(event) => setEmployeeValue(event.target.value)}
            />
            <Button
                variant="outlined"
                color="primary"
                className={classes.addButton}
                onClick={handleCreateEmployeeType}>
                Ajouter
            </Button>
        </div>
    );
}

AddEmployee.propTypes = {
    campusId: PropTypes.string.isRequired
};
