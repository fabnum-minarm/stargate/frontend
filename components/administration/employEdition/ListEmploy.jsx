import React, { useState } from 'react';

import { gql, useMutation, useQuery } from '@apollo/client';

import PropTypes from 'prop-types';

import { GET_EMPLOYEE_LIST } from '../../../lib/apollo/queries';
import { GET_EMPLOYEE_LIST as FRAGMENT_GET_EMPLOYEE_LIST } from '../../../lib/apollo/fragments';
import {
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    TextField,
    IconButton
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CancelIcon from '@material-ui/icons/Cancel';
import CheckIcon from '@material-ui/icons/Check';

export const EDIT_EMPLOYEE = gql`
    mutation editEmployee($campusId: String!, $id: ObjectID!, $employeetype: EmployeeTypeInput!) {
        mutateCampus(id: $campusId) {
            editEmployeeType(id: $id, employeetype: $employeetype) {
                id
                label
            }
        }
    }
`;

export const DELETE_EMPLOYEE = gql`
    mutation deleteEmployee($campusId: String!, $id: ObjectID!) {
        mutateCampus(id: $campusId) {
            deleteEmployeeType(id: $id) {
                id
            }
        }
    }
`;

export default function ListEmploy({ campusId }) {
    const { data, loading } = useQuery(GET_EMPLOYEE_LIST, {
        variables: { campusId },
        skip: !campusId
    });

    const [editId, setEditId] = useState(null);
    const [delId, setDelId] = useState(null);

    const [editValue, setEditValue] = useState('');

    const [editEmployee] = useMutation(EDIT_EMPLOYEE);
    const [deleteEmployee] = useMutation(DELETE_EMPLOYEE);

    const handleEditEmployee = () => {
        editEmployee({
            variables: {
                campusId,
                id: editId,
                employeetype: { label: editValue }
            }
        });
    };

    const handleDeleteEmployee = () => {
        deleteEmployee({
            variables: {
                campusId,
                id: delId
            },
            update: (cache, { data: { mutateCampus: deleteEmployee } }) => {
                const campus = cache.readFragment({
                    id: `Campus:${campusId}`,
                    fragment: FRAGMENT_GET_EMPLOYEE_LIST,
                    fragmentName: 'getEmployeeList'
                });

                const newList = campus.listEmployeeType.list.filter(
                    (p) => p.id !== deleteEmployee.id
                );

                const updateList = {
                    ...campus,
                    listEmployeeType: {
                        ...campus.listEmployeeType,
                        list: newList,
                        meta: campus.listEmployeeType.meta - 1
                    }
                };

                cache.writeFragment({
                    id: `Campus:${campusId}`,
                    fragment: FRAGMENT_GET_EMPLOYEE_LIST,
                    fragmentName: 'getEmployeeList',
                    data: updateList
                });
            }
        });
    };

    if (loading || !data || data?.getCampus?.listEmployeeType?.list?.length === 0) return '';

    return (
        <List>
            {data.getCampus.listEmployeeType.list.map((type) => (
                <ListItem key={type.id}>
                    {(() => {
                        if (delId === type.id)
                            return (
                                <>
                                    <ListItemText primary={`Supprimer ${type.label} ?`} />
                                    <ListItemSecondaryAction>
                                        <IconButton
                                            edge="end"
                                            aria-label="edit"
                                            onClick={() => {
                                                setDelId(null);
                                            }}>
                                            <CancelIcon />
                                        </IconButton>
                                        <IconButton
                                            edge="end"
                                            aria-label="delete"
                                            onClick={handleDeleteEmployee}>
                                            <CheckIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </>
                            );

                        if (editId === type.id)
                            return (
                                <>
                                    <TextField
                                        value={editValue}
                                        onChange={(event) => setEditValue(event.target.value)}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton
                                            edge="end"
                                            aria-label="edit"
                                            onClick={() => {
                                                setEditId(null);
                                                setEditValue('');
                                            }}>
                                            <CancelIcon />
                                        </IconButton>
                                        <IconButton
                                            edge="end"
                                            aria-label="delete"
                                            onClick={() => {
                                                handleEditEmployee();
                                                setEditId(null);
                                            }}>
                                            <CheckIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </>
                            );

                        return (
                            <>
                                <ListItemText primary={type.label} />
                                <ListItemSecondaryAction>
                                    <IconButton
                                        edge="end"
                                        aria-label="edit"
                                        onClick={() => {
                                            setEditId(type.id);
                                            setEditValue(type.label);
                                        }}>
                                        <EditIcon />
                                    </IconButton>
                                    <IconButton
                                        edge="end"
                                        aria-label="delete"
                                        onClick={() => {
                                            setDelId(type.id);
                                        }}>
                                        <DeleteIcon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </>
                        );
                    })()}
                </ListItem>
            ))}
        </List>
    );
}

ListEmploy.propTypes = {
    campusId: PropTypes.string.isRequired
};
