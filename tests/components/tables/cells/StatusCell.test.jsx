import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import StatusCell from '../../../../components/tables/cells/StatusCell';

import { activeRoleCacheVar } from '../../../../lib/apollo/cache';
import { ROLES, WORKFLOW_BEHAVIOR } from '../../../../utils/constants/enums';
import { render, screen } from '../../../../utils/tests/renderApollo';

const mockVisitor = {
    units: [
        {
            label: 'centerPark',
            steps: [
                {
                    role: {
                        template: 'ROLE_BASIC_UNIT_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: [],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_AREA_ADVISEMENT',
                        validator: { behavior: 'ADVISEMENT' }
                    },
                    state: {
                        tags: [],
                        isOK: true,
                        value: 'POSITIVE'
                    }
                },
                {
                    role: {
                        template: 'ROLE_UNIT_VALIDATION_TAGS',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: ['VA'],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_FINAL_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: ['VA'],
                        value: 'ACCEPTED'
                    }
                }
            ]
        },
        {
            label: 'JurassicPark',
            steps: [
                {
                    role: {
                        template: 'ROLE_BASIC_UNIT_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: [],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_AREA_ADVISEMENT',
                        validator: { behavior: 'ADVISEMENT' }
                    },
                    state: {
                        isOK: true,
                        tags: [],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_UNIT_VALIDATION_TAGS',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: ['VA'],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_FINAL_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: ['VA'],
                        value: 'ACCEPTED'
                    }
                }
            ]
        },
        {
            label: 'Ogasawara National Park',
            steps: [
                {
                    role: {
                        template: 'ROLE_BASIC_UNIT_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: [],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_AREA_ADVISEMENT',
                        validator: { behavior: 'ADVISEMENT' }
                    },
                    state: {
                        isOK: true,
                        tags: [],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_UNIT_VALIDATION_TAGS',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: false,
                        tags: [],
                        value: 'REJECTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_FINAL_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: ['VA'],
                        value: 'ACCEPTED'
                    }
                }
            ]
        }
    ]
};

describe('Component: StatusCell', () => {
    it('display correctly for Access office', () => {
        activeRoleCacheVar({
            role: {
                template: ROLES.ROLE_FINAL_VALIDATION.role,
                validator: { behavior: WORKFLOW_BEHAVIOR.VALIDATION.value }
            },
            unit: '60111c31878c3e1190920895'
        });
        render(
            <Table>
                <TableBody>
                    <TableRow>
                        <StatusCell visitor={mockVisitor} />
                    </TableRow>
                </TableBody>
            </Table>
        );
        //check the display of accepted icone
        expect(screen.getByTitle(/icone validée/i)).toBeInTheDocument();
        expect(screen.getByText(/VA/i)).toBeInTheDocument();
    });

    it('display correctly for Security officer', () => {
        activeRoleCacheVar({
            role: {
                template: ROLES.ROLE_UNIT_VALIDATION_TAGS.role
            },
            unit: '60111c31878c3e1190920895',
            unitLabel: 'Ogasawara National Park'
        });
        render(
            <Table>
                <TableBody>
                    <TableRow>
                        <StatusCell visitor={mockVisitor} />
                    </TableRow>
                </TableBody>
            </Table>
        );
        expect(screen.getByTitle(/icone refusée/i)).toBeInTheDocument();
    });
    it('display correctly for UNIT CORRESPONDENT', () => {
        activeRoleCacheVar({
            role: {
                template: ROLES.ROLE_BASIC_UNIT_VALIDATION.role
            },
            unit: '60111c31878c3e1190920895',
            unitLabel: 'Ogasawara National Park'
        });
        render(
            <Table>
                <TableBody>
                    <TableRow>
                        <StatusCell visitor={mockVisitor} />
                    </TableRow>
                </TableBody>
            </Table>
        );
        expect(screen.getByTitle(/icone validée/i)).toBeInTheDocument();
        expect(screen.getByText(/Accepté/i)).toBeInTheDocument();
    });
});
