import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import DecisionsCell from '../../../../components/tables/cells/DecisionsCell';

import { activeRoleCacheVar } from '../../../../lib/apollo/cache';
import { ROLES } from '../../../../utils/constants/enums';
import { render, screen } from '../../../../utils/tests/renderApollo';

const modalOpen = jest.fn();

const mockVisitor = {
    units: [
        {
            label: 'centerPark',
            steps: [
                {
                    role: {
                        template: 'ROLE_BASIC_UNIT_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_AREA_ADVISEMENT',
                        validator: { behavior: 'ADVISEMENT' }
                    },
                    state: {
                        isOK: false,
                        value: 'NEGATIVE'
                    }
                },
                {
                    role: {
                        template: 'ROLE_FINAL_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: null,
                        value: null
                    }
                }
            ]
        },
        {
            label: 'JurassicPark',
            steps: [
                {
                    role: {
                        template: 'ROLE_BASIC_UNIT_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_AREA_ADVISEMENT',
                        validator: { behavior: 'ADVISEMENT' }
                    },
                    state: {
                        isOK: false,
                        value: 'NEGATIVE'
                    }
                },
                {
                    role: {
                        template: 'ROLE_UNIT_VALIDATION_TAGS',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        tags: ['VL'],
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_FINAL_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: null,
                        value: null
                    }
                }
            ]
        },
        {
            label: 'Ogasawara National Park',
            steps: [
                {
                    role: {
                        template: 'ROLE_BASIC_UNIT_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_AREA_ADVISEMENT',
                        validator: { behavior: 'ADVISEMENT' }
                    },
                    state: {
                        isOK: false,
                        value: 'NEGATIVE'
                    }
                },
                {
                    role: {
                        template: 'ROLE_UNIT_VALIDATION_TAGS',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: null,
                        tags: [],
                        value: null
                    }
                },
                {
                    role: {
                        template: 'ROLE_FINAL_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: null,
                        value: null
                    }
                }
            ]
        },
        {
            label: 'Grand Canyon',
            steps: [
                {
                    role: {
                        template: 'ROLE_BASIC_UNIT_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: true,
                        value: 'ACCEPTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_AREA_ADVISEMENT',
                        validator: { behavior: 'ADVISEMENT' }
                    },
                    state: {
                        isOK: false,
                        value: 'NEGATIVE'
                    }
                },
                {
                    role: {
                        template: 'ROLE_UNIT_VALIDATION_TAGS',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: false,
                        tags: [],
                        value: 'REJECTED'
                    }
                },
                {
                    role: {
                        template: 'ROLE_FINAL_VALIDATION',
                        validator: { behavior: 'VALIDATION' }
                    },
                    state: {
                        isOK: null,
                        value: null
                    }
                }
            ]
        }
    ]
};

describe('Component: DecisionsCell', () => {
    it('display correctly for Access office', () => {
        activeRoleCacheVar({
            role: {
                template: ROLES.ROLE_FINAL_VALIDATION.role
            },
            unit: '60111c31878c3e1190920895',
            unitLabel: 'CIRI'
        });
        render(
            <Table>
                <TableBody>
                    <TableRow>
                        <DecisionsCell visitor={mockVisitor} modalOpen={modalOpen} />
                    </TableRow>
                </TableBody>
            </Table>
        );
        //check the display of 4 icons (1 accepted / 1 rejected / 1 waiting / 1 RES)
        expect(screen.getByTitle(/icone RES/i)).toBeInTheDocument();
        expect(screen.getByTitle(/icone validée/i)).toBeInTheDocument();
        expect(screen.getByTitle(/icone en attente/i)).toBeInTheDocument();
        expect(screen.getByTitle(/icone refusée/i)).toBeInTheDocument();
    });

    it('display correctly for Security officer', () => {
        activeRoleCacheVar({
            role: ROLES.ROLE_UNIT_VALIDATION_TAGS.role,
            unit: '60111c31878c3e1190920895',
            unitLabel: 'CIRI'
        });
        render(
            <Table>
                <TableBody>
                    <TableRow>
                        <DecisionsCell visitor={mockVisitor} modalOpen={modalOpen} />
                    </TableRow>
                </TableBody>
            </Table>
        );
        expect(screen.getByTitle(/icone RES/i)).toBeInTheDocument();
    });
});
