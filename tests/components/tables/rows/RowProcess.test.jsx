import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';

import RowProcess from '../../../../components/tables/rows/RowProcess';
import { render, screen } from '../../../../utils/tests/renderApollo';
import { ROLES, WORKFLOW_BEHAVIOR } from '../../../../utils/constants/enums';

const mockColumns = [
    {
        id: 'unit',
        label: 'Unité'
    },
    {
        id: 'steps',
        label: 'Workflow de validation'
    }
];

const mockRow = {
    unit: 'Expandables',
    steps: [
        {
            role: {
                id: ROLES.ROLE_BASIC_UNIT_VALIDATION.role,
                shortLabel: ROLES.ROLE_BASIC_UNIT_VALIDATION.role,
                validator: { behavior: WORKFLOW_BEHAVIOR.VALIDATION.value }
            },
            state: { value: 'ACCEPTED', isOK: true }
        },
        {
            role: {
                id: ROLES.ROLE_AREA_ADVISEMENT.role,
                shortLabel: ROLES.ROLE_AREA_ADVISEMENT.role,
                validator: { behavior: WORKFLOW_BEHAVIOR.ADVISEMENT.value }
            },
            state: { value: null, isOK: null }
        },
        {
            role: {
                id: ROLES.ROLE_UNIT_VALIDATION_TAGS.role,
                shortLabel: ROLES.ROLE_UNIT_VALIDATION_TAGS.role,
                validator: { behavior: WORKFLOW_BEHAVIOR.VALIDATION.value }
            },
            state: { value: null, isOK: null }
        },
        {
            role: {
                id: ROLES.ROLE_FINAL_VALIDATION.role,
                shortLabel: ROLES.ROLE_FINAL_VALIDATION.role,
                validator: { behavior: WORKFLOW_BEHAVIOR.VALIDATION.value }
            },
            state: { value: null, isOK: null }
        }
    ]
};

describe('Component: rowProcess', () => {
    /** @todo test if .toHaveClass completed or error or anything else */
    it('feed & display correctly', () => {
        render(
            <Table>
                <TableBody>
                    <RowProcess columns={mockColumns} row={mockRow} />
                </TableBody>
            </Table>
        );
        expect(screen.queryByText(/expandables/i)).toBeInTheDocument();
        expect(screen.queryByText(ROLES.ROLE_BASIC_UNIT_VALIDATION.role)).toBeInTheDocument();
        expect(screen.queryByText(ROLES.ROLE_FINAL_VALIDATION.role)).toBeInTheDocument();
    });
});
