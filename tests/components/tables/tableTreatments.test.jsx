import { InMemoryCache } from '@apollo/client/core';
import userEvent from '@testing-library/user-event';

import TableTreatments from '../../../components/tables/TableTreatments';
import { activeRoleCacheVar, typePolicies } from '../../../lib/apollo/cache';
import { GET_ACTIVE_ROLE } from '../../../lib/apollo/queries';
import { ACCEPTED_STATUS } from '../../../utils';
import { ROLES, WORKFLOW_BEHAVIOR } from '../../../utils/constants/enums';
import { render, screen } from '../../../utils/tests/renderApollo';

const mockUpdate = jest.fn();

jest.mock('../../../lib/hooks/useDecisions', () => ({
    useDecisions: () => ({
        decisions: {
            '000_0001_000_00V1': {
                id: '000_00V1',
                request: { id: '000_0001' },
                choice: {
                    label: '',
                    validation: '',
                    tags: []
                }
            }
        },
        addDecision: mockUpdate
    })
}));
let mockVisitor;

const mocks = [
    {
        request: {
            query: GET_ACTIVE_ROLE
        },
        result: {
            data: {
                activeRoleCache: {}
            }
        }
    }
];

describe('Component: tableTreatmentsToTreat', () => {
    let cache;

    beforeEach(() => {
        cache = new InMemoryCache({
            typePolicies
        });
    });

    it('display correctly for Unit Correspondent', () => {
        activeRoleCacheVar({
            role: {
                template: ROLES.ROLE_BASIC_UNIT_VALIDATION.role,
                tags: [],
                validator: { behavior: WORKFLOW_BEHAVIOR.VALIDATION.value }
            },
            unit: ''
        });
        mockVisitor = [
            {
                id: 'visitor_1',
                firstname: 'Michel',
                birthLastname: 'Jospin',
                isInternal: true,
                isScreened: ACCEPTED_STATUS,
                company: 'Pastiche',
                employeeType: 'Stagiaire',
                request: {
                    id: '000_0001',
                    from: '2021-02-11T23:00:00.000Z',
                    to: '2021-02-15T23:00:00.000Z',
                    reason: 'Famille',
                    owner: {
                        rank: 'SGT',
                        lastname: 'Jean',
                        firstname: 'Marc',
                        unit: {
                            label: 'La compta'
                        }
                    },
                    places: [{ label: 'MESSE' }, { label: 'Bar PMU de la base' }]
                }
            }
        ];

        render(<TableTreatments visitors={mockVisitor} treated={false} />, { mocks, cache });
        expect(screen.getByText(/Michel Jospin/)).toBeInTheDocument();
        expect(screen.getByText(/000_0001/)).toBeInTheDocument();
        expect(screen.getByText(/ACCEPTER/)).toBeInTheDocument();
        expect(screen.getByText(/Famille/)).toBeInTheDocument();

        //check if column's name is correct for Unit correspondant
        expect(
            screen.getByRole('columnheader', {
                name: /motif/i
            })
        );
    });

    it('update correctly', () => {
        activeRoleCacheVar({
            role: {
                template: ROLES.ROLE_UNIT_VALIDATION_TAGS.role,
                tags: [{ value: 'VA', primary: false }],
                validator: { behavior: WORKFLOW_BEHAVIOR.VALIDATION.value }
            },
            unit: ''
        });
        mockVisitor = [
            {
                id: 'visitor_1',
                firstname: 'Michel',
                birthLastname: 'Jospin',
                isInternal: true,
                isCribled: ACCEPTED_STATUS,
                company: 'Pastiche',
                employeeType: 'Stgiaire',
                units: [
                    {
                        label: 'centerParks',
                        steps: [
                            {
                                role: {
                                    template: 'ROLE_BASIC_UNIT_VALIDATION',
                                    validator: { behavior: 'VALIDATION' }
                                },
                                state: {
                                    isOK: true,
                                    value: 'ACCEPTED'
                                }
                            },
                            {
                                role: {
                                    template: 'ROLE_AREA_ADVISEMENT',
                                    validator: { behavior: 'ADVISEMENT' }
                                },
                                state: {
                                    isOK: true,
                                    value: 'POSITIVE'
                                }
                            },
                            {
                                role: {
                                    template: 'ROLE_UNIT_VALIDATION_TAGS',
                                    validator: { behavior: 'VALIDATION' },
                                    tags: [{ value: 'VA', primary: false }]
                                },
                                state: {
                                    isOK: null,
                                    value: 'null'
                                }
                            },
                            {
                                role: {
                                    template: 'ROLE_FINAL_VALIDATION',
                                    validator: { behavior: 'VALIDATION' }
                                },
                                state: {
                                    isOK: null,
                                    value: null
                                }
                            }
                        ]
                    }
                ],
                request: {
                    id: '000_0001',
                    from: '2021-02-11T23:00:00.000Z',
                    to: '2021-02-15T23:00:00.000Z',
                    reason: 'Famille',
                    owner: {
                        rank: 'SGT',
                        lastname: 'Jean',
                        firstname: 'Marc',
                        unit: {
                            label: 'La compta'
                        }
                    },
                    places: [{ label: 'MESSE' }, { label: 'Bar PMU de la base' }]
                }
            }
        ];

        render(<TableTreatments visitors={mockVisitor} treated={false} />, { mocks, cache });

        //check if column's name is correct for SECURITY OFFICER
        expect(
            screen.getByRole('columnheader', {
                name: /Criblage/i
            })
        );

        //check if Actions choices are correct for SECURITY OFFICER
        userEvent.click(screen.getByLabelText('VA'));

        expect(mockUpdate).toHaveBeenCalled();
    });
});
