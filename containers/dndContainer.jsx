import React, { useCallback, useState } from 'react';
import update from 'immutability-helper';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { Select } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';
import DndCard from '../components/styled/dndCard';
import RoundButton from '../components/styled/common/roundButton';
import { makeStyles } from '@material-ui/core/styles';
import { WORKFLOW_CONDITIONS_CHOICES } from '../utils/constants/enums';

const useStyles = makeStyles(() => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%'
    },
    addButton: {
        height: '100%',
        display: 'flex',
        alignItems: 'flex-end'
    },
    selectList: {
        display: 'none'
    }
}));

const DndContainer = ({ cards, setCards, allCards }) => {
    const classes = useStyles();
    const [openMenu, toggleMenu] = useState(false);
    const moveCard = useCallback(
        (dragIndex, hoverIndex) => {
            const dragCard = cards[dragIndex];
            setCards(
                update(cards, {
                    $splice: [
                        [dragIndex, 1],
                        [hoverIndex, 0, dragCard]
                    ]
                })
            );
        },
        [cards]
    );

    const addCard = (value) => {
        const newCards = cards.map((card) => card);
        newCards.push({
            ...value,
            condition: WORKFLOW_CONDITIONS_CHOICES.NONE.select,
            id: newCards.length + 1
        });
        return setCards(newCards);
    };

    const deleteCard = (text) => {
        const newCards = cards.filter((card) => card.text !== text);
        return setCards(newCards.map((c, index) => ({ ...c, id: index + 1 })));
    };

    const renderCard = (card, index) => {
        const handleChangeCondition = (condition) => {
            return setCards(
                cards.map((c) => {
                    if (c.id === card.id) {
                        return {
                            ...c,
                            condition
                        };
                    }
                    return c;
                })
            );
        };
        return (
            <DndCard
                key={card.id}
                index={index}
                id={card.id}
                text={card.text}
                condition={card.condition}
                moveCard={moveCard}
                deleteCard={deleteCard}
                handleChangeCondition={handleChangeCondition}
            />
        );
    };
    return (
        <DndProvider backend={HTML5Backend}>
            <div className={classes.container}>
                {cards.map((card, i) => renderCard(card, i))}
                {cards.length < allCards.length && (
                    <FormControl>
                        <div id="selectWorkflow" className={classes.addButton}>
                            <RoundButton
                                onClick={() => toggleMenu(!openMenu)}
                                variant="outlined"
                                color="primay">
                                Ajouter
                            </RoundButton>
                        </div>
                        <Select
                            variant="outlined"
                            labelId="create-unit-workflow"
                            id="workFlow"
                            disabled={null}
                            open={openMenu}
                            onClose={() => toggleMenu(false)}
                            onChange={(evt) => addCard(evt.target.value)}
                            className={classes.selectList}
                            MenuProps={{
                                anchorEl: document.getElementById('selectWorkflow'),
                                style: { marginTop: 60 }
                            }}>
                            {allCards
                                .filter((card) => !cards.find((c) => c.text === card.text))
                                .map((card) => (
                                    <MenuItem key={card.id} value={card}>
                                        {card.text}
                                    </MenuItem>
                                ))}
                        </Select>
                    </FormControl>
                )}
            </div>
        </DndProvider>
    );
};

DndContainer.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.shape)).isRequired,
    setCards: PropTypes.func.isRequired,
    allCards: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.shape)).isRequired
};

export default DndContainer;
