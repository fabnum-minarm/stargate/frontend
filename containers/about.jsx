import React from 'react';
import packageInfo from '../package.json';
import List from '@material-ui/core/List';
import Chip from '@material-ui/core/Chip';
import { ListItem } from '@material-ui/core';

export default function about() {
    const clientVersion = `v${packageInfo.version}`;
    const serverVersion = `v${packageInfo.version}`;

    const clientVersionHandleClick = () => {
        let a = document.createElement('a');
        a.target = '_blank';
        a.rel = 'noopener noreferrer nofollow';
        a.href = `https://gitlab.com/fabnum-minarm/stargate/frontend/-/releases/${clientVersion}`;
        a.click();
    };
    const serverVersionHandleClick = () => {
        let a = document.createElement('a');
        a.target = '_blank';
        a.rel = 'noopener noreferrer nofollow';
        a.href = `https://gitlab.com/fabnum-minarm/stargate/backend/-/releases/${serverVersion}`;
        a.click();
    };

    return (
        <div>
            <List component="nav">
                <ListItem>
                    Client version&nbsp;&nbsp;
                    <Chip
                        label={clientVersion}
                        variant="outlined"
                        onClick={clientVersionHandleClick}
                    />
                </ListItem>
                <ListItem>
                    Server version&nbsp;&nbsp;
                    <Chip
                        label={serverVersion}
                        variant="outlined"
                        onClick={serverVersionHandleClick}
                    />
                </ListItem>
            </List>
        </div>
    );
}
