import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { UnitRoleForm } from '../../../../components';
import LoadingCircle from '../../../../components/styled/animations/loadingCircle';
import { useApolloClient } from '@apollo/client';
import { LIST_USERS } from '../../../../lib/apollo/queries';

const UnitRoleFormContainer = ({ unit, campus, rolesList }) => {
    const client = useApolloClient();
    const [listUsersByRole, setListUsersByRole] = useState(null);

    const refreshList = async (roleData) => {
        const { data } = await client.query({
            query: LIST_USERS,
            variables: {
                campus: campus.id,
                cursor: { offset: 0, first: 10 },
                hasRole: { role: roleData.role.template, unit: unit.id }
            }
        });
        return setListUsersByRole({
            ...listUsersByRole,
            [roleData.role.template]: { role: roleData.role, list: data.listUsers.list }
        });
    };
    const getListUsersByRole = async () => {
        let listUsers = {};
        await Promise.all(
            rolesList.map(async (role) => {
                const { data } = await client.query({
                    query: LIST_USERS,
                    variables: {
                        campus: campus.id,
                        cursor: { offset: 0, first: 10 },
                        hasRole: { role: role.template, unit: unit.id }
                    }
                });
                listUsers = { ...listUsers, [role.template]: { role, list: data.listUsers.list } };
                return data;
            })
        );
        return setListUsersByRole(listUsers);
    };

    useEffect(() => {
        if (!listUsersByRole) {
            getListUsersByRole();
        }
    }, [listUsersByRole]);

    if (!listUsersByRole) return <LoadingCircle />;

    return (
        <UnitRoleForm
            unit={unit}
            campus={campus}
            listUsersByRole={listUsersByRole}
            refreshList={refreshList}
        />
    );
};

UnitRoleFormContainer.propTypes = {
    unit: PropTypes.object.isRequired,
    campus: PropTypes.object.isRequired,
    rolesList: PropTypes.array.isRequired
};

export default UnitRoleFormContainer;
