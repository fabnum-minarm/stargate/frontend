import React from 'react';
import { useQuery } from '@apollo/client';
import PageTitle from '../../../components/styled/common/pageTitle';
import HeaderPageBackBtn from '../../../components/styled/headerPageBackBtn';
import { ADMIN_AREA_ADMINISTRATION } from '../../../utils/constants/appUrls';
import { GET_CAMPUSES_LIST } from '../../../lib/apollo/queries';
import PropTypes from 'prop-types';
import AreaForm from '../../../components/administrationForms/areaForm';
import { mapAreaCampusList } from '../../../utils/mappers/adminMappers';

function AreaFormContainer({ onSubmit, defaultValues }) {
    const { data, loading } = useQuery(GET_CAMPUSES_LIST);

    if (loading || !data) return '';

    return (
        <>
            <HeaderPageBackBtn to={ADMIN_AREA_ADMINISTRATION}>
                Retour administration zone influence
            </HeaderPageBackBtn>
            <div>
                <PageTitle subtitles={["Zone d'influence", 'Bases']}>Administration</PageTitle>
                <section>
                    <AreaForm
                        onSubmit={onSubmit}
                        defaultValues={defaultValues}
                        listCampuses={mapAreaCampusList(data.listCampuses.list)}
                    />
                </section>
            </div>
        </>
    );
}

AreaFormContainer.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    defaultValues: PropTypes.shape({
        label: PropTypes.string.isRequired,
        areaCampusesList: PropTypes.array.isRequired
    }).isRequired
};

export default AreaFormContainer;
