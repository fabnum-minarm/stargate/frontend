import React, { useState } from 'react';
import { gql, useQuery } from '@apollo/client';
import IndexAdministration from '../../components/administration';
import { mapUsersList } from '../../utils/mappers/adminMappers';
import { canHandleUser, isAdmin, isSuperAdmin } from '../../utils/permissions';
import { useLogin } from '../../lib/loginContext';
import { campusIdVar } from '../../lib/apollo/cache';
import { ADMIN_USER_EDITION } from '../../utils/constants/appUrls';

const DELETE_USER = gql`
    mutation deleteUser($id: ObjectID!) {
        deleteUser(id: $id) {
            id
        }
    }
`;

const columns = [
    { id: 'lastname', label: 'Nom' },
    { id: 'firstname', label: 'Prénom' },
    { id: 'campus', label: 'Base' },
    { id: 'unit', label: 'Unité' },
    { id: 'role', label: 'Rôle' }
];

export const GET_USERS_LIST = gql`
    query listUsers(
        $cursor: OffsetCursor
        $filters: UserFilters
        $hasRole: HasRoleInput
        $campus: String
        $search: String
    ) {
        campusId @client @export(as: "campus")
        listUsers(
            cursor: $cursor
            filters: $filters
            hasRole: $hasRole
            campus: $campus
            search: $search
        ) {
            meta {
                offset
                first
                total
            }
            list {
                id
                lastname
                firstname
                roles {
                    role {
                        template
                        label
                        shortLabel
                        permissions
                        validator {
                            scope
                            behavior
                        }
                        tags {
                            label
                            value
                            primary
                        }
                    }
                    campuses {
                        id
                        label
                    }
                    units {
                        id
                        label
                    }
                }
                email {
                    original
                }
            }
        }
    }
`;

const createUserData = (data) => ({
    createPath: '/administration/utilisateurs/creation',
    editPath: ADMIN_USER_EDITION(data),
    confirmDeleteText: `Êtes-vous sûr de vouloir supprimer cet utilisateur: ${data} ?`,
    deletedText: `L'utilisateur ${data} a bien été supprimé`
});

const usersListFilters = (activeRole) => {
    switch (true) {
        case isSuperAdmin(activeRole.role):
            return {};
        case isAdmin(activeRole.role):
            return { campus: campusIdVar() };
        case canHandleUser(activeRole.role):
            return { hasRole: { unit: activeRole.unit } };
        default:
            return null;
    }
};

function UserAdministration() {
    const { activeRole } = useLogin();
    const [usersList, setUsersList] = useState({ list: [], total: 0 });
    const [searchInput, setSearchInput] = useState('');

    const onCompletedQuery = (data) =>
        setUsersList({
            list: mapUsersList(data.listUsers.list),
            total: data.listUsers.meta.total
        });

    const { refetch, fetchMore } = useQuery(GET_USERS_LIST, {
        variables: {
            cursor: { first: 10, offset: 0 },
            search: searchInput,
            ...usersListFilters(activeRole)
        },
        onCompleted: (data) => {
            setUsersList({
                list: mapUsersList(data.listUsers.list),
                total: data.listUsers.meta.total
            });
        }
    });

    return (
        <IndexAdministration
            fetchMore={fetchMore}
            refetch={refetch}
            result={usersList}
            deleteMutation={DELETE_USER}
            onCompletedQuery={onCompletedQuery}
            searchInput={searchInput}
            setSearchInput={setSearchInput}
            tabData={createUserData}
            columns={columns}
            subtitles={['Utilisateurs']}
        />
    );
}

export default UserAdministration;
