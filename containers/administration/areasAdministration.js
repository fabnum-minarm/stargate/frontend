import React, { useState, useEffect } from 'react';
import { gql, useQuery } from '@apollo/client';
import IndexAdministration from '../../components/administration';

const columns = [
    { id: 'label', label: 'Nom' },
    { id: 'campuses', label: 'Bases' }
];

const GET_AREAS_LIST = gql`
    query listAreas($cursor: OffsetCursor, $filters: AreaFilters) {
        listAreas(filters: $filters, cursor: $cursor) {
            meta {
                offset
                first
                total
            }
            list {
                id
                label
                campuses {
                    id
                    label
                }
            }
        }
    }
`;

const DELETE_AREA = gql`
    mutation deleteArea($id: ObjectID!) {
        deleteArea(id: $id) {
            id
        }
    }
`;

const createAreaData = (data) => ({
    createPath: '/administration/zoneinfluence/creation',
    editPath: `/administration/zoneinfluence/${data}`,
    confirmDeleteText: `Êtes-vous sûr de vouloir supprimer cette zone d'influence: ${data} ?`,
    deletedText: `La zone d'influence ${data} a bien été supprimée`
});

function AreaAdministration() {
    const [areasList, setAreasList] = useState({ list: [], total: 0 });
    const [searchInput, setSearchInput] = useState('');

    const mapAreasList = (areasList) =>
        areasList.map((area) => ({
            id: area.id,
            label: area.label,
            campuses: area.campuses.map((campus) => campus.label).join(', ')
        }));

    const onCompletedQuery = (data) => {
        return setAreasList({
            list: mapAreasList(data.listAreas.list),
            total: data.listAreas.meta.total
        });
    };

    const { data, refetch, fetchMore } = useQuery(GET_AREAS_LIST, {
        fetchPolicy: 'no-cache',
        variables: {
            cursor: { first: 10, offset: 0 },
            search: searchInput
        }
    });

    const updateDeleteMutation = () => {
        refetch();
    };

    useEffect(() => {
        if (data) {
            onCompletedQuery(data);
        }
    }, [data]);

    return (
        <IndexAdministration
            result={areasList}
            fetchMore={fetchMore}
            refetch={refetch}
            onCompletedQuery={onCompletedQuery}
            searchInput={searchInput}
            setSearchInput={setSearchInput}
            deleteMutation={DELETE_AREA}
            updateFunction={updateDeleteMutation}
            tabData={createAreaData}
            columns={columns}
            subtitles={["Zones d'influence"]}
        />
    );
}

export default AreaAdministration;
