import React from 'react';
import PropTypes from 'prop-types';
import { NotificationSection } from '../../../components';
import { useQuery } from '@apollo/client';
import { GET_CAMPUS } from '../../../lib/apollo/queries';

function NotificationContainer({ campusId }) {
    const { data: campusData, loading: loadCampus } = useQuery(GET_CAMPUS, {
        variables: { id: campusId }
    });

    return !loadCampus && <NotificationSection campusData={campusData.getCampus} />;
}

NotificationContainer.propTypes = {
    campusId: PropTypes.string.isRequired
};

export default NotificationContainer;
