import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PageTitle from '../../../../components/styled/common/pageTitle';
import { GET_CAMPUS } from '../../../../lib/apollo/queries';
import { useQuery } from '@apollo/client';
import { ValidatorRoleAdd } from '../../../../components';
import HeaderPageBackBtn from '../../../../components/styled/headerPageBackBtn';
import { ADMIN_CAMPUS_MANAGEMENT } from '../../../../utils/constants/appUrls';

const subtitles = ['Ajouter Validateur'];

const AddRoleConfiguration = ({ campusId, listRoles }) => {
    const { data } = useQuery(GET_CAMPUS, { variables: { id: campusId } });
    const role = listRoles.find((r) => r.template === 'ROLE_BASIC_UNIT_VALIDATION');
    return (
        <Grid>
            <HeaderPageBackBtn to={ADMIN_CAMPUS_MANAGEMENT(campusId)}>
                Retour administration de base
            </HeaderPageBackBtn>
            <PageTitle subtitles={subtitles}>Administration bases</PageTitle>
            <Paper>
                <ValidatorRoleAdd roleData={{ data, role }} listRoles={listRoles} />
            </Paper>
        </Grid>
    );
};

AddRoleConfiguration.propTypes = {
    campusId: PropTypes.string.isRequired,
    listRoles: PropTypes.array.isRequired
};

export default AddRoleConfiguration;
