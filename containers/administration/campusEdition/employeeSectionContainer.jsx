import React from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@apollo/client';
import { EmployeeSection } from '../../../components';
import { GET_EMPLOYEE_LIST } from '../../../lib/apollo/queries';

function EmployeeSectionContainer({ campusId }) {
    const { data, loading } = useQuery(GET_EMPLOYEE_LIST, { variables: { campusId } });
    return (
        !loading &&
        data && (
            <EmployeeSection
                listEmployeeType={data.getCampus.listEmployeeType}
                campusId={campusId}
            />
        )
    );
}

EmployeeSectionContainer.propTypes = {
    campusId: PropTypes.string.isRequired
};

export default EmployeeSectionContainer;
