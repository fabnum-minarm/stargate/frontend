import React from 'react';
import PropTypes from 'prop-types';
import { UploadSection } from '../../../components';
import { gql, useQuery, useMutation } from '@apollo/client';
import { GET_CAMPUS } from '../../../lib/apollo/queries';

const EDIT_CAMPUS = gql`
    mutation editCampus($campus: EditCampusInput!, $id: String!) {
        editCampus(campus: $campus, id: $id) {
            id
            label
        }
    }
`;

function UploadSectionContainer({ campusId }) {
    const { data: campusData, loading: loadCampus } = useQuery(GET_CAMPUS, {
        variables: { id: campusId }
    });

    const [editCampus] = useMutation(EDIT_CAMPUS, {
        refetchQueries: [
            {
                query: GET_CAMPUS,
                variables: {
                    id: campusId
                }
            } // DocumentNode object parsed with gql
        ]
    });

    const handleChange = ({ index, files }) => {
        const campus = { ...campusData.getCampus };

        editCampus({
            variables: {
                id: campusId,
                campus: {
                    fileIndexChanged: index,
                    file: [files],
                    label: campus.label,
                    trigram: campus.trigram
                }
            }
        });
    };

    return (
        !loadCampus && (
            <UploadSection campusData={campusData} onChange={(file) => handleChange(file)} />
        )
    );
}

UploadSectionContainer.propTypes = {
    campusId: PropTypes.string.isRequired
};

export default UploadSectionContainer;
