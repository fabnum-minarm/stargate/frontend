import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useApolloClient } from '@apollo/client';
import { CampusSection } from '../../../components';
import { LIST_USERS } from '../../../lib/apollo/queries';
import { VALIDATION_SCOPE_CAMPUS } from '../../../utils/constants/enums';

function CampusSectionContainer({ campusId, campusData }) {
    const client = useApolloClient();
    const [usersTotalByRole, setUsersTotalByRole] = useState(null);

    useEffect(() => {
        async function loadUsersTotalByRole() {
            let usersTotal = {};
            await Promise.all(
                campusData.getCampus.roles
                    .filter((r) => r.validator.scope === VALIDATION_SCOPE_CAMPUS)
                    .map(async (role) => {
                        const users = await client.query({
                            query: LIST_USERS,
                            variables: { hasRole: { role: role.template }, campus: campusId }
                        });
                        usersTotal = {
                            ...usersTotal,
                            [role.template]: users.data.listUsers.meta.total
                        };
                    })
            );
            setUsersTotalByRole(usersTotal);
        }

        if (!campusData) return;
        loadUsersTotalByRole();
    }, [campusData]);

    return (
        usersTotalByRole && (
            <CampusSection
                campusData={campusData.getCampus}
                usersTotalByRole={usersTotalByRole}
                listRoles={campusData.getCampus.roles}
            />
        )
    );
}

CampusSectionContainer.propTypes = {
    campusId: PropTypes.string.isRequired,
    campusData: PropTypes.shape({
        getCampus: PropTypes.shape({
            roles: PropTypes.arrayOf(PropTypes.object)
        })
    }).isRequired
};

export default CampusSectionContainer;
