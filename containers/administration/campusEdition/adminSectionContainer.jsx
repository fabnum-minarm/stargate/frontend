import React from 'react';
import PropTypes from 'prop-types';
import { AdminSection } from '../../../components';
import { useQuery } from '@apollo/client';
import { LIST_USERS, GET_CAMPUS } from '../../../lib/apollo/queries';
import { HANDLE_CAMPUS } from '../../../utils/permissions';

const roleData = (campusData, adminRole) => ({
    role: adminRole,
    campus: { id: campusData.id, label: campusData.label }
});

function AdminSectionContainer({ campusId, listRoles }) {
    const adminRole = listRoles.find((r) => r.permissions.includes(HANDLE_CAMPUS));
    const { data, loading } = useQuery(LIST_USERS, {
        variables: {
            campus: campusId,
            cursor: { offset: 0, first: 10 },
            hasRole: { role: adminRole.template }
        }
    });
    const { data: campusData, loading: loadCampus } = useQuery(GET_CAMPUS, {
        variables: { id: campusId }
    });

    return (
        !loading &&
        !loadCampus && (
            <AdminSection
                listAdmins={data?.listUsers?.list || []}
                roleData={roleData(campusData.getCampus, adminRole)}
            />
        )
    );
}

AdminSectionContainer.propTypes = {
    campusId: PropTypes.string.isRequired,
    listRoles: PropTypes.array.isRequired
};

export default AdminSectionContainer;
