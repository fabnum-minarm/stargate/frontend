import React from 'react';
import PropTypes from 'prop-types';
import { ValidatorRoleEdit } from '../../../../components';
import { useQuery } from '@apollo/client';
import { LIST_USERS } from '../../../../lib/apollo/queries';

function ValidatorRoleEditContainer({ campus, role, listRoles }) {
    const { data, loading } = useQuery(LIST_USERS, {
        variables: { campus: campus.id, hasRole: { role: role.template } }
    });

    return (
        !loading &&
        data && (
            <ValidatorRoleEdit
                listRoles={listRoles}
                roleUsers={data?.listUsers?.list ?? []}
                roleData={{ campus, role }}
            />
        )
    );
}

ValidatorRoleEditContainer.propTypes = {
    campus: PropTypes.object.isRequired,
    role: PropTypes.object.isRequired,
    listRoles: PropTypes.array.isRequired
};

export default ValidatorRoleEditContainer;
