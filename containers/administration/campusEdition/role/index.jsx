import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PageTitle from '../../../../components/styled/common/pageTitle';
import { HeaderConfigurationValidator } from '../../../../components/index';
import { VALIDATION_SCOPE_CAMPUS } from '../../../../utils/constants/enums';
import ValidatorRoleEditContainer from './ValidatorRoleEditContainer';
import { GET_CAMPUS } from '../../../../lib/apollo/queries';
import { useQuery } from '@apollo/client';
import HeaderPageBackBtn from '../../../../components/styled/headerPageBackBtn';
import { ADMIN_CAMPUS_MANAGEMENT } from '../../../../utils/constants/appUrls';

const subtitles = ['Configuration validateurs'];

const ValidatorConfiguration = ({ campusId, listRoles }) => {
    const { data } = useQuery(GET_CAMPUS, {
        variables: { id: campusId }
    });
    const [selectedRole, setSelectedRole] = useState(
        listRoles.find((r) => r.validator.scope === VALIDATION_SCOPE_CAMPUS) || null
    );
    const handleSelectRole = (label) => {
        const role = listRoles.find((r) => r.label === label);
        return setSelectedRole(role);
    };

    useEffect(() => {
        const role = listRoles.find((r) => r.template === selectedRole.template) || null;
        return setSelectedRole(role);
    }, [listRoles]);

    return (
        <Grid>
            <HeaderPageBackBtn to={ADMIN_CAMPUS_MANAGEMENT(campusId)}>
                Retour administration de base
            </HeaderPageBackBtn>
            <PageTitle subtitles={subtitles}>Administration bases</PageTitle>
            <Paper>
                <HeaderConfigurationValidator
                    validatorsRoles={listRoles}
                    selectedRole={selectedRole}
                    handleSelectRole={handleSelectRole}
                />
                {data && (
                    <ValidatorRoleEditContainer
                        role={selectedRole}
                        campus={data.getCampus}
                        listRoles={listRoles}
                    />
                )}
            </Paper>
        </Grid>
    );
};

ValidatorConfiguration.propTypes = {
    campusId: PropTypes.string.isRequired,
    listRoles: PropTypes.array.isRequired
};

export default ValidatorConfiguration;
