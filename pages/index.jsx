import React from 'react';
import dynamic from 'next/dynamic';

import { ROLES } from '../utils/constants/enums';
import { useLogin } from '../lib/loginContext';

const MyTreatments = dynamic(() => import('../containers/myTreatments'));
const MyAccessRequest = dynamic(() => import('../containers/myAccessRequests'));
const ScreeningManagement = dynamic(() => import('../containers/screeningManagement'));
const UserAdministration = dynamic(() => import('../containers/administration/userAdministration'));
const GatekeeperManagement = dynamic(() => import('../containers/gatekeeperManagement'));

// @todo : dynamic import

function selectLandingComponent(roleId) {
    switch (roleId) {
        case ROLES.ROLE_AREA_ADVISEMENT.role:
            return <ScreeningManagement />;
        case ROLES.ROLE_BASIC_USER.role:
            return <GatekeeperManagement />;
        case ROLES.ROLE_HOST.role:
            return <MyAccessRequest />;
        case ROLES.ROLE_ADMIN.role:
        case ROLES.ROLE_SUPER_ADMIN.role:
            return <UserAdministration />;
        case ROLES.ROLE_FINAL_VALIDATION.role:
        case ROLES.ROLE_FINAL_VALIDATION_CFAS.role:
        case ROLES.ROLE_FINAL_VALIDATION_OSB.role:
        case ROLES.ROLE_UNIT_VALIDATION_TAGS.role:
        case ROLES.ROLE_BASIC_UNIT_VALIDATION.role:
            return <MyTreatments />;
        default:
            return 'NO ACCESS';
    }
}

export default function Home() {
    const { activeRole } = useLogin();

    return selectLandingComponent(activeRole.role.template);
}
