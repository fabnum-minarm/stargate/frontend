/* eslint-disable react/jsx-wrap-multilines */
import React from 'react';

import PropTypes from 'prop-types';

import { useRouter } from 'next/router';

import { gql, useQuery } from '@apollo/client';

import Typography from '@material-ui/core/Typography';

import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import { format } from 'date-fns';
import { ID_DOCUMENT } from '../../utils/constants/enums';

const useStyles = makeStyles({
    root: {
        padding: '26px',
        '& h6': {
            textTransform: 'uppercase',
            fontWeight: 'bold',
            maxWidth: '350px',
            fontSize: '0.775rem'
        },
        '& p': {
            fontSize: '0.675rem'
        },
        '& .dashed': {
            width: '100%',
            marginTop: '10px',
            borderBottom: '1px dashed black'
        },
        '& th, td': {
            border: '1px solid',
            width: '33%',
            padding: '12px',
            '& .flex': {
                height: '80px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'space-around'
            },
            '& .flexName': {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-around',
                width: '100%',
                '& h6': {
                    minWidth: '40px'
                },
                '& > div': {
                    display: 'grid',
                    gridTemplateColumns: '80px 80px'
                }
            }
        },
        '& table': {
            marginTop: '10px',
            border: '2px solid',
            borderCollapse: 'collapse',
            '& tr': {
                height: '80px'
            },
            '& tbody::before': {
                content: 'none'
            },
            '& tbody::after': {
                content: 'none'
            }
        }
    },
    textField: {
        position: 'relative',
        maxWidth: '240px',
        '& span': {
            backgroundColor: 'white',
            paddingRight: '10px',
            display: 'table-cell',
            minWidth: '90px'
        },
        '&:after': {
            content: (props) => (props.text ? `'${props.text}'` : "''"),
            display: 'table-cell',
            width: '100%',
            height: '25px',
            border: '1px solid black',
            zIndex: '-1'
        }
    }
});

export const GET_VISITORS = gql`
    query getVisitors($campusId: String!, $visitorsId: [String]) {
        campusId @client @export(as: "campusId")
        getCampus(id: $campusId) {
            id
            label
            listVisitors(visitorsId: $visitorsId) {
                list {
                    id
                    firstname
                    nationality
                    birthday
                    birthplace
                    birthcountry
                    birthLastname
                    usageLastname
                    company
                    identityDocuments {
                        kind
                        reference
                        file {
                            id
                        }
                    }
                    generateIdentityFileExportLink {
                        link
                    }
                    units {
                        id
                        label
                        steps {
                            role {
                                template
                                label
                                shortLabel
                            }
                            state {
                                isOK
                                value
                                tags
                            }
                        }
                    }
                    request {
                        id
                        reason
                        from
                        to
                        places {
                            label
                        }
                        owner {
                            id
                            lastname
                            firstname
                        }
                    }
                }
                meta {
                    total
                }
            }
        }
    }
`;

function TextArea(props) {
    const classes = useStyles(props);

    return (
        <Typography className={classes.textField} display="inline" variant="body2">
            <span
                style={{
                    minWidth: props.width
                }}>
                {props.children}
            </span>
        </Typography>
    );
}

TextArea.propTypes = {
    width: PropTypes.string,
    children: PropTypes.string
};

export default function RequestToPrint() {
    const router = useRouter();

    const { data, loading } = useQuery(GET_VISITORS, {
        variables: {
            visitorsId: [router.query.id]
        },
        onCompleted: () => {
            parent.postMessage({ action: `print-${router.query.id}` });
        }
    });

    const visitor = data?.getCampus?.listVisitors?.list[0];
    const classes = useStyles();

    if (loading || !visitor) return '';

    return (
        <div className={classes.root}>
            <section>
                <Grid container direction="column" alignItems="center" spacing={1}>
                    <Grid item container>
                        <Grid item>
                            <Typography variant="subtitle2">
                                {`Fiche d'accompagnement d'un badge temporaire ou visteur de la
                    ${data?.getCampus?.label}`}
                            </Typography>
                        </Grid>
                        <Grid item style={{ width: '290px' }}>
                            <Grid container spacing={1} direction="column">
                                <Grid item>
                                    <TextArea width="140px">N° LP Temporaire:</TextArea>
                                </Grid>
                                <Grid item>
                                    <TextArea width="140px">N° LP Visiteur:</TextArea>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item container spacing={1}>
                        <Grid
                            item
                            container
                            spacing={1}
                            direction="column"
                            style={{ width: '33%' }}>
                            <Grid item>
                                <TextArea text={visitor.birthLastname}>Nom:</TextArea>
                            </Grid>
                            <Grid item>
                                <TextArea text={visitor.firstname}>Prénom:</TextArea>
                            </Grid>
                            <Grid item>
                                <TextArea text={visitor.company}>Société:</TextArea>
                            </Grid>
                        </Grid>
                        <Grid
                            item
                            container
                            spacing={1}
                            direction="column"
                            style={{ width: '33%' }}>
                            <Grid item>
                                <TextArea
                                    text={visitor.request.places.reduce((prev, place, index) => {
                                        if (index === 0) return place.label;
                                        return `${prev}, ${place.label}`;
                                    }, '')}>
                                    Lieux visités:
                                </TextArea>
                            </Grid>
                            <Grid item>
                                <TextArea
                                    text={ID_DOCUMENT[visitor.identityDocuments[0].kind].label}>
                                    Type pièce identité:
                                </TextArea>
                            </Grid>
                        </Grid>
                        <Grid
                            item
                            container
                            spacing={1}
                            direction="column"
                            style={{ width: '33%' }}>
                            <Grid item>
                                <TextArea
                                    text={format(new Date(), 'dd/MM/yyyy HH:mm')}
                                    width="120px">
                                    Date délivrance:
                                </TextArea>
                            </Grid>
                            <Grid item>
                                <TextArea
                                    text={format(new Date(visitor.request.to), 'dd/MM/yyyy')}
                                    width="120px">
                                    Date péremption:
                                </TextArea>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item container>
                        <div className="dashed" />
                    </Grid>
                    <Grid item container>
                        <table style={{ width: '100%' }}>
                            <tbody>
                                <tr>
                                    <td>
                                        <Typography variant="h5" style={{ fontWeight: 'bold' }}>
                                            N° LP T
                                        </Typography>
                                    </td>
                                    <td rowSpan="2">
                                        <div className="flex">
                                            <Typography variant="subtitle2">
                                                Date de delivrance:
                                            </Typography>
                                            <Typography variant="body2">
                                                {format(new Date(), 'dd/MM/yyyy HH:mm')}
                                            </Typography>
                                        </div>
                                    </td>
                                    <td rowSpan="2">
                                        <div className="flex">
                                            <Typography variant="subtitle2">
                                                Date de péremption:
                                            </Typography>
                                            <Typography variant="body2">
                                                {format(new Date(visitor.request.to), 'dd/MM/yyyy')}
                                            </Typography>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Typography variant="h5" style={{ fontWeight: 'bold' }}>
                                            N° LP V
                                        </Typography>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowSpan="2">
                                        <div className="flexName">
                                            <div>
                                                <Typography variant="subtitle2" display="inline">
                                                    Nom:
                                                </Typography>
                                                <Typography variant="body2" display="inline">
                                                    {visitor.birthLastname}
                                                </Typography>
                                            </div>
                                            <div>
                                                <Typography variant="subtitle2" display="inline">
                                                    Prenom:
                                                </Typography>
                                                <Typography variant="body2" display="inline">
                                                    {visitor.firstname}
                                                </Typography>
                                            </div>
                                            <div>
                                                <Typography variant="subtitle2" display="inline">
                                                    Né(e) le:
                                                </Typography>

                                                <Typography variant="body2" display="inline">
                                                    {format(
                                                        new Date(visitor.birthday),
                                                        'dd/MM/yyyy'
                                                    )}
                                                </Typography>
                                            </div>
                                            <div>
                                                <Typography variant="subtitle2" display="inline">
                                                    Société:
                                                </Typography>
                                                <Typography variant="body2" display="inline">
                                                    {visitor.company}
                                                </Typography>
                                            </div>
                                        </div>
                                    </td>
                                    <td rowSpan="2">
                                        <div className="flex">
                                            <Typography variant="subtitle2">
                                                Lieux visités:
                                            </Typography>
                                            <Typography variant="body2">
                                                {visitor.request.places.reduce(
                                                    (prev, place, index) => {
                                                        if (index === 0) return place.label;
                                                        return `${prev}, ${place.label}`;
                                                    },
                                                    ''
                                                )}
                                            </Typography>
                                            <Typography
                                                variant="subtitle2"
                                                style={{ marginTop: '15px' }}>
                                                Type pièce d&apos;identité
                                            </Typography>
                                            <Typography variant="body2">
                                                {
                                                    ID_DOCUMENT[visitor.identityDocuments[0].kind]
                                                        .label
                                                }
                                            </Typography>
                                        </div>
                                    </td>
                                    <td rowSpan="2">
                                        <div className="flex">
                                            <Typography variant="subtitle2">Vehicule</Typography>
                                            <div className="flexName" style={{ marginTop: '15px' }}>
                                                <div>
                                                    <Typography variant="body1">
                                                        Immatriculation:
                                                    </Typography>
                                                    <div />
                                                </div>
                                                <div style={{ marginTop: '15px' }}>
                                                    <Typography variant="body1">
                                                        Val. Assurance:
                                                    </Typography>
                                                    <div />
                                                </div>
                                                <div style={{ marginTop: '15px' }}>
                                                    <Typography variant="body1">Marque:</Typography>
                                                    <div />
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr></tr>
                                <tr>
                                    <td rowSpan="2">
                                        <div className="flex">
                                            <Typography variant="subtitle2" textAlign="center">
                                                Visa obligatoire de la personne visitée
                                            </Typography>
                                            <div style={{ margin: '50px' }} />
                                        </div>
                                    </td>
                                    <td rowSpan="2">
                                        <div className="flex">
                                            <Typography variant="subtitle2" textAlign="center">
                                                Visa (CSB)
                                            </Typography>
                                            <div style={{ margin: '50px' }} />
                                        </div>
                                    </td>
                                    <td rowSpan="2">
                                        <div className="flex">
                                            <Typography variant="subtitle2" textAlign="center">
                                                Identité de l&apos;accompagnateur
                                            </Typography>
                                            <Typography variant="body2" textAlign="center">
                                                (sauf personnel avec LP T)
                                            </Typography>
                                            <div style={{ margin: '30px' }} />
                                            <Typography
                                                variant="body2"
                                                textAlign="center"
                                                style={{ fontSize: '0.6rem' }}>
                                                Le visiteur doit être accompoagné en permanance par
                                                la personne mentionnée
                                            </Typography>
                                        </div>
                                    </td>
                                </tr>
                                <tr></tr>
                            </tbody>
                        </table>
                    </Grid>
                    <Grid item container direction="column" alignItems="center">
                        <Typography variant="body2">
                            Toute reproduction du document est formellement interdite.
                        </Typography>
                        <Typography variant="body2">
                            A restituer impérativement avant péremption à la C.S.B.
                        </Typography>
                        <Typography variant="body2" style={{ fontWeight: 'bold' }}>
                            Nota: Votre pièce d&apos;identité vous sera restituée en échange de
                            cette fiche de visite.
                        </Typography>
                    </Grid>
                    <Grid item container direction="column">
                        <div
                            style={{
                                border: '1px solid black',
                                backgroundColor: '#D3D3D3',
                                width: '100%'
                            }}>
                            <Typography
                                variant="h6"
                                style={{
                                    margin: 'auto',
                                    textAlign: 'center',
                                    fontSize: '1rem'
                                }}>
                                Acces des visteurs
                            </Typography>
                        </div>
                        <Typography variant="body2" style={{ marginTop: '15px' }}>
                            Le laissez-passer visteur ou temporaire qui vous a été delivré VOUS
                            PERMET de rejoindre une unité de la {data.getCampus.label}.
                        </Typography>
                        <Typography variant="body2" paragraph>
                            En aucun cas vous ne devez vous rendre en dehors des installations
                            autorisées et mentionnées ci-dessus. Les véhicules doivent stationner
                            sur le parkings prévus à cet effet. Pour les zones FAS leur accès est
                            interdit, sauf dérogation délivrée par le commandant de la zone visitée.
                            TOUT MANQUEMENT à ces consignes entraînera un raccompagnement à la
                            sortie et une INTERDICTION D&apos;ACCES à la {data.getCampus.label}.
                        </Typography>
                        <Typography variant="body2">
                            Je soussigné................................reconnais avoir pris
                            connaissance des régles d&apos;accès et de circulation sur{' '}
                            {data.getCampus.label} et m&apos;engage à les respecter.
                        </Typography>
                        <div>
                            <Typography
                                variant="body2"
                                display="inline"
                                style={{ marginRight: '140px' }}>
                                DATE:
                            </Typography>
                            <Typography variant="body2" display="inline">
                                SIGNATURE:
                            </Typography>
                        </div>
                    </Grid>
                </Grid>
            </section>
        </div>
    );
}
