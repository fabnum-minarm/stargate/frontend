import React, { useEffect, useState } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import PageTitle from '../../../components/styled/common/pageTitle';
import UserForm from '../../../components/administrationForms/userForm';
import { useSnackBar } from '../../../lib/hooks/snackbar';
import { useLogin } from '../../../lib/loginContext';
import { ADMIN_USER_ADMINISTRATION } from '../../../utils/constants/appUrls';
import { ADD_USER_ROLE, DELETE_ROLE } from '../../../lib/apollo/mutations';
import { ROLES } from '../../../utils/constants/enums';

const GET_USER = gql`
    query getUser($id: ObjectID!) {
        getUser(id: $id) {
            id
            firstname
            lastname
            email {
                original
            }
            roles {
                role {
                    template
                    label
                    shortLabel
                    permissions
                    validator {
                        scope
                        behavior
                    }
                }
                campuses {
                    id
                    label
                }
                units {
                    id
                    label
                }
            }
        }
    }
`;

const EDIT_USER = gql`
    mutation editUser($user: UserInput!, $id: ObjectID!) {
        editUser(user: $user, id: $id) {
            id
            firstname
            lastname
            email {
                original
            }
            roles {
                role {
                    template
                    label
                    shortLabel
                    permissions
                    validator {
                        scope
                        behavior
                    }
                    tags {
                        label
                        value
                        primary
                    }
                }
                campuses {
                    id
                    label
                }
                units {
                    id
                    label
                }
            }
        }
    }
`;

function EditUser() {
    const { addAlert } = useSnackBar();
    const router = useRouter();
    const { id } = router.query;
    const { data: editUserData, loading } = useQuery(GET_USER, { variables: { id } });
    const [editUser] = useMutation(EDIT_USER);
    const [addUserRole] = useMutation(ADD_USER_ROLE);
    const [deleteUserRole] = useMutation(DELETE_ROLE);
    const { activeRole } = useLogin();

    const [defaultValues, setDefaultValues] = useState(null);

    const submitEditUser = async (mappedUserData) => {
        try {
            const {
                data: {
                    editUser: { roles: roles }
                }
            } = await editUser({ variables: { user: mappedUserData.userData, id } });

            /* Checking if the role is already in the roles array. If it is not, it deletes the role
            and adds the new one. */

            if (
                !roles.find(
                    (role) =>
                        role.role.template === mappedUserData.roleData.role &&
                        role.units[0].id === mappedUserData.roleData.unit.id
                )
            ) {
                await deleteUserRole({
                    variables: {
                        id,
                        roleData: {
                            role: roles[0].role.template,
                            unit: roles[0].units
                        }
                    }
                });
                await addUserRole({
                    variables: {
                        roleData: {
                            role: mappedUserData.roleData.role,
                            campuses: [mappedUserData.roleData.campus],
                            unit: mappedUserData.roleData.unit
                        },
                        id,
                        campusId: mappedUserData.roleData.campus.id
                    }
                });
            }

            addAlert({ message: "L'utilisateur a bien été modifié", severity: 'success' });
            return router.push(ADMIN_USER_ADMINISTRATION);
        } catch (e) {
            switch (true) {
                case e.message.includes('User already exists'):
                    return addAlert({
                        message: 'Un utilisateur est déjà enregistré avec cet e-mail',
                        severity: 'error'
                    });
                case e.message.includes('User validation failed: email.original: '):
                    return addAlert({
                        message: "Erreur, veuillez vérifier le domaine de l'adresse e-mail",
                        severity: 'warning'
                    });
                default:
                    return addAlert({
                        message: 'Erreur serveur, merci de réessayer',
                        severity: 'warning'
                    });
            }
        }
    };

    const isUnitValidation = activeRole.role.template === ROLES.ROLE_BASIC_UNIT_VALIDATION.role;

    const mapEditUser = (data) => ({
        ...data,
        email: data.email.original,
        campus: data.roles[0] && data.roles[0].campuses[0] ? data.roles[0].campuses[0] : null,
        unit: data.roles[0] && data.roles[0].units[0] ? data.roles[0].units[0] : null,
        role: data.roles[0] ? data.roles[0].role.template : null
    });

    useEffect(() => {
        if (editUserData && editUserData.getUser) {
            setDefaultValues(mapEditUser(editUserData.getUser));
        }
    }, [editUserData]);

    if (loading) return '';
    return (
        <>
            <PageTitle
                subtitles={
                    isUnitValidation
                        ? [`Unité ${activeRole?.unitLabel}`, 'Utilisateur', 'Modifier demandeur']
                        : ['Utilisateur', 'Modifier utilisateur']
                }>
                Administration
            </PageTitle>
            {defaultValues && (
                <UserForm
                    submitForm={submitEditUser}
                    defaultValues={defaultValues}
                    userRole={activeRole}
                    type="edit"
                />
            )}
        </>
    );
}

export default EditUser;
