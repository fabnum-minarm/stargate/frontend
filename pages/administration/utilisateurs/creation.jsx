import React from 'react';
import { useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import PageTitle from '../../../components/styled/common/pageTitle';
import UserForm from '../../../components/administrationForms/userForm';
import { useSnackBar } from '../../../lib/hooks/snackbar';
import { useLogin } from '../../../lib/loginContext';
import { isAdmin, isSuperAdmin } from '../../../utils/permissions';
import { campusIdVar } from '../../../lib/apollo/cache';
import { ROLES } from '../../../utils/constants/enums';
import { ADD_USER_ROLE, CREATE_USER } from '../../../lib/apollo/mutations';
import { ADMIN_USER_ADMINISTRATION } from '../../../utils/constants/appUrls';
import { GET_USERS_LIST } from '../../../containers/administration/userAdministration';

function CreateUser() {
    const { addAlert } = useSnackBar();
    const router = useRouter();
    const { activeRole } = useLogin();

    const campusId = campusIdVar();
    const [addUserRole] = useMutation(ADD_USER_ROLE, {
        update: async (cache, { data: { addUserRole: updatedUser } }) => {
            const currentUsers = await cache.readQuery({
                query: GET_USERS_LIST,
                variables: {
                    campus: campusId,
                    search: '',
                    hasRole:
                        isAdmin(activeRole.role) || isSuperAdmin(activeRole.role)
                            ? {}
                            : { unit: activeRole.unit }
                }
            });
            if (!currentUsers) return;
            const updatedTotal = currentUsers.listUsers.meta.total + 1;
            const updatedUsers = {
                ...currentUsers,
                listUsers: {
                    ...currentUsers.listUsers,
                    ...(updatedTotal < 10 && {
                        list: [...currentUsers.listUsers.list, updatedUser]
                    }),
                    meta: {
                        ...currentUsers.listUsers.meta,
                        total: updatedTotal
                    }
                }
            };
            await cache.writeQuery({
                query: GET_USERS_LIST,
                variables: {
                    campus: campusId,
                    search: '',
                    hasRole:
                        isAdmin(activeRole.role) || isSuperAdmin(activeRole.role)
                            ? {}
                            : { unit: activeRole.unit }
                },
                data: updatedUsers
            });
        }
    });
    const [createUser] = useMutation(CREATE_USER);

    const submitCreateUser = async (mappedUserData) => {
        try {
            const {
                data: {
                    createUser: { id }
                }
            } = await createUser({ variables: { user: mappedUserData.userData } });
            await addUserRole({
                variables: {
                    id,
                    roleData: {
                        role: mappedUserData.roleData.role,
                        campuses: [mappedUserData.roleData.campus],
                        unit: mappedUserData.roleData.unit
                    },
                    campusId: mappedUserData.roleData.campus.id
                }
            });
            addAlert({ message: "L'utilisateur a bien été créé", severity: 'success' });
            return router.push(ADMIN_USER_ADMINISTRATION);
        } catch (e) {
            switch (true) {
                case e.message.includes('User already exists'):
                    return addAlert({
                        message: 'Un utilisateur est déjà enregistré avec cet e-mail',
                        severity: 'error'
                    });
                case e.message.includes('User validation failed: email.original: '):
                    return addAlert({
                        message: "Erreur, veuillez vérifier le domaine de l'adresse e-mail",
                        severity: 'warning'
                    });
                default:
                    return addAlert({
                        message: 'Erreur serveur, merci de réessayer',
                        severity: 'warning'
                    });
            }
        }
    };

    const isUnitValidation = activeRole.role.template === ROLES.ROLE_BASIC_UNIT_VALIDATION.role;

    return (
        <>
            <PageTitle
                subtitles={
                    isUnitValidation
                        ? [`Unité ${activeRole?.unitLabel}`, 'Utilisateur', 'Nouveau demandeur']
                        : ['Utilisateur', 'Nouvel utilisateur']
                }>
                Administration
            </PageTitle>
            <UserForm
                submitForm={submitCreateUser}
                defaultValues={{
                    campus: { id: campusId },
                    unit: { id: activeRole.unit },
                    role: ROLES.ROLE_HOST.role
                }}
                userRole={activeRole}
                type="create"
            />
        </>
    );
}

export default CreateUser;
