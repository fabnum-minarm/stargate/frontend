import React from 'react';
import { CampusesAdministration } from '../../../containers';
import { activeRoleCacheVar, campusIdVar } from '../../../lib/apollo/cache';
import { useRouter } from 'next/router';
import { HANDLE_CAMPUSES } from '../../../utils/permissions';
import { ADMIN_CAMPUS_MANAGEMENT } from '../../../utils/constants/appUrls';

function CampusesAdministrationIndex() {
    const router = useRouter();
    if (!activeRoleCacheVar().role.permissions.includes(HANDLE_CAMPUSES)) {
        router.push(ADMIN_CAMPUS_MANAGEMENT(campusIdVar()));
        return <></>;
    }
    return <CampusesAdministration />;
}

export default CampusesAdministrationIndex;
