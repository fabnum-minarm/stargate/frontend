import React from 'react';
import {
    CampusSectionContainer,
    PlaceSectionContainer,
    UnitSectionContainer,
    AdminSectionContainer,
    NotificationContainer,
    UploadSectionContainer,
    EmployeeSectionContainer
} from '../../../../containers';
import { useRouter } from 'next/router';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { useLogin } from '../../../../lib/loginContext';
import HeaderPageBackBtn from '../../../../components/styled/headerPageBackBtn';
import { HANDLE_CAMPUSES } from '../../../../utils/permissions';
import PageTitle from '../../../../components/styled/common/pageTitle';
import { ADMIN_CAMPUS_ADMINISTRATION } from '../../../../utils/constants/appUrls';
import { useQuery } from '@apollo/client';
import { GET_CAMPUS } from '../../../../lib/apollo/queries';

const useStyles = makeStyles(() => ({
    root: {
        '& > *': {
            marginBottom: 10
        }
    }
}));

function CampusEditionPage() {
    const router = useRouter();
    const { activeRole } = useLogin();
    const { campusId } = router.query;
    const { data, loading } = useQuery(GET_CAMPUS, { variables: { id: campusId } });
    const classes = useStyles();

    if (!campusId) {
        return 'no data';
    }

    return (
        !loading && (
            <Grid className={classes.root}>
                {activeRole.role.permissions.includes(HANDLE_CAMPUSES) && (
                    <HeaderPageBackBtn to={ADMIN_CAMPUS_ADMINISTRATION}>
                        Retour bases
                    </HeaderPageBackBtn>
                )}
                <PageTitle>Administration Base</PageTitle>
                <CampusSectionContainer campusId={campusId} campusData={data} />
                <PlaceSectionContainer campusId={campusId} />
                <UnitSectionContainer campusId={campusId} />
                <AdminSectionContainer campusId={campusId} listRoles={data.getCampus.roles} />
                <EmployeeSectionContainer campusId={campusId} />
                <NotificationContainer campusId={campusId} />
                <UploadSectionContainer campusId={campusId} />
            </Grid>
        )
    );
}

export default CampusEditionPage;
