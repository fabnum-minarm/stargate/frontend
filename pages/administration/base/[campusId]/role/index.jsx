import React from 'react';
import { useRouter } from 'next/router';
import { ValidatorConfiguration } from '../../../../../containers';
import { useQuery } from '@apollo/client';
import { GET_CAMPUS } from '../../../../../lib/apollo/queries';

function RoleEditionPage() {
    const router = useRouter();
    const { campusId } = router.query;
    const { data } = useQuery(GET_CAMPUS, { variables: { id: campusId } });

    if (!campusId || !data) {
        return null;
    }

    return <ValidatorConfiguration campusId={campusId} listRoles={data.getCampus.roles} />;
}

export default RoleEditionPage;
