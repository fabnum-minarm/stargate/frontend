import React from 'react';
import { gql, useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import { AreaFormContainer } from '../../../containers';
import { useSnackBar } from '../../../lib/hooks/snackbar';
import { ADMIN_AREA_ADMINISTRATION } from '../../../utils/constants/appUrls';
import { LIST_AREAS } from '../../../lib/apollo/queries';

const CREATE_AREA = gql`
    mutation createArea($area: AreaInput!) {
        createArea(area: $area) {
            id
            label
            campuses {
                id
                label
            }
        }
    }
`;

function CreateArea() {
    const { addAlert } = useSnackBar();
    const router = useRouter();
    const [createArea] = useMutation(CREATE_AREA, {
        update: async (cache, { data: { createArea } }) => {
            const currentAreas = await cache.readQuery({
                query: LIST_AREAS
            });
            if (!currentAreas) return;
            const updatedTotal = currentAreas.listAreas.meta.total + 1;
            const updatedAreas = {
                listAreas: {
                    ...currentAreas.listAreas,
                    ...(updatedTotal < 10 && {
                        list: [...currentAreas.listAreas.list, createArea]
                    }),
                    meta: {
                        ...currentAreas.listAreas.meta,
                        total: updatedTotal
                    }
                }
            };
            cache.writeQuery({
                query: LIST_AREAS,
                data: updatedAreas
            });
        }
    });

    const submitCreateArea = async (data) => {
        try {
            await createArea({
                variables: {
                    area: { label: data.areaLabel.trim(), campuses: data.areaCampusesList }
                }
            });

            router.push(ADMIN_AREA_ADMINISTRATION);
            return addAlert({
                message: "La zone d'influence a bien été créée",
                severity: 'success'
            });
        } catch (e) {
            return addAlert({
                message: "Erreur : échec de la création de la zone d'influence",
                severity: 'warning'
            });
        }
    };

    return (
        <AreaFormContainer
            onSubmit={submitCreateArea}
            defaultValues={{ label: '', areaCampusesList: [] }}
        />
    );
}

export default CreateArea;
