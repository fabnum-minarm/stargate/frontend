import React, { useState } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import { AreaFormContainer } from '../../../../containers';
import { useSnackBar } from '../../../../lib/hooks/snackbar';
import { ADMIN_AREA_ADMINISTRATION } from '../../../../utils/constants/appUrls';
import { GET_AREA } from '../../../../lib/apollo/queries';
import { mapAreaCampusList } from '../../../../utils/mappers/adminMappers';

const EDIT_AREA = gql`
    mutation editArea($area: AreaInput!, $id: ObjectID!) {
        editArea(area: $area, id: $id) {
            id
            label
            campuses {
                id
                label
            }
        }
    }
`;

function EditArea() {
    const { addAlert } = useSnackBar();
    const router = useRouter();
    const { areaId } = router.query;

    const [editArea] = useMutation(EDIT_AREA);
    const [defaultValues, setDefaultValues] = useState(null);

    useQuery(GET_AREA, {
        variables: { id: areaId },
        onCompleted: (d) =>
            setDefaultValues({
                label: d.getArea.label,
                areaCampusesList: mapAreaCampusList(d.getArea.campuses)
            })
    });

    const submitEditArea = async (data) => {
        try {
            await editArea({
                variables: {
                    id: areaId,
                    area: { label: data.areaLabel.trim(), campuses: data.areaCampusesList }
                }
            });

            router.push(ADMIN_AREA_ADMINISTRATION);
            return addAlert({
                message: "La zone d'influence a bien été modifiée",
                severity: 'success'
            });
        } catch (e) {
            return addAlert({
                message: "Erreur : échec de la modification de la zone d'influence",
                severity: 'warning'
            });
        }
    };

    if (!defaultValues) {
        return '';
    }

    return <AreaFormContainer onSubmit={submitEditArea} defaultValues={defaultValues} />;
}

export default EditArea;
