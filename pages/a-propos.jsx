import React from 'react';
import { About } from '../containers';

function AboutPage() {
    return <About />;
}

export default AboutPage;
