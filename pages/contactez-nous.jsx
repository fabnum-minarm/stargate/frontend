import React from 'react';

import Typography from '@material-ui/core/Typography';
import { IconButton } from '@material-ui/core';

import MailOutlineIcon from '@material-ui/icons/MailOutline';

const ENV_MAIL_FONCTIONNEL =
    'dc-dirisi-div-perf-sd-tips-fabnum-stargate.admin.fct@intradef.gouv.fr';

function ContactUsPage() {
    return (
        <div style={{ textAlign: 'center' }}>
            <Typography variant="h6" color="initial">
                Pour nous signaler un incident ou suggestion d&apos;amélioration:
            </Typography>

            <a
                href={`mailto:${ENV_MAIL_FONCTIONNEL}`}
                title="contact par mail"
                target="_blank"
                rel="noreferrer">
                <IconButton>
                    <MailOutlineIcon />
                </IconButton>
            </a>
        </div>
    );
}

export default ContactUsPage;
